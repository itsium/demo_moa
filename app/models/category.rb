class Category < ActiveRecord::Base
  attr_accessible :color, :is_event, :name
  has_many :contents
end
