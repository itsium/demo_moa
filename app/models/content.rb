class Content < ActiveRecord::Base
  has_many :content_medias, :dependent => :destroy
  has_one :event_info, :dependent => :destroy
  accepts_nested_attributes_for :content_medias, :allow_destroy => :true, reject_if: proc { |t| t['image'].blank?  }
  accepts_nested_attributes_for :event_info, :allow_destroy => :true, reject_if: proc { |t| logger.debug "[DEBUG] #{t.inspect}"; t['adress'].blank? }
  attr_accessible :updated_at, :created_at , :category_id, :data, :title, :content_medias_attributes, :_is_event, :event_info_attributes, :event_info
  belongs_to :category
  has_and_belongs_to_many :users, :join_table => "users_contents"
  has_many :bookmarks
  has_many :fans, source: :user, foreign_key: "user_id", through: :bookmarks
  attr_accessor :_is_event
  alias_method :event_info=, :event_info_attributes=
  validates :title, :presence => true
  validates :category_id, :presence => true
  validates :data, :presence => true

  def is_user_favorite(user)
    self.fans.include? user
  end

  def has_user_subscribed(user)
    self.users.include? user
  end
end
