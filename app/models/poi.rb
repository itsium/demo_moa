class Poi < ActiveRecord::Base
  attr_accessible :address, :description, :latitude, :longitude, :name
  attr_accessible :image
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/poi_image_missing.png"
  geocoded_by :address
  after_validation :geocode, :if => :address_changed?
end
