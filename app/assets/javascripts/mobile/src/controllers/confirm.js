/* confirm */

App.confirm = (function() {

    var _id = 'confirm-alert',
        _alert,
        _titleEl,
        _contentEl,
        _callback,
        _fs = Fs,
        _history = _fs.History;

    var _getAlert = function() {
        if (!_alert) {
            _alert = new _fs.views.Alert({
                config: {
                    id: _id,
                    overlayHide: false,
                    ui: 'd',
                    header: {
                        title: t('Confirm'),
                        ui: 'd'
                    },
                    style: 'position: fixed; top: 50%; left: 50%; margin-left: -140px; width: 280px; margin-top: -140px;'
                },
                listeners: {
                    scope: this,
                    show: function() {
                        _fs.Selector.addClass(document.getElementById('mainpage'), 'blur');
                    },
                    hide: function() {
                        _fs.Selector.removeClass(document.getElementById('mainpage'), 'blur');
                    }
                },
                items: [
                    '<p id="' + _id + '-content"></p><div class="ui-grid-a"><div class="ui-block-a">', {
                    xtype: 'button',
                    config: {
                        text: t('Yes'),
                        ui: 'd',
                        inline: false
                    },
                    handler: function() {
                        if (_callback) {
                            _callback(true);
                        }
                        _alert.hide();
                    }
                }, '</div><div class="ui-block-b">', {
                    xtype: 'button',
                    config: {
                        text: t('No'),
                        ui: 'c',
                        inline: false
                    },
                    handler: function() {
                        if (_callback) {
                            _callback(false);
                        }
                        _alert.hide();
                    }
                }, '</div></div>']
            });
            _alert.compile();
            _alert.render('body', 'afterbegin');
            _titleEl = _fs.Selector.get('.ui-title', document.getElementById(_id));
            _contentEl = document.getElementById(_id + '-content');
        }
        return _alert;
    };

    var _show = function(title, msg, callback) {
        if (!_alert) {
            _getAlert();
        }
        _callback = callback;
        _fs.Selector.updateHtml(_titleEl, title);
        _fs.Selector.updateHtml(_contentEl, _formatmsg(msg));
        _getAlert().show();
        _alert.el.style.marginTop = '-' + (_alert.el.offsetHeight / 2) + 'px';
    };

    var _formatmsg = function(msg) {
        var type = typeof msg;

        if (type === 'string') {
            return msg;
        } else if (type === 'object' &&
            typeof msg.join === 'function') {
            return msg.join('\n');
        } else {
            var i, result = '';

            for (i in msg) {
                result += '<b>' + i + '</b>: ' + msg[i].join(' and ') + '<br>';
            }
            return result;
        }
    };

    return {
        show: _show
    };
})();