/* success */

App.success = (function() {

    var _id = 'success-alert',
        _alert,
        _titleEl,
        _contentEl,
        _callback,
        _fs = Fs,
        _history = _fs.History;

    var _getAlert = function() {
        if (!_alert) {
            _alert = new _fs.views.Alert({
                config: {
                    id: _id,
                    overlayHide: false,
                    ui: 'd',
                    header: {
                        title: t('Success'),
                        ui: 'g'
                    },
                    style: 'position: fixed; top: 50%; left: 50%; margin-left: -140px; width: 280px; margin-top: -140px;'
                },
                listeners: {
                    scope: this,
                    show: function() {
                        _fs.Selector.addClass(document.getElementById('mainpage'), 'blur');
                    },
                    hide: function() {
                        _fs.Selector.removeClass(document.getElementById('mainpage'), 'blur');
                    }
                },
                items: [
                    '<p id="' + _id + '-content"></p>', {
                    xtype: 'button',
                    config: {
                        text: 'OK',
                        ui: 'c',
                        inline: false,
                        style: 'margin: 0 auto;'
                    },
                    handler: function() {
                        if (_callback) {
                            _callback();
                        }
                        _alert.hide();
                    }
                }]
            });
            _alert.compile();
            _alert.render('body', 'afterbegin');
            _titleEl = _fs.Selector.get('.ui-title', document.getElementById(_id));
            _contentEl = document.getElementById(_id + '-content');
        }
        return _alert;
    };

    var _showAlert = function(title, msg, callback) {
        if (!_alert) {
            _getAlert();
        }
        _callback = callback;
        _fs.Selector.updateHtml(_titleEl, title);
        _fs.Selector.updateHtml(_contentEl, _formatMessage(msg));
        _getAlert().show();
        _alert.el.style.marginTop = '-' + (_alert.el.offsetHeight / 2) + 'px';
    };

    var _formatMessage = function(msg) {
        var type = typeof msg;

        if (type === 'string') {
            return msg;
        } else if (type === 'object' &&
            typeof msg.join === 'function') {
            return msg.join('\n');
        } else {
            var i, result = '';

            for (i in msg) {
                result += '<b>' + i + '</b>: ' + msg[i].join(' and ') + '<br>';
            }
            return result;
        }
    };

    var _defaultBehavior = function(requestSuccess, responseData) {
        if (requestSuccess) {
            var message = responseData.message;

            _showAlert(t('Success'), t(message ? message : t('Operation done successfully.')));
        }
    };

    var _onStoreSuccess = function(requestSuccess, store, responseData) {
        return _defaultBehavior(requestSuccess, responseData);
    };

    var _onJsonpSuccess = function(requestSuccess, responseData) {
        return _defaultBehavior(requestSuccess, responseData);
    };

    return {
        onStoreSuccess: _onStoreSuccess,
        onJsonpSuccess: _onJsonpSuccess,
        showAlert: _showAlert
    };
})();