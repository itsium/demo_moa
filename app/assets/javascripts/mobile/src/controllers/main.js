/* Main controller */

App.controllers.Main = (function() {
    // private
    var _layoutPage,
        _mainMenu,
        _activeTab,
        _lastPage,
        _defaultScrollY = (navigator.userAgent.match(/Android/i) ? 1 : 0),
        _pages = {},
        _fs = Fs,
        _history = _fs.History,
        _parent = _fs.Router,
        _app = App,
        _appViews = _app.views,
        _settings = _app.Settings,
        _selector = _fs.Selector,
        _scrollPos = {},
        _views = {
            previous: null,
            current: null
        },
        _activeBtnClass = 'ui-btn-active';


    var _changeActiveTab = function(newTab) {
        var ntab = document.getElementById(newTab);

        if (!ntab) {
            return;
        }
        var activeTabs = _selector.gets('.' + _activeBtnClass,
            document.getElementById('main-menu')),
            length = activeTabs.length;

        while (length--) {
            _selector.removeClass(activeTabs[length], _activeBtnClass);
        }
        _selector.addClass(ntab, _activeBtnClass);
        _activeTab = newTab;
    };

    var _switchPage = function(newPage, routeArgs) {

        var anim,
            w = window,
            fromPage = _pages[_lastPage],
            toPage = _pages[newPage];

        function revert_scroll() {
            if (//typeof _scrollPos[newPage] === 'undefined' &&
                w.navigator.userAgent.match(/Android/i)) {
                var hideAddressBar = function() {
                    //console.log('removing android address bar...');

                    w.scrollTo(0,0);
                    var nPageH = document.height;
                    var nViewH = w.outerHeight;
                    if (nViewH > nPageH) {
                        nViewH -= 250;
                        document.body.style.height = nViewH + 'px';
                    }

                    w.scrollTo(0,1);
                };
                hideAddressBar();
            }
            //w.scrollTo(0, _scrollPos[newPage] || _defaultScrollY);
        }

        function check_footer() {
            if (_views.current.fx && _views.current.fx.hidefooter === true) {
                _selector.addClass(document.getElementById('page-footer'), 'hide');
            } else {
                _selector.removeClass(document.getElementById('page-footer'), 'hide');
            }
        }

        if (_lastPage) {
            _scrollPos[_lastPage] = (w.scrollY < 1 ? _defaultScrollY : w.scrollY);
            /*_selector.addClass(document.getElementById(_lastPage),
                'hidden');*/

            anim = (_fs.fx.isAnimating() ? false : _settings.get('anim'));

            if (!anim && _views.current.fx) {
                anim = _views.current.fx;
            }

            if (!anim) {
                //Fs.fx.fade(document.getElementById(_lastPage), document.getElementById(newPage));
                _selector.addClass(document.getElementById(_lastPage),
                    'hidden');
                check_footer();
            } else {
                _animateTransition(anim, document.getElementById(_lastPage), document.getElementById(newPage), {
                    reverse: (anim.reverse === true ? true : false),
                    callback: revert_scroll
                });
            }

            if (fromPage) {
                fromPage.fireScope('hide', fromPage, 0, _scrollPos[_lastPage]);
            }
        } else {
            check_footer();
        }

        if (!anim) {
            _selector.removeClass(document.getElementById(newPage),
                'hidden');
            revert_scroll();
        }

        _lastPage = newPage;
        if (toPage) {
            toPage.fireScope('show', toPage, routeArgs);
        }
    };

    var _animateTransition = function(anim, from, to, opts) {

        _settings.empty('anim');

        var footer = document.getElementById('page-footer');

        if (_views.current.fx && _views.current.fx.hidefooter === true) {

            opts.afterstart = function() {
                _selector.addClass(footer, 'hide');
            };

        } else if (_selector.hasClass(footer, 'hide')) {

            opts.beforestart = function() {
                _selector.removeClass(footer, 'hide');
            };

        }

        var view = _views.current;

        if (opts.reverse === true) {
            view = _views.previous;
        }

        if (view.fx && view.fx.animation) {
            anim.type = view.fx.animation;
        } else {
            anim.type = 'paraslide';
        }

        _fs.fx[anim.type](from, to, opts);


    };

    var _routeGenerate = function(name, parent) {
        return function() {
            var pageId = 'page-' + name,
                p = _pages[pageId],
                tabname = 'menu-' + (parent ? parent : name) + '-btn';

            if (!p) {
                p = _pages[pageId] = new _appViews[_fs.utils.capitalize(name)]({
                    config: {
                        id: pageId,
                        hidden: true
                    }
                });
                p.compile();
                p.render('#page-footer', 'beforebegin');
            }

            if (_views.current) {
                _views.previous = _views.current;
            }
            _views.current = p;

            _changeActiveTab(tabname);
            _switchPage(pageId, arguments);
        };
    };

    var _parseConfiguration = function() {

        var i, item, conf = App.conf;

        for (i in conf) {
            item = conf[i];

            if (item.enabled === true && item.router) {

                if (item.router.routes) {

                    var route, rconf;

                    for (route in item.router.routes) {
                        rconf = item.router.routes[route];

                        this.routes[route] = rconf[0];
                        this[rconf[0]] = _routeGenerate.apply(this, rconf);
                    }

                }

                if (item.router.before) {

                    var regexp;

                    for (regexp in item.router.before) {
                        this.before[regexp] = item.router.before[regexp];
                    }

                }

            }
        }

    };

    // public
    return _parent.subclass({

        before: {},
        routes: {
            '': 'routeNewsSearch',
            '/news/search': 'routeNewsSearch',
            '/news/detail/:id(/?back=:back)': 'routeNewsDetail',
            //'/news/comments/:id(?back=:back)': 'routeNewsDetailComments',
            '/more/menu': 'routeMoreMenu'
        },

        constructor: function() {

            if (!_layoutPage) {
                _layoutPage = new _appViews.Layout({
                    config: {
                        id: 'mainpage'
                    }
                });
                _layoutPage.compile();
                _layoutPage.render('body');
            }

            _mainMenu = document.getElementById('main-menu');

            // Generating route with activated modules
            _parseConfiguration.call(this);

            _parent.prototype.constructor.apply(this, arguments);

            _fs.History.start();
        },

        isAuthenticated: function(callback) {
            var user = _settings.get('currentUser');

            if (!user) {
                _history.navigate('/user/login/?back=' + _history.here(true));
            } else {
                callback();
            }
        },

        isAlreadyAuthenticated: function(callback) {
            var user = _settings.get('currentUser');

            if (!user) {
                callback();
            } else {
                _history.navigate('/user/profile');
            }
        },

        routeNewsSearch: _routeGenerate('newssearch', 'news'),
        routeNewsDetail: _routeGenerate('newsdetail', 'news'),
        routeMoreMenu: _routeGenerate('moremenu', 'more')
    });
}());