/* showing every views used */

(function() {

    var view,
        parent,
        xkeys = [],
        xtypes = {},
        views = Fs.views;

    function addXtype(xtype, className) {
        xtypes[xtype] = className || true;
        if (typeof className === 'string') {
            var parentXtype = eval(className + '.prototype.xtype');

            if (xkeys.indexOf(parentXtype) === -1) {
                xkeys.push(parentXtype);
            }
        }
        if (xkeys.indexOf(xtype) === -1) {
            xkeys.push(xtype);
            //xkeys.sort();
        }
    };

    function overrideConstructor(parent) {
        return function() {
            addXtype(this.xtype, this.className);
            if (parent) {
                return parent.apply(this, arguments);
            }
        };
    };

    for (view in views) {
        parent = views[view].prototype.constructor;
        views[view].prototype.constructor = overrideConstructor(parent);
    }

    Object.create = (function(parent) {

        return function(obj) {
            if (obj.xtype) {
                addXtype(obj.xtype, obj.className);
            }
            return parent.apply(this, arguments);
        };

    })(Object.create);

    Fs.xtype = (function(parent) {

        return function(xtype) {
            addXtype(xtype);
            return parent.apply(this, arguments);
        };

    })(Fs.xtype);

    Fs.package = function() {
        var key, i = -1, len = xkeys.length;

        while (++i < len) {
            key = xkeys[i];
            console.log(key, ' \t\t ', xtypes[key]);
        }
        console.debug('Found ' + len + ' used xtypes');
    };

    Fs.packageCommand = function() {
        var cmd = 'node utils/package.js --engine "' +
            Fs.config.engine.prototype.xtype + '" views.' +
            xkeys.join(' views.');

        console.log(cmd);
    };

})();