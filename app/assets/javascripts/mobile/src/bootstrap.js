/* bootstrap */

var App = {
    controllers: {},
    views: {},
    models: {},
    stores: {}
};

App.Settings = new Fs.Settings({
    serverUrl: 'http://' + window.location.host,
    //serverUrl: 'http://app.tusstar.com/',
    updaterJson: '/updater.json',
    mapGet: '/pois.json',
    homeGet: '/company/get.json',
    contentSearch: '/contents.json',
    contentGet: '/contents/{id}.json',
    commentsGet: '/comments.json',
    categoryList: '/categories.json',
    userLogin: '/users/sign_in.json?user[email]={email}&user[password]={password}&user[remember_me]=true&_method=POST',
    userLogout: '/users/sign_out.json?_method=DELETE',
    userRegister: '/users.json?user[name]={name}&user[email]={email}&user[password]={password}&user[password_confirmation]={password_confirmation}&_method=POST',
    userProfileUpdate: '/users.json?user[name]={name}&user[company]={company}&user[email]={email}&user[phone]={phone}&user[role]={role}&user[current_password]={current_password}&_method=PUT',
    userFavorites: '/users/favorites.json',
    addFavorite: '/users/add_favorite.json?user[content_id]={id}',
    deleteFavorite: '/users/delete_favorite.json?user[content_id]={id}',
    subscribeEvent: '/contents/{id}/subscribe.json',
    unsubscribeEvent: '/contents/{id}/unsubscribe.json'
});

if (window.currentUser) {
    App.Settings.set('currentUser', window.currentUser);
}

Fs.Storage.setPrefix('moa-');
