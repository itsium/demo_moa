App.views.NewsSocial = (function() {

    var _record,
        _fs = Fs,
        _app = App,
        _helpers = _app.helpers,
        _parent = _fs.views.Popup,
        _settings = _app.Settings,
        _selector = _fs.Selector,
        _addFavUrl = _settings.gets('serverUrl', 'addFavorite').join(''),
        _delFavUrl = _settings.gets('serverUrl', 'deleteFavorite').join(''),
        _hasWechat = false,
        _appConf = _app.conf;

    function device_ready() {
        _hasWechat = (typeof window.Wechat === 'object' ? true : false);
        document.removeEventListener('deviceready', device_ready, false);
    }

    document.addEventListener('deviceready', device_ready, false);

    function afterrender(self) {
        // nothing here for new
    }

    function record_update(self, record) {

        _record = record;

        if (_appConf.enabled('favorites')) {

            if (record.is_favorite === true) {
                _selector.addClass(self.addFavBtn.el, 'hidden');
                _selector.removeClass(self.delFavBtn.el, 'hidden');
            } else {
                _selector.removeClass(self.addFavBtn.el, 'hidden');
                _selector.addClass(self.delFavBtn.el, 'hidden');
            }

        }

    }

    var wechat_moment_share = function() {
        return wechat_share(this, 'TIMELINE');
    };

    var wechat_message_share = function() {
        return wechat_share(this, 'SESSION');
    };

    function wechat_share(self, shareType) {

        var wechat = window.Wechat,
            data = _record,
            synopsis = (data.synopsis || data.data.replace(/^\s+|\s+$/g, '').substr(0, 300).replace(/<[^>]+>/ig, '') + '...'),
            share = {
                title: data.title,
                thumb: _settings.get('serverUrl'),
                description: synopsis,
                media: {
                    type: wechat.Type.WEBPAGE,
                    webpageUrl: (_settings.get('serverUrl') +
                        '/?t=' + (new Date()).getTime() +
                        '#' + _fs.History.here())
                }
            };

        if (data.content_medias.length > 0) {
            share.thumb += data.content_medias[0].thumb;
        } else {
            share.thumb += '/assets/icons/Icon-72@2x.png';
        }

        wechat.share({
            message: share,
            scene: wechat.Scene[shareType]
        }, function () {
            self.hide();
        }, function (reason) {
            self.hide();
            _app.errors.showAlert(t('Wechat error'), reason);
        });

    }

    var _toggleFavorite = function() {

        if (_helpers.isLoading()) {
            return false;
        }

        var self = this,
            user = _settings.get('currentUser');

        if (typeof user !== 'object') {
            self.hide();
            _settings.set('flashlogin', t('You need to login for add a favorite.'));
            _fs.History.navigate('/user/login/?back=' + _fs.History.here(true));
            return false;
        }

        var isFav = _record.is_favorite,
            url = (isFav ? _delFavUrl : _addFavUrl);

        _helpers.showLoading();

        url = url.replace('{id}', _record.id) + '&callback={callback}';

        _fs.Request.jsonp(url, {
            scope: self,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _record.is_favorite = !isFav;
                    self.fire('favoriteUpdate', !isFav);
                    record_update(self, _record);
                } else {
                    _app.errors.onJsonpError(success, data);
                }
            }
        });
    };

    function show() {
        _selector.addClasses(document.querySelector('.ui-page-active'),
            'blur blur-header');
    }

    function hide() {
        _selector.removeClasses(document.querySelector('.ui-page-active'),
            'blur blur-header');
    }

    return _parent.subclass({

        isActive: function() {
            return (_appConf.enabled('favorites') ||
                (_hasWechat && _appConf.enabled('wechat')));
        },

        constructor: function(opts) {

            var self = this,
                items = [];

            if (_appConf.enabled('favorites') === true) {
                items.push({
                    xtype: 'button',
                    ref: 'addFavBtn',
                    config: {
                        ui: 'g',
                        icon: '&#9734;',
                        inline: false,
                        hidden: true,
                        text: t('Add to Favorites')
                    },
                    scope: self,
                    handler: _toggleFavorite
                });
                items.push({
                    xtype: 'button',
                    ref: 'delFavBtn',
                    config: {
                        ui: 'd',
                        icon: '&#9733;',
                        inline: false,
                        hidden: true,
                        text: t('Remove from Favorites')
                    },
                    scope: self,
                    handler: _toggleFavorite
                });
            }

            if (_hasWechat && _appConf.enabled('wechat') === true) {
                items.push({
                    xtype: 'button',
                    config: {
                        inline: false,
                        text: t('Wechat Moment'),
                        iconCssClass: 'wechat',
                        icon: '&#xE801;'
                    },
                    scope: self,
                    handler: wechat_moment_share
                });
                items.push({
                    xtype: 'button',
                    config: {
                        inline: false,
                        text: t('Wechat Message'),
                        iconCssClass: 'wechat',
                        icon: '&#xE800;'
                    },
                    scope: self,
                    handler: wechat_message_share
                });
            }

            opts = {
                config: {
                    //actionSheet: 'top'
                    header: {
                        title: t('Social')
                    }
                },
                items: items,
                listeners: {
                    scope: self,
                    afterrender: afterrender
                }
            };

            _parent.prototype.constructor.call(self, opts);

            self.on('show', show, self);
            self.on('hide', hide, self);
            self.on('recordUpdate', record_update, self);

        }

    });

}());