/* comments */

App.views.Newscomments = (function() {

    var _handlebars = Handlebars,
        _serverUrl = App.Settings.get('serverUrl');

    _handlebars.registerHelper('hasImage', function(context, options) {
        if (context && context.length) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    _handlebars.registerHelper('getImage', function(medias, index) {
        index = index || 0;
        return '"' + _serverUrl + medias[index].medium.replace('"', '\\"') + '"';
    });

    _handlebars.registerHelper('nl2br', function(str) {
        var breakTag = '<br />';
        str = (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        return new _handlebars.SafeString(str);
    });

    var _id = 'newscomments',
        _getUrl = App.Settings.gets('serverUrl', 'commentsGet').join(''),
        _fs = Fs,
        _parent = _fs.views.Container,
        _newsTpl = _fs.Templates.compile([
            '<div id="', _id, '-item" class="news-detail">',
                '{{#if data}}',

                    '<p class="news-detail-date">{{substr data.created_at 10}}</p>',
                    '<p class="news-detail-title">{{data.title}}</p>',
                    '{{#hasImage data.content_medias}}',
                        '{{#foreach data.content_medias}}',
                            '<div class="news-detail-medias" style="background-image:',
                                'url({{getImage ../data.content_medias $index}});"></div>',
                            '{{#if_eq $last compare=false}}',
                                '<hr class="news-detail-image-separator">',
                            '{{/if_eq}}',
                        '{{/foreach}}',
                    '{{/hasImage}}',
                    '<p class="news-detail-content">{{nl2br data.data}}</p>',

                '{{/if}}',
            '</div>'
        ].join(''));

    return _parent.subclass({

        constructor: function(opts) {

        }

    });

}());