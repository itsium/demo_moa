/* home.js */

App.views.Home = (function() {

    var _fs = Fs,
        _settings = App.Settings,
        _url = _settings.gets('serverUrl', 'homeGet').join(''),
        _request = _fs.Request,
        _parent = _fs.views.Container;

    var _onafterrender = function() {

        this.detailTpl.parentEl = this.detailTpl.el.parentNode;

        _request.jsonp(_url + '?callback={callback}', {
            scope: this,
            callback: function(success, data) {

                if (success === true &&
                    typeof data === 'object' &&
                    data.success === true) {
                    _updateLayout.call(this, data);
                } else {
                    App.errors.onJsonpError(success, data);
                }

            }
        });
    };

    var _updateLayout = function(data) {

        var self = this,
            i = -1,
            len = data.data.images.length,
            images = [],
            url = _settings.get('serverUrl');

        while (++i < len) {
            images.push('"' + url + '/' +
                data.data.images[i].url.replace('"', '\\"') + '"');
        }

        self.carousel = new _fs.views.Carousel({
            config: {
                style: 'border-bottom: 2px solid #ddd;',
                images: images,
                height: '250px',
                animation: 1000
            }
        });

        self.carousel.compile();
        self.carousel.render(self.content.el, 'beforebegin');

        /*self.slideClick = new _fs.events.Click();
        self.slideClick.attach(self.carousel.el, function(e) {
            console.log('slideClick: ', arguments, e.target, new Date());
            _fs.History.navigate('/news/detail/4');
        }, self);*/

        self.on('show', function() {
            self.carousel.fireScope('show', self.renderer);
        });

        self.on('hide', function() {
            self.carousel.fireScope('hide', self.renderer);
        });

        self.carousel.fireScope('show', self.renderer);

        self.detailTpl.data = data;
        self.detailTpl.compile();
        self.detailTpl.render(self.content.el);

    };

    return _parent.subclass({

        constructor: function(opts) {

            this.detailTpl = new _fs.views.Template({
                template: _fs.Templates.get('homedetail')
            });

            opts.config.style = 'padding-bottom: 52px;';

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Home'),
                        position: 'fixed'
                    }
                }, {
                    xtype: 'abstract',
                    xtpl: 'container',
                    items: {
                        xtype: 'abstract',
                        xtpl: 'content',
                        ref: 'content'
                    }
                }]//, '<a href="maps://?sll=39.915,116.404&spn=39.915,116.404">Launch ios map !</a><br>',
                //'<a href="comgooglemaps://?center=39.915,116.404&zoom=14&views=traffic">Launch android map !</a>']
            });

            this.on('afterrender', _onafterrender, this);
        }

    });

}());