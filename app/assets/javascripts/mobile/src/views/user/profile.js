/* User profile */

App.views.Userprofile = (function() {

    var _fs = Fs,
        _settings = App.Settings,
        _url = _settings.gets('serverUrl', 'userProfileUpdate').join(''),
        _helpers = App.helpers,
        _errors = App.errors,
        _request = _fs.Request,
        _selector = _fs.Selector,
        _parent = _fs.views.Container;

    var _onAfterRender = function() {
        this.keypressEvent = new _fs.events.Abstract({
            eventName: 'keypress',
            el: this.el,
            autoAttach: true,
            scope: this,
            handler: _keypressed
        });

        var user = _settings.get('currentUser'),
            fields = _fs.Selector.gets('input[name]', this.el);

        _settings.on('currentUserchanged', function(settings, name, value) {
            _helpers.jsonToFields(value, fields);
        }, this);

        if (user) {
            _helpers.jsonToFields(user, fields);
        }
    };

    var _keypressed = function(e) {

        var target = e.target,
            parent = target.parentNode;

        if (_selector.hasClass(parent, 'fielderror') === true) {
            if (target.validity.valid === true) {
                _selector.removeClass(parent, 'fielderror');
            }
        }

        if (e.keyCode === 13) {
            e.target.blur();
            _updateProfile.call(this);
        }
    };

    var _updateProfile = function() {

        if (_helpers.validateFields('input[name]', this.el) === false) {
            return false;
        }

        var fields = _helpers.fieldsToJson('input[name]', this.el),
            url = _url + '&callback={callback}';

        if (fields !== false) {
            url = _helpers.replaceByValues(url, fields, encodeURIComponent);
            _helpers.showLoading();
            _request.jsonp(url, {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        _settings.set('currentUser', data.user);
                    } else {
                        _errors.onJsonpError(success, data);
                    }
                }
            });
        } else {
            var self = this;
            setTimeout(function() {
                _fs.Selector.get('input[name="current_password"]', self.el).focus();
            }, 500);
        }
    };

    return _parent.subclass({

        fx: {
            hidefooter: true,
            animation: 'paraslide'
        },

        constructor: function(opts) {

            var self = this;

            var profile = [{
                xtype: 'textfield',
                config: {
                    label: t('Email'),
                    labelInline: true,
                    icon: 'mail',
                    type: 'tel',
                    name: 'email',
                    readonly: true,
                    required: true,
                    disabled: true,
                    placeHolder: t('Email') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Name'),
                    labelInline: true,
                    icon: 'user',
                    name: 'name',
                    placeHolder: t('Name') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Company'),
                    labelInline: true,
                    icon: 'company',
                    name: 'company',
                    placeHolder: t('Company') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Role'),
                    labelInline: true,
                    icon: 'role',
                    name: 'role',
                    placeHolder: t('Role') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Phone'),
                    labelInline: true,
                    icon: 'phone',
                    type: 'tel',
                    name: 'phone',
                    placeHolder: t('Phone') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Current password'),
                    labelInline: true,
                    icon: 'key',
                    type: 'password',
                    name: 'current_password',
                    required: true,
                    placeHolder: t('Password') + '...'
                }
            }, {
                xtype: 'button',
                config: {
                    text: t('Update'),
                    ui: 'g',
                    inline: false
                },
                scope: this,
                handler: _updateProfile
            }];

            var items = [{
                xtype: 'header',
                config: {
                    ui: 'f',
                    title: t('My profile'),
                    position: 'fixed'
                },
                items: [{
                    xtype: 'button',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-left simple-btn',
                        icon: '&#59233;'
                    },
                    scope: this,
                    handler: function() {

                        _settings.set('anim', {
                            type: 'slide',
                            reverse: true
                        });

                        _fs.History.navigate('/more/menu');
                    }
                }]
            }, {
                xtype: 'abstract',
                config: {
                    style: 'padding-bottom: 51px;'
                },
                xtpl: 'content',
                items: profile
            }];

            _parent.prototype.constructor.call(self, {
                config: opts.config,
                items: items,
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });
        }
    });

}());