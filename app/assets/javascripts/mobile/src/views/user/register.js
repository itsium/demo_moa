/* user register */

App.views.Userregister = (function() {

    var _fs = Fs,
        _settings = App.Settings,
        _url = _settings.gets('serverUrl', 'userRegister').join(''),
        _helpers = App.helpers,
        _errors = App.errors,
        _request = _fs.Request,
        _selector = _fs.Selector,
        _parent = _fs.views.Container;

    var _onAfterRender = function() {
        this.keypressEvent = new _fs.events.Abstract({
            eventName: 'keypress',
            el: this.el,
            autoAttach: true,
            scope: this,
            handler: _keypressed
        });
    };

    var _onShow = function(routeArgs) {
        _backUrl = decodeURIComponent(routeArgs[0]) || undefined;
        this.keypressEvent.attach();
    };

    var _onHide = function() {
        this.keypressEvent.detach();
    };

    var _keypressed = function(e) {

        var target = e.target,
            parent = target.parentNode;

        if (_selector.hasClass(parent, 'fielderror') === true) {
            if (target.validity.valid === true) {
                _selector.removeClass(parent, 'fielderror');
            }
        }

        if (e.keyCode === 13) {
            e.target.blur();
            _register.call(this);
        }
    };

    var _register = function() {

        if (_helpers.validateFields('input[name]', this.el) === false) {
            return false;
        }

        var fields = _helpers.fieldsToJson('input[name]', this.el),
            url = _url + '&callback={callback}';

        if (fields !== false) {
            url = _helpers.replaceByValues(url, fields, encodeURIComponent);
            _helpers.showLoading();
            _request.jsonp(url, {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        App.success.onJsonpSuccess(success, data);
                        App.Settings.set('currentUser', data.user);
                        _fs.History.navigate('');
                    } else {
                        _errors.onJsonpError(success, data);
                    }
                }
            });
        }
    };

    return _parent.subclass({

        xtpl: 'container',

        fx: {
            hidefooter: true,
            animation: 'upslide'
        },

        constructor: function(opts) {

            var items = [{
                xtype: 'textfield',
                ref: 'emailField',
                config: {
                    name: 'name',
                    icon: 'user',
                    type: 'text',
                    required: false,
                    placeHolder: t('Name') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'emailField',
                config: {
                    name: 'email',
                    icon: 'mail',
                    type: 'email',
                    required: true,
                    placeHolder: t('Email') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'passField',
                config: {
                    name: 'password',
                    icon: 'key',
                    type: 'password',
                    required: true,
                    placeHolder: t('Password') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'passconfirmField',
                config: {
                    name: 'password_confirmation',
                    icon: 'key',
                    type: 'password',
                    required: true,
                    placeHolder: t('Confirm password') + '...'
                }
            }, {
                xtype: 'button',
                config: {
                    inline: false,
                    ui: 'g',
                    text: t('Register'),
                    xicon: '&#128274;'
                },
                scope: this,
                handler: _register
            }];

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Register'),
                        position: 'fixed'
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            ui: 'f',
                            size: 'large',
                            cssClass: 'ui-btn-left simple-btn back-btn',
                            icon: '&#59233;'
                        },
                        scope: this,
                        handler: function() {

                            _settings.set('anim', {
                                type: 'slide',
                                reverse: true
                            });

                            _fs.History.navigate('/more/menu');
                        }
                    }]
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    items: items
                }],
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });

            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
        }

    });

}());