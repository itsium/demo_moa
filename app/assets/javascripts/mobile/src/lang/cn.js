/* cn.js */

App.helpers.lang.addLang('cn', {
  'About':'关于',
  'About us':'关于我们',
  'Account':'账号',
  'Company':'公司',
  'Confirm password':'确认密码',
  'Current password':'现在的密码',
  'Email':'电子邮箱',
  'Event date':'日期',
  'Favorites':'收藏',
  'Full':'满',
  'Home':'主页',
  'Loading':'载入中',
  'Logged as':'登录为',
  'Login':'登录',
  'Logout':'退出',
  'Map':'地图',
  'More':'更多',
  'My profile':'我的信息',
  'Name':'姓名',
  'News':'新闻',
  'No favorites.':'没有收藏。',
  'No news.':'没有新闻',
  'Not opened yet':'未开发活动',
  'Password':'密码',
  'Phone':'电话号码',
  'Profile':'我的信息',
  'Register':'注册',
  'Response error':'信息错误',
  'Unknown error.':'服务器错误，代码 002',
  'Role':'职位',
  'Show all':'所有内容',
  'Sign in':'登录',
  'Subscribe to event':'报名',
  'Unable to reach server.':'服务器错误，代码 003',
  'Unsubscribe to event':'取消报名',
  'Update':'更新',
  'You has been logged out, please sign in again.':'请重新登录。',
  'You need to login to use favorites.': '请先登录再使用此功能。',
  'You need to login for add a favorite.': '请先登录再使用此功能。',
  'You need to login for register to an event.': '请先登录再使用此功能。',
  'Start':'开始',
  'End':'介绍',
  'Total seats':'一共位置',
  'Available seats':'剩下位置',
  'Address' : '地址',
  'or' : '或者',
  'Success': '成功',
  'You have successfully subscribed to this events.': '您已经成功报名这次活动。',
  'Operation done successfully.': '操作成功。',
  'Unsubscribe': '取消报名',
  'Are you sure you want to unsubscribe from this event ?': '是否确定取消这次的活动报名。',
  'Yes': '是',
  'No': '否',
  'Add to Favorites': '添加到最爱',
  'Remove from Favorites': '从最爱中移除',
  'Wechat Moment': '微信分享时刻',
  'Wechat Message': '微信短信',
  'Social': '社交',
  'Wechat error': '微信错误'
});

App.helpers.lang.setLang('cn');
