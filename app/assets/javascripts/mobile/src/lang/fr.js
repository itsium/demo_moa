/* en.js */

App.helpers.lang.addLang('fr', {
    // Errors
    'Error': 'Erreur',
    'You has been logged out, please sign in again.': 'Vous avez été déconnecté. Merci de vous reconnecter.',
    'Response error': 'Erreur de retour',
    'Unknown error.': 'Erreur inconnue.',
    'Server error': 'Erreur serveur',
    'Unable to reach server.': 'Impossible de contacter le serveur.',

    // User account
    'Email': 'Adresse email',
    'Password': 'Mot de passe',
    'Sign in': 'Se connecter',
    'Register': 'Créer un compte',
    'Name': 'Nom',
    'Confirm password': 'Confirmer mot de passe',
    'Company': 'Entreprise',
    'Role': 'Rôle',
    'Phone': 'Téléphone',
    'My profile': 'Mon profile',

    // Contents
    'Show all': 'Tout afficher'
});