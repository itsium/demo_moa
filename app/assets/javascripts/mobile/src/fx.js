// fs animation

/**
 * @singleton
 * @class Fs.fx
 * @extends Fs
 */
Fs.fx = new (function() {

    var _fs = Fs,
        _win = window,
        _doc = document,
        _selector = _fs.Selector,
        _parent = _fs.Event,
        _settings = App.Settings,
        _isAnimating = false,
        _footer;

    var _isFailover = !('box-shadow' in _doc.body.style),
        link = _doc.createElement('link');

    link.type = 'text/css';
    link.media = 'screen';
    link.rel = 'stylesheet';

    // @TODO CSS URL Need to be dynamic or updated easily !!!!!
    if (_isFailover === true) {
        link.href = _settings.get('assets', '/assets') + '/animation-failover.min.css?' + _fs.version;
    } else {
        link.href = _settings.get('assets', '/assets') + '/animation.min.css?' + _fs.version;
    }

    link.id = 'fs-animation';
    _doc.head.appendChild(link);

    /*
    _win.requestAnimationFrame = (function(){
        return (_win.requestAnimationFrame ||
            _win.webkitRequestAnimationFrame ||
            _win.mozRequestAnimationFrame ||
            _win.requestAnimFrame ||
            function(callback) {
                _win.setTimeout(callback, 1000 / 60);
            });
    })();
    */

    var _redraw = function(el) {
        _selector.addClass(el, 'repaint');
        var h = el.offsetHeight;
        _selector.removeClass(el, 'repaint');
    };

    /*
    var _frameEffect = function(name, from, to, opts) {

        _isAnimating = true;

        opts = opts || {
            reverse: false
        };

        if (!from && !to) {
            if (opts.callback) {
                opts.callback(false);
            }
            return false;
        }

        _selector.addClass(document.body, 'ui-mobile-viewport-transitioning');

    };
    */

    /**
     * @method
     * @param {String} name Effect name.
     * @param {DOM} from From DOM element.
     * @param {DOM} to To DOM element.
     * @param {Object} opts Options:<pre>
        - scope: Object / window
        - reverse: true / false
        - callback: function(success) {} / undefined
        - directions: { from: 'in', to: 'out' }
        - beforeend: function() {}
        - beforestart: function() {}
     * </pre>
     */
    var _effect = function(name, from, to, opts) {

        _isAnimating = true;

        opts = opts || {
            reverse: false,
            scope: window
        };

        if (typeof opts.directions !== 'object') {
            opts.directions = {
                from: 'in',
                to: 'out'
            };
        }

        if (!from && !to) {
            if (opts.callback) {
                opts.callback.call(opts.scope, false);
            }
            return false;
        }

        if (_isFailover === true) {

            if (opts.beforestart) {
                opts.beforestart.call(opts.scope);
            }

            if (opts.afterstart) {
                opts.afterstart.call(opts.scope);
            }

            setTimeout(function() {

                if (from) {
                    _selector.addClass(from, 'hidden');
                }

                if (to) {
                    _selector.removeClass(to, 'hidden');
                }

                if (opts.beforeend) {
                    opts.beforeend.call(opts.scope);
                }

                _isAnimating = false;

                if (opts.callback) {
                    opts.callback.call(opts.scope, true);
                }

            }, 1);

            return;
        }

        if (to) {
            to.addEventListener('webkitAnimationEnd', afteranim, false);
        } else {
            from.addEventListener('webkitAnimationEnd', afteranim, false);
        }

        if (to) {
            _selector.removeClass(to, 'hidden');
            _selector.addClasses(to, 'positioning container-animating ' +
                name + ' in' + (opts.reverse === true ? ' reverse' : ''));
            //_redraw(to);
        }

        if (from) {
            _selector.addClasses(from, 'positioning container-animating ' +
                name + ' out' + (opts.reverse === true ? ' reverse' : ''));
            //_redraw(from);
        }

        if (opts.beforestart) {
            opts.beforestart.call(opts.scope);
        }

        _selector.addClass(document.body, 'ui-mobile-viewport-transitioning');

        if (opts.afterstart) {
            opts.afterstart.call(opts.scope);
        }

        function afteranim(e) {

            if (from) {
                _selector.addClass(from, 'hidden');
                _selector.removeClasses(from, (name +
                    ' container-animating out positioning'));
                if (opts.reverse === true) {
                    _selector.removeClass(from, 'reverse');
                }
            }

            if (to) {
                _selector.removeClasses(to, (name +
                    ' in positioning container-animating'));
                if (opts.reverse === true) {
                    _selector.removeClass(to, 'reverse');
                }
            }

            if (e) {
                e.target.removeEventListener('webkitAnimationEnd', arguments.callee);
            }

            if (opts.beforeend) {
                opts.beforeend.call(opts.scope);
            }

            _selector.removeClass(document.body, 'ui-mobile-viewport-transitioning');

            _isAnimating = false;

            if (opts.callback) {
                opts.callback.call(opts.scope, true);
            }
        }

        //setTimeout(afteranim, 350);

    };

    return _parent.subclass({

        constructor: function() {
            _parent.prototype.constructor.apply(this, arguments);
        },

        isAnimating: function() {
            return _isAnimating;
        },

        slide: function(from, to, opts) {
            _effect.call(this, 'slide', from, to, opts);
        },

        upslide: function(from, to, opts) {
            _effect.call(this, 'upslide', from, to, opts);
        },

        paraslide: function(from, to, opts) {
            _effect.call(this, 'paraslide', from, to, opts);
        },

        fade: function(from, to, opts) {
            _effect.call(this, 'fade', from, to, opts);
        },

        pop: function(from, to, opts) {
            _effect.call(this, 'pop', from, to, opts);
        },

        turn: function(from, to, opts) {

            _selector.addClass(document.body, 'viewport-turn');

            var cb = opts.callback;

            opts.callback = function(success) {

                _selector.removeClass(document.body, 'viewport-turn');

                if (cb) {
                    cb(success);
                }
            };

            _effect.call(this, 'turn', from, to, opts);
        },

        flip: function(from, to, opts) {

            _selector.addClass(document.body, 'viewport-flip');

            var cb = opts.callback;

            opts.callback = function(success) {

                _selector.removeClass(document.body, 'viewport-flip');

                if (cb) {
                    cb(success);
                }
            };

            _effect.call(this, 'flip', from, to, opts);
        }

    });

}())();