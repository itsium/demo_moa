
function FastClick(layer) {
    'use strict';
    var oldOnClick, self = this;
    
    this.trackingClick = false;
    
    this.trackingClickStart = 0;
    
    this.targetElement = null;
    
    this.touchStartX = 0;
    
    this.touchStartY = 0;
    
    this.lastTouchIdentifier = 0;
    
    this.touchBoundary = 10;
    
    this.layer = layer;
    if (!layer || !layer.nodeType) {
        throw new TypeError('Layer must be a document node');
    }
    
    this.onClick = function() { return FastClick.prototype.onClick.apply(self, arguments); };
    
    this.onMouse = function() { return FastClick.prototype.onMouse.apply(self, arguments); };
    
    this.onTouchStart = function() { return FastClick.prototype.onTouchStart.apply(self, arguments); };
    
    this.onTouchEnd = function() { return FastClick.prototype.onTouchEnd.apply(self, arguments); };
    
    this.onTouchCancel = function() { return FastClick.prototype.onTouchCancel.apply(self, arguments); };
    if (FastClick.notNeeded(layer)) {
        return;
    }
    
    if (this.deviceIsAndroid) {
        layer.addEventListener('mouseover', this.onMouse, true);
        layer.addEventListener('mousedown', this.onMouse, true);
        layer.addEventListener('mouseup', this.onMouse, true);
    }
    layer.addEventListener('click', this.onClick, true);
    layer.addEventListener('touchstart', this.onTouchStart, false);
    layer.addEventListener('touchend', this.onTouchEnd, false);
    layer.addEventListener('touchcancel', this.onTouchCancel, false);
    
    
    
    if (!Event.prototype.stopImmediatePropagation) {
        layer.removeEventListener = function(type, callback, capture) {
            var rmv = Node.prototype.removeEventListener;
            if (type === 'click') {
                rmv.call(layer, type, callback.hijacked || callback, capture);
            } else {
                rmv.call(layer, type, callback, capture);
            }
        };
        layer.addEventListener = function(type, callback, capture) {
            var adv = Node.prototype.addEventListener;
            if (type === 'click') {
                adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
                    if (!event.propagationStopped) {
                        callback(event);
                    }
                }), capture);
            } else {
                adv.call(layer, type, callback, capture);
            }
        };
    }
    
    
    
    if (typeof layer.onclick === 'function') {
        
        
        oldOnClick = layer.onclick;
        layer.addEventListener('click', function(event) {
            oldOnClick(event);
        }, false);
        layer.onclick = null;
    }
}
FastClick.prototype.deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0;
FastClick.prototype.deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent);
FastClick.prototype.deviceIsIOS4 = FastClick.prototype.deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);
FastClick.prototype.deviceIsIOSWithBadTarget = FastClick.prototype.deviceIsIOS && (/OS ([6-9]|\d{2})_\d/).test(navigator.userAgent);
FastClick.prototype.needsClick = function(target) {
    'use strict';
    switch (target.nodeName.toLowerCase()) {
    
    case 'button':
    case 'select':
    case 'textarea':
        if (target.disabled) {
            return true;
        }
        break;
    case 'input':
        
        if ((this.deviceIsIOS && target.type === 'file') || target.disabled) {
            return true;
        }
        break;
    case 'label':
    case 'video':
        return true;
    }
    return (/\bneedsclick\b/).test(target.className);
};
FastClick.prototype.needsFocus = function(target) {
    'use strict';
    switch (target.nodeName.toLowerCase()) {
    case 'textarea':
    case 'select':
        return true;
    case 'input':
        switch (target.type) {
        case 'button':
        case 'checkbox':
        case 'file':
        case 'image':
        case 'radio':
        case 'submit':
            return false;
        }
        
        return !target.disabled && !target.readOnly;
    default:
        return (/\bneedsfocus\b/).test(target.className);
    }
};
FastClick.prototype.sendClick = function(targetElement, event) {
    'use strict';
    var clickEvent, touch;
    
    if (document.activeElement && document.activeElement !== targetElement) {
        document.activeElement.blur();
    }
    touch = event.changedTouches[0];
    
    clickEvent = document.createEvent('MouseEvents');
    clickEvent.initMouseEvent('click', true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
    clickEvent.forwardedTouchEvent = true;
    targetElement.dispatchEvent(clickEvent);
};
FastClick.prototype.focus = function(targetElement) {
    'use strict';
    var length;
    if (this.deviceIsIOS && targetElement.setSelectionRange) {
        length = targetElement.value.length;
        targetElement.setSelectionRange(length, length);
    } else {
        targetElement.focus();
    }
};
FastClick.prototype.updateScrollParent = function(targetElement) {
    'use strict';
    var scrollParent, parentElement;
    scrollParent = targetElement.fastClickScrollParent;
    
    
    if (!scrollParent || !scrollParent.contains(targetElement)) {
        parentElement = targetElement;
        do {
            if (parentElement.scrollHeight > parentElement.offsetHeight) {
                scrollParent = parentElement;
                targetElement.fastClickScrollParent = parentElement;
                break;
            }
            parentElement = parentElement.parentElement;
        } while (parentElement);
    }
    
    if (scrollParent) {
        scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
    }
};
FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {
    'use strict';
    
    if (eventTarget.nodeType === Node.TEXT_NODE) {
        return eventTarget.parentNode;
    }
    return eventTarget;
};
FastClick.prototype.onTouchStart = function(event) {
    'use strict';
    var targetElement, touch, selection;
    
    if (event.targetTouches.length > 1) {
        return true;
    }
    targetElement = this.getTargetElementFromEventTarget(event.target);
    touch = event.targetTouches[0];
    if (this.deviceIsIOS) {
        
        selection = window.getSelection();
        if (selection.rangeCount && !selection.isCollapsed) {
            return true;
        }
        if (!this.deviceIsIOS4) {
            
            
            
            
            
            if (touch.identifier === this.lastTouchIdentifier) {
                event.preventDefault();
                return false;
            }
            this.lastTouchIdentifier = touch.identifier;
            
            
            
            
            
            
            this.updateScrollParent(targetElement);
        }
    }
    this.trackingClick = true;
    this.trackingClickStart = event.timeStamp;
    this.targetElement = targetElement;
    this.touchStartX = touch.pageX;
    this.touchStartY = touch.pageY;
    
    if ((event.timeStamp - this.lastClickTime) < 200) {
        event.preventDefault();
    }
    return true;
};
FastClick.prototype.touchHasMoved = function(event) {
    'use strict';
    var touch = event.changedTouches[0], boundary = this.touchBoundary;
    if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
        return true;
    }
    return false;
};
FastClick.prototype.findControl = function(labelElement) {
    'use strict';
    
    if (labelElement.control !== undefined) {
        return labelElement.control;
    }
    
    if (labelElement.htmlFor) {
        return document.getElementById(labelElement.htmlFor);
    }
    
    
    return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
};
FastClick.prototype.onTouchEnd = function(event) {
    'use strict';
    var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;
    
    if (this.touchHasMoved(event)) {
        this.trackingClick = false;
        this.targetElement = null;
    }
    if (!this.trackingClick) {
        return true;
    }
    
    if ((event.timeStamp - this.lastClickTime) < 200) {
        this.cancelNextClick = true;
        return true;
    }
    this.lastClickTime = event.timeStamp;
    trackingClickStart = this.trackingClickStart;
    this.trackingClick = false;
    this.trackingClickStart = 0;
    
    
    
    
    if (this.deviceIsIOSWithBadTarget) {
        touch = event.changedTouches[0];
        
        targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
        targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
    }
    targetTagName = targetElement.tagName.toLowerCase();
    if (targetTagName === 'label') {
        forElement = this.findControl(targetElement);
        if (forElement) {
            this.focus(targetElement);
            if (this.deviceIsAndroid) {
                return false;
            }
            targetElement = forElement;
        }
    } else if (this.needsFocus(targetElement)) {
        
        
        if ((event.timeStamp - trackingClickStart) > 100 || (this.deviceIsIOS && window.top !== window && targetTagName === 'input')) {
            this.targetElement = null;
            return false;
        }
        this.focus(targetElement);
        
        if (!this.deviceIsIOS4 || targetTagName !== 'select') {
            this.targetElement = null;
            event.preventDefault();
        }
        return false;
    }
    if (this.deviceIsIOS && !this.deviceIsIOS4) {
        
        
        scrollParent = targetElement.fastClickScrollParent;
        if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
            return true;
        }
    }
    
    
    if (!this.needsClick(targetElement)) {
        event.preventDefault();
        this.sendClick(targetElement, event);
    }
    return false;
};
FastClick.prototype.onTouchCancel = function() {
    'use strict';
    this.trackingClick = false;
    this.targetElement = null;
};
FastClick.prototype.onMouse = function(event) {
    'use strict';
    
    if (!this.targetElement) {
        return true;
    }
    if (event.forwardedTouchEvent) {
        return true;
    }
    
    if (!event.cancelable) {
        return true;
    }
    
    
    
    if (!this.needsClick(this.targetElement) || this.cancelNextClick) {
        
        if (event.stopImmediatePropagation) {
            event.stopImmediatePropagation();
        } else {
            
            event.propagationStopped = true;
        }
        
        event.stopPropagation();
        event.preventDefault();
        return false;
    }
    
    return true;
};
FastClick.prototype.onClick = function(event) {
    'use strict';
    var permitted;
    
    if (this.trackingClick) {
        this.targetElement = null;
        this.trackingClick = false;
        return true;
    }
    
    if (event.target.type === 'submit' && event.detail === 0) {
        return true;
    }
    permitted = this.onMouse(event);
    
    if (!permitted) {
        this.targetElement = null;
    }
    
    return permitted;
};
FastClick.prototype.destroy = function() {
    'use strict';
    var layer = this.layer;
    if (this.deviceIsAndroid) {
        layer.removeEventListener('mouseover', this.onMouse, true);
        layer.removeEventListener('mousedown', this.onMouse, true);
        layer.removeEventListener('mouseup', this.onMouse, true);
    }
    layer.removeEventListener('click', this.onClick, true);
    layer.removeEventListener('touchstart', this.onTouchStart, false);
    layer.removeEventListener('touchend', this.onTouchEnd, false);
    layer.removeEventListener('touchcancel', this.onTouchCancel, false);
};
FastClick.notNeeded = function(layer) {
    'use strict';
    var metaViewport;
    
    if (typeof window.ontouchstart === 'undefined') {
        return true;
    }
    if ((/Chrome\/[0-9]+/).test(navigator.userAgent)) {
        
        if (FastClick.prototype.deviceIsAndroid) {
            metaViewport = document.querySelector('meta[name=viewport]');
            if (metaViewport && metaViewport.content.indexOf('user-scalable=no') !== -1) {
                return true;
            }
        
        } else {
            return true;
        }
    }
    
    if (layer.style.msTouchAction === 'none') {
        return true;
    }
    return false;
};
FastClick.attach = function(layer) {
    'use strict';
    return new FastClick(layer);
};
if (typeof define !== 'undefined' && define.amd) {
    
    define(function() {
        'use strict';
        return FastClick;
    });
} else if (typeof module !== 'undefined' && module.exports) {
    module.exports = FastClick.attach;
    module.exports.FastClick = FastClick;
} else {
    window.FastClick = FastClick;
}
var Handlebars = (function() {
var __module3__ = (function() {
  "use strict";
  var __exports__;
  
  function SafeString(string) {
    this.string = string;
  }
  SafeString.prototype.toString = function() {
    return "" + this.string;
  };
  __exports__ = SafeString;
  return __exports__;
})();
var __module2__ = (function(__dependency1__) {
  "use strict";
  var __exports__ = {};
  
  var SafeString = __dependency1__;
  var escape = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': "&quot;",
    "'": "&#x27;",
    "`": "&#x60;"
  };
  var badChars = /[&<>"'`]/g;
  var possible = /[&<>"'`]/;
  function escapeChar(chr) {
    return escape[chr] || "&amp;";
  }
  function extend(obj, value) {
    for(var key in value) {
      if(Object.prototype.hasOwnProperty.call(value, key)) {
        obj[key] = value[key];
      }
    }
  }
  __exports__.extend = extend;var toString = Object.prototype.toString;
  __exports__.toString = toString;
  
  
  var isFunction = function(value) {
    return typeof value === 'function';
  };
  
  if (isFunction(/x/)) {
    isFunction = function(value) {
      return typeof value === 'function' && toString.call(value) === '[object Function]';
    };
  }
  var isFunction;
  __exports__.isFunction = isFunction;
  var isArray = Array.isArray || function(value) {
    return (value && typeof value === 'object') ? toString.call(value) === '[object Array]' : false;
  };
  __exports__.isArray = isArray;
  function escapeExpression(string) {
    
    if (string instanceof SafeString) {
      return string.toString();
    } else if (!string && string !== 0) {
      return "";
    }
    
    
    
    string = "" + string;
    if(!possible.test(string)) { return string; }
    return string.replace(badChars, escapeChar);
  }
  __exports__.escapeExpression = escapeExpression;function isEmpty(value) {
    if (!value && value !== 0) {
      return true;
    } else if (isArray(value) && value.length === 0) {
      return true;
    } else {
      return false;
    }
  }
  __exports__.isEmpty = isEmpty;
  return __exports__;
})(__module3__);
var __module4__ = (function() {
  "use strict";
  var __exports__;
  var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];
  function Exception(message, node) {
    var line;
    if (node && node.firstLine) {
      line = node.firstLine;
      message += ' - ' + line + ':' + node.firstColumn;
    }
    var tmp = Error.prototype.constructor.call(this, message);
    
    for (var idx = 0; idx < errorProps.length; idx++) {
      this[errorProps[idx]] = tmp[errorProps[idx]];
    }
    if (line) {
      this.lineNumber = line;
      this.column = node.firstColumn;
    }
  }
  Exception.prototype = new Error();
  __exports__ = Exception;
  return __exports__;
})();
var __module1__ = (function(__dependency1__, __dependency2__) {
  "use strict";
  var __exports__ = {};
  var Utils = __dependency1__;
  var Exception = __dependency2__;
  var VERSION = "1.3.0";
  __exports__.VERSION = VERSION;var COMPILER_REVISION = 4;
  __exports__.COMPILER_REVISION = COMPILER_REVISION;
  var REVISION_CHANGES = {
    1: '<= 1.0.rc.2', 
    2: '== 1.0.0-rc.3',
    3: '== 1.0.0-rc.4',
    4: '>= 1.0.0'
  };
  __exports__.REVISION_CHANGES = REVISION_CHANGES;
  var isArray = Utils.isArray,
      isFunction = Utils.isFunction,
      toString = Utils.toString,
      objectType = '[object Object]';
  function HandlebarsEnvironment(helpers, partials) {
    this.helpers = helpers || {};
    this.partials = partials || {};
    registerDefaultHelpers(this);
  }
  __exports__.HandlebarsEnvironment = HandlebarsEnvironment;HandlebarsEnvironment.prototype = {
    constructor: HandlebarsEnvironment,
    logger: logger,
    log: log,
    registerHelper: function(name, fn, inverse) {
      if (toString.call(name) === objectType) {
        if (inverse || fn) { throw new Exception('Arg not supported with multiple helpers'); }
        Utils.extend(this.helpers, name);
      } else {
        if (inverse) { fn.not = inverse; }
        this.helpers[name] = fn;
      }
    },
    registerPartial: function(name, str) {
      if (toString.call(name) === objectType) {
        Utils.extend(this.partials,  name);
      } else {
        this.partials[name] = str;
      }
    }
  };
  function registerDefaultHelpers(instance) {
    instance.registerHelper('helperMissing', function(arg) {
      if(arguments.length === 2) {
        return undefined;
      } else {
        throw new Exception("Missing helper: '" + arg + "'");
      }
    });
    instance.registerHelper('blockHelperMissing', function(context, options) {
      var inverse = options.inverse || function() {}, fn = options.fn;
      if (isFunction(context)) { context = context.call(this); }
      if(context === true) {
        return fn(this);
      } else if(context === false || context == null) {
        return inverse(this);
      } else if (isArray(context)) {
        if(context.length > 0) {
          return instance.helpers.each(context, options);
        } else {
          return inverse(this);
        }
      } else {
        return fn(context);
      }
    });
    instance.registerHelper('each', function(context, options) {
      var fn = options.fn, inverse = options.inverse;
      var i = 0, ret = "", data;
      if (isFunction(context)) { context = context.call(this); }
      if (options.data) {
        data = createFrame(options.data);
      }
      if(context && typeof context === 'object') {
        if (isArray(context)) {
          for(var j = context.length; i<j; i++) {
            if (data) {
              data.index = i;
              data.first = (i === 0);
              data.last  = (i === (context.length-1));
            }
            ret = ret + fn(context[i], { data: data });
          }
        } else {
          for(var key in context) {
            if(context.hasOwnProperty(key)) {
              if(data) { 
                data.key = key; 
                data.index = i;
                data.first = (i === 0);
              }
              ret = ret + fn(context[key], {data: data});
              i++;
            }
          }
        }
      }
      if(i === 0){
        ret = inverse(this);
      }
      return ret;
    });
    instance.registerHelper('if', function(conditional, options) {
      if (isFunction(conditional)) { conditional = conditional.call(this); }
      
      
      
      if ((!options.hash.includeZero && !conditional) || Utils.isEmpty(conditional)) {
        return options.inverse(this);
      } else {
        return options.fn(this);
      }
    });
    instance.registerHelper('unless', function(conditional, options) {
      return instance.helpers['if'].call(this, conditional, {fn: options.inverse, inverse: options.fn, hash: options.hash});
    });
    instance.registerHelper('with', function(context, options) {
      if (isFunction(context)) { context = context.call(this); }
      if (!Utils.isEmpty(context)) return options.fn(context);
    });
    instance.registerHelper('log', function(context, options) {
      var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
      instance.log(level, context);
    });
  }
  var logger = {
    methodMap: { 0: 'debug', 1: 'info', 2: 'warn', 3: 'error' },
    
    DEBUG: 0,
    INFO: 1,
    WARN: 2,
    ERROR: 3,
    level: 3,
    
    log: function(level, obj) {
      if (logger.level <= level) {
        var method = logger.methodMap[level];
        if (typeof console !== 'undefined' && console[method]) {
          console[method].call(console, obj);
        }
      }
    }
  };
  __exports__.logger = logger;
  function log(level, obj) { logger.log(level, obj); }
  __exports__.log = log;var createFrame = function(object) {
    var obj = {};
    Utils.extend(obj, object);
    return obj;
  };
  __exports__.createFrame = createFrame;
  return __exports__;
})(__module2__, __module4__);
var __module5__ = (function(__dependency1__, __dependency2__, __dependency3__) {
  "use strict";
  var __exports__ = {};
  var Utils = __dependency1__;
  var Exception = __dependency2__;
  var COMPILER_REVISION = __dependency3__.COMPILER_REVISION;
  var REVISION_CHANGES = __dependency3__.REVISION_CHANGES;
  function checkRevision(compilerInfo) {
    var compilerRevision = compilerInfo && compilerInfo[0] || 1,
        currentRevision = COMPILER_REVISION;
    if (compilerRevision !== currentRevision) {
      if (compilerRevision < currentRevision) {
        var runtimeVersions = REVISION_CHANGES[currentRevision],
            compilerVersions = REVISION_CHANGES[compilerRevision];
        throw new Exception("Template was precompiled with an older version of Handlebars than the current runtime. "+
              "Please update your precompiler to a newer version ("+runtimeVersions+") or downgrade your runtime to an older version ("+compilerVersions+").");
      } else {
        
        throw new Exception("Template was precompiled with a newer version of Handlebars than the current runtime. "+
              "Please update your runtime to a newer version ("+compilerInfo[1]+").");
      }
    }
  }
  __exports__.checkRevision = checkRevision;
  function template(templateSpec, env) {
    if (!env) {
      throw new Exception("No environment passed to template");
    }
    
    
    var invokePartialWrapper = function(partial, name, context, helpers, partials, data) {
      var result = env.VM.invokePartial.apply(this, arguments);
      if (result != null) { return result; }
      if (env.compile) {
        var options = { helpers: helpers, partials: partials, data: data };
        partials[name] = env.compile(partial, { data: data !== undefined }, env);
        return partials[name](context, options);
      } else {
        throw new Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
      }
    };
    
    var container = {
      escapeExpression: Utils.escapeExpression,
      invokePartial: invokePartialWrapper,
      programs: [],
      program: function(i, fn, data) {
        var programWrapper = this.programs[i];
        if(data) {
          programWrapper = program(i, fn, data);
        } else if (!programWrapper) {
          programWrapper = this.programs[i] = program(i, fn);
        }
        return programWrapper;
      },
      merge: function(param, common) {
        var ret = param || common;
        if (param && common && (param !== common)) {
          ret = {};
          Utils.extend(ret, common);
          Utils.extend(ret, param);
        }
        return ret;
      },
      programWithDepth: env.VM.programWithDepth,
      noop: env.VM.noop,
      compilerInfo: null
    };
    return function(context, options) {
      options = options || {};
      var namespace = options.partial ? options : env,
          helpers,
          partials;
      if (!options.partial) {
        helpers = options.helpers;
        partials = options.partials;
      }
      var result = templateSpec.call(
            container,
            namespace, context,
            helpers,
            partials,
            options.data);
      if (!options.partial) {
        env.VM.checkRevision(container.compilerInfo);
      }
      return result;
    };
  }
  __exports__.template = template;function programWithDepth(i, fn, data ) {
    var args = Array.prototype.slice.call(arguments, 3);
    var prog = function(context, options) {
      options = options || {};
      return fn.apply(this, [context, options.data || data].concat(args));
    };
    prog.program = i;
    prog.depth = args.length;
    return prog;
  }
  __exports__.programWithDepth = programWithDepth;function program(i, fn, data) {
    var prog = function(context, options) {
      options = options || {};
      return fn(context, options.data || data);
    };
    prog.program = i;
    prog.depth = 0;
    return prog;
  }
  __exports__.program = program;function invokePartial(partial, name, context, helpers, partials, data) {
    var options = { partial: true, helpers: helpers, partials: partials, data: data };
    if(partial === undefined) {
      throw new Exception("The partial " + name + " could not be found");
    } else if(partial instanceof Function) {
      return partial(context, options);
    }
  }
  __exports__.invokePartial = invokePartial;function noop() { return ""; }
  __exports__.noop = noop;
  return __exports__;
})(__module2__, __module4__, __module1__);
var __module0__ = (function(__dependency1__, __dependency2__, __dependency3__, __dependency4__, __dependency5__) {
  "use strict";
  var __exports__;
  
  var base = __dependency1__;
  
  
  var SafeString = __dependency2__;
  var Exception = __dependency3__;
  var Utils = __dependency4__;
  var runtime = __dependency5__;
  
  var create = function() {
    var hb = new base.HandlebarsEnvironment();
    Utils.extend(hb, base);
    hb.SafeString = SafeString;
    hb.Exception = Exception;
    hb.Utils = Utils;
    hb.VM = runtime;
    hb.template = function(spec) {
      return runtime.template(spec, hb);
    };
    return hb;
  };
  var Handlebars = create();
  Handlebars.create = create;
  __exports__ = Handlebars;
  return __exports__;
})(__module1__, __module3__, __module4__, __module2__, __module5__);
  return __module0__;
})();
;(function(root) {
  'use strict';
  
  var oldRoot = root;
  
  var freeExports = typeof exports == 'object' && exports;
  
  var freeGlobal = typeof global == 'object' && global &&
    (global == global.global ? (root = global) : global);
  
  var reOpera = /Opera/;
  
  var toString = Object.prototype.toString;
  
  var java = /Java/.test(getClassOf(root.java)) && root.java;
  
  var rhino = java && getClassOf(root.environment) == 'Environment';
  
  var alpha = java ? 'a' : '\u03b1';
  
  var beta = java ? 'b' : '\u03b2';
  
  var doc = root.document || {};
  
  var hasOwnProperty = {}.hasOwnProperty;
  
  var nav = root.navigator || {};
  
  var opera = root.operamini || root.opera;
  
  var operaClass = reOpera.test(operaClass = getClassOf(opera)) ? operaClass : (opera = null);
  
  var thisBinding = this;
  
  var userAgent = nav.userAgent || '';
  
  
  function capitalize(string) {
    string = String(string);
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  
  function each(object, callback) {
    var index = -1,
        length = object.length;
    if (length == length >>> 0) {
      while (++index < length) {
        callback(object[index], index, object);
      }
    } else {
      forOwn(object, callback);
    }
  }
  
  function format(string) {
    string = trim(string);
    return /^(?:webOS|i(?:OS|P))/.test(string)
      ? string
      : capitalize(string);
  }
  
  function forOwn(object, callback) {
    for (var key in object) {
      hasKey(object, key) && callback(object[key], key, object);
    }
  }
  
  function getClassOf(value) {
    return value == null
      ? capitalize(value)
      : toString.call(value).slice(8, -1);
  }
  
  function hasKey() {
    
    hasKey = function(object, key) {
      var parent = object != null && (object.constructor || Object).prototype;
      return !!parent && key in Object(object) && !(key in parent && object[key] === parent[key]);
    };
    
    if (getClassOf(hasOwnProperty) == 'Function') {
      hasKey = function(object, key) {
        return object != null && hasOwnProperty.call(object, key);
      };
    }
    
    else if ({}.__proto__ == Object.prototype) {
      hasKey = function(object, key) {
        var result = false;
        if (object != null) {
          object = Object(object);
          object.__proto__ = [object.__proto__, object.__proto__ = null, result = key in object][0];
        }
        return result;
      };
    }
    return hasKey.apply(this, arguments);
  }
  
  function isHostType(object, property) {
    var type = object != null ? typeof object[property] : 'number';
    return !/^(?:boolean|number|string|undefined)$/.test(type) &&
      (type == 'object' ? !!object[property] : true);
  }
  
  function qualify(string) {
    return String(string).replace(/([ -])(?!$)/g, '$1?');
  }
  
  function reduce(array, callback) {
    var accumulator = null;
    each(array, function(value, index) {
      accumulator = callback(accumulator, value, index, array);
    });
    return accumulator;
  }
  
  function trim(string) {
    return String(string).replace(/^ +| +$/g, '');
  }
  
  
  function parse(ua) {
    ua || (ua = userAgent);
    
    var data;
    
    var arch = ua;
    
    var description = [];
    
    var prerelease = null;
    
    var useFeatures = ua == userAgent;
    
    var version = useFeatures && opera && typeof opera.version == 'function' && opera.version();
    
    var layout = getLayout([
      { 'label': 'WebKit', 'pattern': 'AppleWebKit' },
      'iCab',
      'Presto',
      'NetFront',
      'Tasman',
      'Trident',
      'KHTML',
      'Gecko'
    ]);
    
    var name = getName([
      'Adobe AIR',
      'Arora',
      'Avant Browser',
      'Camino',
      'Epiphany',
      'Fennec',
      'Flock',
      'Galeon',
      'GreenBrowser',
      'iCab',
      'Iceweasel',
      'Iron',
      'K-Meleon',
      'Konqueror',
      'Lunascape',
      'Maxthon',
      'Midori',
      'Nook Browser',
      'PhantomJS',
      'Raven',
      'Rekonq',
      'RockMelt',
      'SeaMonkey',
      { 'label': 'Silk', 'pattern': '(?:Cloud9|Silk-Accelerated)' },
      'Sleipnir',
      'SlimBrowser',
      'Sunrise',
      'Swiftfox',
      'WebPositive',
      'Opera Mini',
      'Opera',
      { 'label': 'Opera', 'pattern': 'OPR' },
      'Chrome',
      { 'label': 'Chrome Mobile', 'pattern': '(?:CriOS|CrMo)' },
      { 'label': 'Firefox', 'pattern': '(?:Firefox|Minefield)' },
      { 'label': 'IE', 'pattern': 'MSIE' },
      'Safari'
    ]);
    
    var product = getProduct([
      'BlackBerry',
      { 'label': 'Galaxy S', 'pattern': 'GT-I9000' },
      { 'label': 'Galaxy S2', 'pattern': 'GT-I9100' },
      'Google TV',
      'iPad',
      'iPod',
      'iPhone',
      'Kindle',
      { 'label': 'Kindle Fire', 'pattern': '(?:Cloud9|Silk-Accelerated)' },
      'Nook',
      'PlayBook',
      'PlayStation Vita',
      'TouchPad',
      'Transformer',
      'Xoom'
    ]);
    
    var manufacturer = getManufacturer({
      'Apple': { 'iPad': 1, 'iPhone': 1, 'iPod': 1 },
      'Amazon': { 'Kindle': 1, 'Kindle Fire': 1 },
      'Asus': { 'Transformer': 1 },
      'Barnes & Noble': { 'Nook': 1 },
      'BlackBerry': { 'PlayBook': 1 },
      'Google': { 'Google TV': 1 },
      'HP': { 'TouchPad': 1 },
      'LG': { },
      'Motorola': { 'Xoom': 1 },
      'Nokia': { },
      'Samsung': { 'Galaxy S': 1, 'Galaxy S2': 1 },
      'Sony': { 'PlayStation Vita': 1 }
    });
    
    var os = getOS([
      'Android',
      'CentOS',
      'Debian',
      'Fedora',
      'FreeBSD',
      'Gentoo',
      'Haiku',
      'Kubuntu',
      'Linux Mint',
      'Red Hat',
      'SuSE',
      'Ubuntu',
      'Xubuntu',
      'Cygwin',
      'Symbian OS',
      'hpwOS',
      'webOS ',
      'webOS',
      'Tablet OS',
      'Linux',
      'Mac OS X',
      'Macintosh',
      'Mac',
      'Windows 98;',
      'Windows '
    ]);
    
    
    function getLayout(guesses) {
      return reduce(guesses, function(result, guess) {
        return result || RegExp('\\b' + (
          guess.pattern || qualify(guess)
        ) + '\\b', 'i').exec(ua) && (guess.label || guess);
      });
    }
    
    function getManufacturer(guesses) {
      return reduce(guesses, function(result, value, key) {
        
        return result || (
          value[product] ||
          value[0, /^[a-z]+(?: +[a-z]+\b)*/i.exec(product)] ||
          RegExp('\\b' + (key.pattern || qualify(key)) + '(?:\\b|\\w*\\d)', 'i').exec(ua)
        ) && (key.label || key);
      });
    }
    
    function getName(guesses) {
      return reduce(guesses, function(result, guess) {
        return result || RegExp('\\b' + (
          guess.pattern || qualify(guess)
        ) + '\\b', 'i').exec(ua) && (guess.label || guess);
      });
    }
    
    function getOS(guesses) {
      return reduce(guesses, function(result, guess) {
        var pattern = guess.pattern || qualify(guess);
        if (!result && (result =
            RegExp('\\b' + pattern + '(?:/[\\d.]+|[ \\w.]*)', 'i').exec(ua))) {
          
          
          
          data = {
            '6.2':  '8',
            '6.1':  'Server 2008 R2 / 7',
            '6.0':  'Server 2008 / Vista',
            '5.2':  'Server 2003 / XP 64-bit',
            '5.1':  'XP',
            '5.01': '2000 SP1',
            '5.0':  '2000',
            '4.0':  'NT',
            '4.90': 'ME'
          };
          
          if (/^Win/i.test(result) &&
              (data = data[0, /[\d.]+$/.exec(result)])) {
            result = 'Windows ' + data;
          }
          
          result = format(String(result)
            .replace(RegExp(pattern, 'i'), guess.label || guess)
            .replace(/ ce$/i, ' CE')
            .replace(/hpw/i, 'web')
            .replace(/Macintosh/, 'Mac OS')
            .replace(/_PowerPC/i, ' OS')
            .replace(/(OS X) [^ \d]+/i, '$1')
            .replace(/Mac (OS X)/, '$1')
            .replace(/\/(\d)/, ' $1')
            .replace(/_/g, '.')
            .replace(/(?: BePC|[ .]*fc[ \d.]+)$/i, '')
            .replace(/x86\.64/gi, 'x86_64')
            .split(' on ')[0]);
        }
        return result;
      });
    }
    
    function getProduct(guesses) {
      return reduce(guesses, function(result, guess) {
        var pattern = guess.pattern || qualify(guess);
        if (!result && (result =
              RegExp('\\b' + pattern + ' *\\d+[.\\w_]*', 'i').exec(ua) ||
              RegExp('\\b' + pattern + '(?:; *(?:[a-z]+[_-])?[a-z]+\\d+|[^ ();-]*)', 'i').exec(ua)
            )) {
          
          if ((result = String(guess.label || result).split('/'))[1] && !/[\d.]+/.test(result[0])) {
            result[0] += ' ' + result[1];
          }
          
          guess = guess.label || guess;
          result = format(result[0]
            .replace(RegExp(pattern, 'i'), guess)
            .replace(RegExp('; *(?:' + guess + '[_-])?', 'i'), ' ')
            .replace(RegExp('(' + guess + ')(\\w)', 'i'), '$1 $2'));
        }
        return result;
      });
    }
    
    function getVersion(patterns) {
      return reduce(patterns, function(result, pattern) {
        return result || (RegExp(pattern +
          '(?:-[\\d.]+/|(?: for [\\w-]+)?[ /-])([\\d.]+[^ ();/_-]*)', 'i').exec(ua) || 0)[1] || null;
      });
    }
    
    
    function toStringPlatform() {
      return this.description || '';
    }
    
    
    layout && (layout = [layout]);
    
    if (manufacturer && !product) {
      product = getProduct([manufacturer]);
    }
    
    if ((data = /Google TV/.exec(product))) {
      product = data[0];
    }
    
    if (/\bSimulator\b/i.test(ua)) {
      product = (product ? product + ' ' : '') + 'Simulator';
    }
    
    if (/^iP/.test(product)) {
      name || (name = 'Safari');
      os = 'iOS' + ((data = / OS ([\d_]+)/i.exec(ua))
        ? ' ' + data[1].replace(/_/g, '.')
        : '');
    }
    
    else if (name == 'Konqueror' && !/buntu/i.test(os)) {
      os = 'Kubuntu';
    }
    
    else if (manufacturer && manufacturer != 'Google' &&
        ((/Chrome/.test(name) && !/Mobile Safari/.test(ua)) || /Vita/.test(product))) {
      name = 'Android Browser';
      os = /Android/.test(os) ? os : 'Android';
    }
    
    else if (!name || (data = !/\bMinefield\b/i.test(ua) && /Firefox|Safari/.exec(name))) {
      
      if (name && !product && /[\/,]|^[^(]+?\)/.test(ua.slice(ua.indexOf(data + '/') + 8))) {
        
        name = null;
      }
      
      if ((data = product || manufacturer || os) &&
          (product || manufacturer || /Android|Symbian OS|Tablet OS|webOS/.test(os))) {
        name = /[a-z]+(?: Hat)?/i.exec(/Android/.test(os) ? os : data) + ' Browser';
      }
    }
    
    if (!version) {
      version = getVersion([
        '(?:Cloud9|CriOS|CrMo|Opera ?Mini|OPR|Raven|Silk(?!/[\\d.]+$))',
        'Version',
        qualify(name),
        '(?:Firefox|Minefield|NetFront)'
      ]);
    }
    
    if (layout == 'iCab' && parseFloat(version) > 3) {
      layout = ['WebKit'];
    } else if ((data =
          /Opera/.test(name) && (/OPR/.test(ua) ? 'Blink' : 'Presto') ||
          /\b(?:Midori|Nook|Safari)\b/i.test(ua) && 'WebKit' ||
          !layout && /\bMSIE\b/i.test(ua) && (os == 'Mac OS' ? 'Tasman' : 'Trident')
        )) {
      layout = [data];
    }
    
    if (useFeatures) {
      
      
      if (isHostType(root, 'global')) {
        if (java) {
          data = java.lang.System;
          arch = data.getProperty('os.arch');
          os = os || data.getProperty('os.name') + ' ' + data.getProperty('os.version');
        }
        if (freeExports) {
          
          if (thisBinding == oldRoot && typeof system == 'object' && (data = [system])[0]) {
            os || (os = data[0].os || null);
            try {
              data[1] = require('ringo/engine').version;
              version = data[1].join('.');
              name = 'RingoJS';
            } catch(e) {
              if (data[0].global == freeGlobal) {
                name = 'Narwhal';
              }
            }
          }
          else if (typeof process == 'object' && (data = process)) {
            name = 'Node.js';
            arch = data.arch;
            os = data.platform;
            version = /[\d.]+/.exec(data.version)[0];
          }
          else if (rhino) {
            name = 'Rhino';
          }
        }
        else if (rhino) {
          name = 'Rhino';
        }
      }
      
      else if (getClassOf((data = root.runtime)) == 'ScriptBridgingProxyObject') {
        name = 'Adobe AIR';
        os = data.flash.system.Capabilities.os;
      }
      
      else if (getClassOf((data = root.phantom)) == 'RuntimeObject') {
        name = 'PhantomJS';
        version = (data = data.version || null) && (data.major + '.' + data.minor + '.' + data.patch);
      }
      
      else if (typeof doc.documentMode == 'number' && (data = /\bTrident\/(\d+)/i.exec(ua))) {
        
        
        version = [version, doc.documentMode];
        if ((data = +data[1] + 4) != version[1]) {
          description.push('IE ' + version[1] + ' mode');
          layout[1] = '';
          version[1] = data;
        }
        version = name == 'IE' ? String(version[1].toFixed(1)) : version[0];
      }
      os = os && format(os);
    }
    
    if (version && (data =
          /(?:[ab]|dp|pre|[ab]\d+pre)(?:\d+\+?)?$/i.exec(version) ||
          /(?:alpha|beta)(?: ?\d)?/i.exec(ua + ';' + (useFeatures && nav.appMinorVersion)) ||
          /\bMinefield\b/i.test(ua) && 'a'
        )) {
      prerelease = /b/i.test(data) ? 'beta' : 'alpha';
      version = version.replace(RegExp(data + '\\+?$'), '') +
        (prerelease == 'beta' ? beta : alpha) + (/\d+\+?/.exec(data) || '');
    }
    
    if (name == 'Fennec') {
      name = 'Firefox Mobile';
    }
    
    else if (name == 'Maxthon' && version) {
      version = version.replace(/\.[\d.]+/, '.x');
    }
    
    else if (name == 'Silk') {
      if (!/Mobi/i.test(ua)) {
        os = 'Android';
        description.unshift('desktop mode');
      }
      if (/Accelerated *= *true/i.test(ua)) {
        description.unshift('accelerated');
      }
    }
    
    else if (name == 'IE' && (data = (/; *(?:XBLWP|ZuneWP)(\d+)/i.exec(ua) || 0)[1])) {
      name += ' Mobile';
      os = 'Windows Phone OS ' + data + '.x';
      description.unshift('desktop mode');
    }
    
    else if ((name == 'Chrome' || name == 'IE' || name && !product && !/Browser|Mobi/.test(name)) &&
        (os == 'Windows CE' || /Mobi/i.test(ua))) {
      name += ' Mobile';
    }
    
    else if (name == 'IE' && useFeatures && typeof external == 'object' && !external) {
      description.unshift('platform preview');
    }
    
    
    else if (/BlackBerry/.test(product) && (data =
          (RegExp(product.replace(/ +/g, ' *') + '/([.\\d]+)', 'i').exec(ua) || 0)[1] ||
          version
        )) {
      os = 'Device Software ' + data;
      version = null;
    }
    
    
    else if (this != forOwn && (
          (useFeatures && opera) ||
          (/Opera/.test(name) && /\b(?:MSIE|Firefox)\b/i.test(ua)) ||
          (name == 'Firefox' && /OS X (?:\d+\.){2,}/.test(os)) ||
          (name == 'IE' && (
            (os && !/^Win/.test(os) && version > 5.5) ||
            /Windows XP/.test(os) && version > 8 ||
            version == 8 && !/Trident/.test(ua)
          ))
        ) && !reOpera.test((data = parse.call(forOwn, ua.replace(reOpera, '') + ';'))) && data.name) {
      
      data = 'ing as ' + data.name + ((data = data.version) ? ' ' + data : '');
      if (reOpera.test(name)) {
        if (/IE/.test(data) && os == 'Mac OS') {
          os = null;
        }
        data = 'identify' + data;
      }
      
      else {
        data = 'mask' + data;
        if (operaClass) {
          name = format(operaClass.replace(/([a-z])([A-Z])/g, '$1 $2'));
        } else {
          name = 'Opera';
        }
        if (/IE/.test(data)) {
          os = null;
        }
        if (!useFeatures) {
          version = null;
        }
      }
      layout = ['Presto'];
      description.push(data);
    }
    
    if ((data = (/\bAppleWebKit\/([\d.]+\+?)/i.exec(ua) || 0)[1])) {
      
      
      data = [parseFloat(data.replace(/\.(\d)$/, '.0$1')), data];
      
      if (name == 'Safari' && data[1].slice(-1) == '+') {
        name = 'WebKit Nightly';
        prerelease = 'alpha';
        version = data[1].slice(0, -1);
      }
      
      else if (version == data[1] ||
          version == (/\bSafari\/([\d.]+\+?)/i.exec(ua) || 0)[1]) {
        version = null;
      }
      
      data = [data[0], (/\bChrome\/([\d.]+)/i.exec(ua) || 0)[1]];
      
      
      if (!useFeatures || (/internal|\n/i.test(toString.toString()) && !data[1])) {
        layout[1] = 'like Safari';
        data = (data = data[0], data < 400 ? 1 : data < 500 ? 2 : data < 526 ? 3 : data < 533 ? 4 : data < 534 ? '4+' : data < 535 ? 5 : '5');
      } else {
        layout[1] = 'like Chrome';
        data = data[1] || (data = data[0], data < 530 ? 1 : data < 532 ? 2 : data < 532.05 ? 3 : data < 533 ? 4 : data < 534.03 ? 5 : data < 534.07 ? 6 : data < 534.10 ? 7 : data < 534.13 ? 8 : data < 534.16 ? 9 : data < 534.24 ? 10 : data < 534.30 ? 11 : data < 535.01 ? 12 : data < 535.02 ? '13+' : data < 535.07 ? 15 : data < 535.11 ? 16 : data < 535.19 ? 17 : data < 536.05 ? 18 : data < 536.10 ? 19 : data < 537.01 ? 20 : '21');
      }
      
      layout[1] += ' ' + (data += typeof data == 'number' ? '.x' : /[.+]/.test(data) ? '' : '+');
      
      if (name == 'Safari' && (!version || parseInt(version) > 45)) {
        version = data;
      }
    }
    
    if (name == 'Opera' &&  (data = /(?:zbov|zvav)$/.exec(os))) {
      name += ' ';
      description.unshift('desktop mode');
      if (data == 'zvav') {
        name += 'Mini';
        version = null;
      } else {
        name += 'Mobile';
      }
    }
    
    else if (name == 'Safari' && /Chrome/.exec(layout[1])) {
      description.unshift('desktop mode');
      name = 'Chrome Mobile';
      version = null;
      if (/OS X/.test(os)) {
        manufacturer = 'Apple';
        os = 'iOS 4.3+';
      } else {
        os = null;
      }
    }
    
    if (version && version.indexOf((data = /[\d.]+$/.exec(os))) == 0 &&
        ua.indexOf('/' + data + '-') > -1) {
      os = trim(os.replace(data, ''));
    }
    
    if (layout && !/Avant|Nook/.test(name) && (
        /Browser|Lunascape|Maxthon/.test(name) ||
        /^(?:Adobe|Arora|Midori|Phantom|Rekonq|Rock|Sleipnir|Web)/.test(name) && layout[1])) {
      
      (data = layout[layout.length - 1]) && description.push(data);
    }
    
    if (description.length) {
      description = ['(' + description.join('; ') + ')'];
    }
    
    if (manufacturer && product && product.indexOf(manufacturer) < 0) {
      description.push('on ' + manufacturer);
    }
    
    if (product) {
      description.push((/^on /.test(description[description.length -1]) ? '' : 'on ') + product);
    }
    
    if (os) {
      data = / ([\d.+]+)$/.exec(os);
      os = {
        'architecture': 32,
        'family': data ? os.replace(data[0], '') : os,
        'version': data ? data[1] : null,
        'toString': function() {
          var version = this.version;
          return this.family + (version ? ' ' + version : '') + (this.architecture == 64 ? ' 64-bit' : '');
        }
      };
    }
    
    if ((data = /\b(?:AMD|IA|Win|WOW|x86_|x)64\b/i.exec(arch)) && !/\bi686\b/i.test(arch)) {
      if (os) {
        os.architecture = 64;
        os.family = os.family.replace(RegExp(' *' + data), '');
      }
      if (name && (/WOW64/i.test(ua) ||
          (useFeatures && /\w(?:86|32)$/.test(nav.cpuClass || nav.platform)))) {
        description.unshift('32-bit');
      }
    }
    ua || (ua = null);
    
    
    return {
      
      'version': name && version && (description.unshift(version), version),
      
      'name': name && (description.unshift(name), name),
      
      'os': os
        ? (name &&
            !(os == String(os).split(' ')[0] && (os == name.split(' ')[0] || product)) &&
              description.push(product ? '(' + os + ')' : 'on ' + os), os)
        : {
          
          'architecture': null,
          
          'family': null,
          
          'version': null,
          
          'toString': function() { return 'null'; }
        },
      
      'description': description.length ? description.join(' ') : ua,
      
      'layout': layout && layout[0],
      
      'manufacturer': manufacturer,
      
      'prerelease': prerelease,
      
      'product': product,
      
      'ua': ua,
      
      'parse': parse,
      
      'toString': toStringPlatform
    };
  }
  
  
  
  if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
    
    define(function() {
      return parse();
    });
  }
  
  else if (freeExports && !freeExports.nodeType) {
    
    forOwn(parse(), function(value, key) {
      freeExports[key] = value;
    });
  }
  
  else {
    root.platform = parse();
  }
}(this));
(function( w, undefined ){
	var doc = w.document,
		docElem = doc.documentElement,
		classtext = "overthrow-enabled",
		
		canBeFilledWithPoly = "ontouchmove" in doc,
		
		
		overflowProbablyAlreadyWorks =
			
			"WebkitOverflowScrolling" in docElem.style ||
			
			
			
			( !canBeFilledWithPoly && w.screen.width > 1200 ) ||
			
			
			
			(function(){
				var ua = w.navigator.userAgent,
					
					webkit = ua.match( /AppleWebKit\/([0-9]+)/ ),
					wkversion = webkit && webkit[1],
					wkLte534 = webkit && wkversion >= 534;
				return (
					
					ua.match( /Android ([0-9]+)/ ) && RegExp.$1 >= 3 && wkLte534 ||
					
					ua.match( / Version\/([0-9]+)/ ) && RegExp.$1 >= 0 && w.blackberry && wkLte534 ||
					
					ua.indexOf( /PlayBook/ ) > -1 && RegExp.$1 >= 0 && wkLte534 ||
					
					ua.match( /Fennec\/([0-9]+)/ ) && RegExp.$1 >= 4 ||
					
					ua.match( /wOSBrowser\/([0-9]+)/ ) && RegExp.$1 >= 233 && wkLte534 ||
					
					ua.match( /NokiaBrowser\/([0-9\.]+)/ ) && parseFloat(RegExp.$1) === 7.3 && webkit && wkversion >= 533
				);
			})(),
		
		
		
		defaultEasing = function (t, b, c, d) {
			return c*((t=t/d-1)*t*t + 1) + b;
		},
		enabled = false,
		// Keeper of intervals
		timeKeeper,
		/* toss scrolls and element with easing
		
		
			* left is the desired horizontal scroll. Default is "+0". For relative distances, pass a string with "+" or "-" in front.
			* top is the desired vertical scroll. Default is "+0". For relative distances, pass a string with "+" or "-" in front.
			* duration is the number of milliseconds the throw will take. Default is 100.
			* easing is an optional custom easing function. Default is w.overthrow.easing. Must follow the easing function signature
		*/
		toss = function( elem, options ){
			var i = 0,
				sLeft = elem.scrollLeft,
				sTop = elem.scrollTop,
				// Toss defaults
				o = {
					top: "+0",
					left: "+0",
					duration: 100,
					easing: w.overthrow.easing
				},
				endLeft, endTop;
			// Mixin based on predefined defaults
			if( options ){
				for( var j in o ){
					if( options[ j ] !== undefined ){
						o[ j ] = options[ j ];
					}
				}
			}
			// Convert relative values to ints
			// First the left val
			if( typeof o.left === "string" ){
				o.left = parseFloat( o.left );
				endLeft = o.left + sLeft;
			}
			else {
				endLeft = o.left;
				o.left = o.left - sLeft;
			}
			// Then the top val
			if( typeof o.top === "string" ){
				o.top = parseFloat( o.top );
				endTop = o.top + sTop;
			}
			else {
				endTop = o.top;
				o.top = o.top - sTop;
			}
			timeKeeper = setInterval(function(){
				if( i++ < o.duration ){
					elem.scrollLeft = o.easing( i, sLeft, o.left, o.duration );
					elem.scrollTop = o.easing( i, sTop, o.top, o.duration );
				}
				else{
					if( endLeft !== elem.scrollLeft ){
						elem.scrollLeft = endLeft;
					}
					if( endTop !== elem.scrollTop ){
						elem.scrollTop = endTop;
					}
					intercept();
				}
			}, 1 );
			// Return the values, post-mixin, with end values specified
			return { top: endTop, left: endLeft, duration: o.duration, easing: o.easing };
		},
		// find closest overthrow (elem or a parent)
		closest = function( target, ascend ){
			return !ascend && target.className && target.className.indexOf( "overthrow" ) > -1 && target || closest( target.parentNode );
		},
		// Intercept any throw in progress
		intercept = function(){
			clearInterval( timeKeeper );
		},
		// Enable and potentially polyfill overflow
		enable = function(){
			// If it's on,
			if( enabled ){
				return;
			}
			// It's on.
			enabled = true;
			// If overflowProbablyAlreadyWorks or at least the element canBeFilledWithPoly, add a class to cue CSS that assumes overflow scrolling will work (setting height on elements and such)
			if( overflowProbablyAlreadyWorks || canBeFilledWithPoly ){
				docElem.className += " " + classtext;
			}
			// Destroy everything later. If you want to.
			w.overthrow.forget = function(){
				// Strip the class name from docElem
				docElem.className = docElem.className.replace( classtext, "" );
				// Remove touch binding (check for method support since this part isn't qualified by touch support like the rest)
				if( doc.removeEventListener ){
					doc.removeEventListener( "touchstart", start, false );
				}
				// reset easing to default
				w.overthrow.easing = defaultEasing;
				// Let 'em know
				enabled = false;
			};
			// If overflowProbablyAlreadyWorks or it doesn't look like the browser canBeFilledWithPoly, our job is done here. Exit viewport left.
			if( overflowProbablyAlreadyWorks || !canBeFilledWithPoly ){
				return;
			}
			// Fill 'er up!
			// From here down, all logic is associated with touch scroll handling
				// elem references the overthrow element in use
			var elem,
				// The last several Y values are kept here
				lastTops = [],
				// The last several X values are kept here
				lastLefts = [],
				// lastDown will be true if the last scroll direction was down, false if it was up
				lastDown,
				// lastRight will be true if the last scroll direction was right, false if it was left
				lastRight,
				// For a new gesture, or change in direction, reset the values from last scroll
				resetVertTracking = function(){
					lastTops = [];
					lastDown = null;
				},
				resetHorTracking = function(){
					lastLefts = [];
					lastRight = null;
				},
				// After releasing touchend, throw the overthrow element, depending on momentum
				finishScroll = function(){
					// Come up with a distance and duration based on how
					// Multipliers are tweaked to a comfortable balance across platforms
					var top = ( lastTops[ 0 ] - lastTops[ lastTops.length -1 ] ) * 8,
						left = ( lastLefts[ 0 ] - lastLefts[ lastLefts.length -1 ] ) * 8,
						duration = Math.max( Math.abs( left ), Math.abs( top ) ) / 8;
					
					top = ( top > 0 ? "+" : "" ) + top;
					left = ( left > 0 ? "+" : "" ) + left;
					
					if( !isNaN( duration ) && duration > 0 && ( Math.abs( left ) > 80 || Math.abs( top ) > 80 ) ){
						toss( elem, { left: left, top: top, duration: duration } );
					}
				},
				
				
				
				
				inputs,
				setPointers = function( val ){
					inputs = elem.querySelectorAll( "textarea, input" );
					for( var i = 0, il = inputs.length; i < il; i++ ) {
						inputs[ i ].style.pointerEvents = val;
					}
				},
				
				changeScrollTarget = function( startEvent, ascend ){
					if( doc.createEvent ){
						var newTarget = ( !ascend || ascend === undefined ) && elem.parentNode || elem.touchchild || elem,
							tEnd;
						if( newTarget !== elem ){
							tEnd = doc.createEvent( "HTMLEvents" );
							tEnd.initEvent( "touchend", true, true );
							elem.dispatchEvent( tEnd );
							newTarget.touchchild = elem;
							elem = newTarget;
							newTarget.dispatchEvent( startEvent );
						}
					}
				},
				
				
				
				start = function( e ){
					
					intercept();
					
					resetVertTracking();
					resetHorTracking();
					elem = closest( e.target );
					if( !elem || elem === docElem || e.touches.length > 1 ){
						return;
					}
					setPointers( "none" );
					var touchStartE = e,
						scrollT = elem.scrollTop,
						scrollL = elem.scrollLeft,
						height = elem.offsetHeight,
						width = elem.offsetWidth,
						startY = e.touches[ 0 ].pageY,
						startX = e.touches[ 0 ].pageX,
						scrollHeight = elem.scrollHeight,
						scrollWidth = elem.scrollWidth,
						
						move = function( e ){
							var ty = scrollT + startY - e.touches[ 0 ].pageY,
								tx = scrollL + startX - e.touches[ 0 ].pageX,
								down = ty >= ( lastTops.length ? lastTops[ 0 ] : 0 ),
								right = tx >= ( lastLefts.length ? lastLefts[ 0 ] : 0 );
							
							if( ( ty > 0 && ty < scrollHeight - height ) || ( tx > 0 && tx < scrollWidth - width ) ){
								e.preventDefault();
							}
							
							else {
								changeScrollTarget( touchStartE );
							}
							
							if( lastDown && down !== lastDown ){
								resetVertTracking();
							}
							
							if( lastRight && right !== lastRight ){
								resetHorTracking();
							}
							
							lastDown = down;
							lastRight = right;
							
							elem.scrollTop = ty;
							elem.scrollLeft = tx;
							lastTops.unshift( ty );
							lastLefts.unshift( tx );
							if( lastTops.length > 3 ){
								lastTops.pop();
							}
							if( lastLefts.length > 3 ){
								lastLefts.pop();
							}
						},
						
						end = function( e ){
							
							finishScroll();
							
							setPointers( "auto" );
							setTimeout( function(){
								setPointers( "none" );
							}, 450 );
							elem.removeEventListener( "touchmove", move, false );
							elem.removeEventListener( "touchend", end, false );
						};
					elem.addEventListener( "touchmove", move, false );
					elem.addEventListener( "touchend", end, false );
				};
			
			doc.addEventListener( "touchstart", start, false );
		};
	
	w.overthrow = {
		set: enable,
		forget: function(){},
		easing: defaultEasing,
		toss: toss,
		intercept: intercept,
		closest: closest,
		support: overflowProbablyAlreadyWorks ? "native" : canBeFilledWithPoly && "polyfilled" || "none"
	};
	
	enable();
})( this );
(function (win) {
    document.addEventListener('DOMContentLoaded', function() {
        var style = document.createElement('style');
        style.innerHTML = [
            '.slides, .slides .slide {',
                '-webkit-transform: translate3D(0, 0, 0);',
            '}',
            '.slide {',
                'float: left;',
            '}',
            '.slide-image {',
                'background-repeat: no-repeat;',
                'background-size: cover;',
                'background-position: center center;',
            '}'
        ].join('');
        style.id = 'touchslider-style';
        document.head.appendChild(style);
        document.removeEventListener('DOMContentLoaded', arguments.callee, false);
    }, false);
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    function bezier_for_velocity(v) {
        var importance = 0.5,
            x = (v < 0 ? -1 : 1) * Math.sqrt(importance * importance * (v * v / (1 + v * v))),
            t = x / v,
            sameness = t;
        return 'cubic-bezier(' + [t, x, sameness, 1.0].join(", ") + ')';
    }
    function css(elements, props) {
        if (typeof elements !== 'object' ||
            typeof elements.length === 'undefined') {
            elements = [elements];
        }
        var i = elements.length;
        while (i--) {
            for (var p in props) {
                elements[i].style[p] = props[p];
            }
        }
        return true;
    }
    win.touchSlider = function (config) {
        var x, t,
            initial_x, initial_offset,
            previous_x, previous_t,
            direction = {},
            events = {},
            event_added = true,
            current_slide = 0,
            current_offset = 0,
            self = this,
            el = (typeof config.el === 'string' ? document.querySelector(config.el) : config.el),
            slides = el.querySelector('.slides'),
            slideEl = el.querySelectorAll('.slide'),
            animation = {},
            default_duration = config.duration || 500,
            slide_width = config.width || el.offsetWidth,
            previousWidth = slide_width,
            slide_height = config.height || '200px',
            last_slide = slideEl.length - 1,
            left_edge = 0,
            right_edge = slide_width * last_slide;
        el.style.overflow = 'hidden';
        css(el, {
            height: slide_height
        });
        css(slides, {
            height: slide_height
        });
        css(slideEl, {
            height: slide_height
        });
        css(slides, {
            'width': (slide_width * slideEl.length) + 'px',
            '-webkit-transition-property': '-webkit-transform',
            '-webkit-transform': 'translate3D(0, 0, 0)'
        });
        
        css(slideEl, {
            'width': slide_width + 'px',
            '-webkit-transform': 'translate3D(0, 0, 0)'
        });
        events.ontouchstart = function (e) {
            if (e.touches.length !== 1) {
                return;
            }
            direction = {
                x: e.touches[0].clientX,
                y: e.touches[0].clientY
            };
            
            
            
            current_offset = 0 - Number(window.getComputedStyle(slides)['-webkit-transform'].split(", ")[4]);
            css(slides, {
                
                '-webkit-transition-duration': '0s',
                '-webkit-transform': 'translate3D(' + (0 - current_offset) + 'px, 0, 0)'
            });
            initial_offset = current_offset;
            initial_x = previous_x = x = e.touches[0].clientX;
            previous_t = t = new Date();
            if (typeof config.userslide === 'function') {
                config.userslide();
            }
        }
        el.addEventListener('touchstart', events.ontouchstart, false);
        events.onresize = function (e) {
            slide_width = el.offsetWidth;
            if (slide_width === previousWidth) {
                return;
            }
            previousWidth = slide_width;
            current_offset = current_slide * slide_width;
            right_edge = slide_width * last_slide;
            css(slides, {
                'width': (slide_width * (last_slide + 1)) + 'px',
                '-webkit-transition-duration': '0s',
                '-webkit-transform': 'translate3D(' + (0 - current_offset) + 'px, 0, 0)'
            });
            
            css(slideEl, {
                'width': slide_width + 'px'
            });
            
        }
        win.addEventListener('resize', events.onresize, false);
        events.ontouchmove = function (e) {
            
            
            if (e.touches.length !== 1) {
                return;
            } else {
                if (direction.x || direction.y) {
                    var diffx = Math.abs(direction.x - e.touches[0].clientX);
                    var diffy = Math.abs(direction.y - e.touches[0].clientY);
                    if (diffx > diffy) {
                        e.preventDefault();
                    } else {
                        return;
                    }
                }
                
            }
            previous_x = x;
            previous_t = t;
            x = e.touches[0].clientX;
            t = new Date();
            current_offset = initial_offset + (initial_x - x);
            
            if (current_offset < left_edge) {
                current_offset = current_offset / 2;
            } else if (current_offset > right_edge) {
                current_offset = right_edge + (current_offset - right_edge) / 2;
            }
            css(slides, {
                '-webkit-transform': 'translate3D(' + (0 - current_offset) + 'px, 0, 0)'
            });
        }
        el.addEventListener('touchmove', events.ontouchmove, false);
        events.ontouchend = function (e) {
            var target_slide, target_distance, velocity, final_destination;
            direction = {};
            final_destination = current_offset;
            
            
            if (Math.abs(t - previous_t) > 1) {
                final_destination += Math.min(slide_width / 2, Math.max(-slide_width / 2,
                                        default_duration * (previous_x - x) / (t - previous_t)
                                     ));
            }
            target_slide = Math.round(final_destination / slide_width);
            if (target_slide < 0) {
                target_slide = 0;
            } else if (target_slide > last_slide) {
                target_slide = last_slide;
            }
            target_distance = current_offset - target_slide * slide_width;
            
            if (Math.abs(x - previous_x) < 1 || Math.abs(t - previous_t) < 1) {
                velocity = 0.1;
            } else {
                velocity = ((x - previous_x) / (t - previous_t)) * (default_duration / target_distance);
            }
            slideTo({
                slide: target_slide,
                bezier: bezier_for_velocity(velocity)
            });
        }
        el.addEventListener('touchend', events.ontouchend, false);
        function slideTo(opts) {
            var target_offset = opts.slide * slide_width;
            animation = {
                slide: opts.slide,
                
                duration: Math.max(typeof opts.duration === 'undefined' ? default_duration : opts.duration, 1),
                bezier: opts.bezier || bezier_for_velocity(0.1)
            };
            css(slides, {
                '-webkit-transition-timing-function': animation.bezier,
                '-webkit-transition-duration': animation.duration + 'ms',
                '-webkit-transform': 'translate3D(' + (0 - target_offset) + 'px, 0, 0)'
            });
            current_slide = opts.slide;
            if (typeof config.afterslide === 'function') {
                animation.timeout = win.setTimeout(function() {
                    config.afterslide(animation);
                }, animation.duration);
            }
            
        }
        function pause() {
            if (event_added === false) {
                return false;
            }
            el.removeEventListener('touchstart', events.ontouchstart, false);
            el.removeEventListener('touchmove', events.ontouchmove, false);
            el.removeEventListener('touchend', events.ontouchend, false);
            win.removeEventListener('resize', events.onresize, false);
            event_added = false;
        }
        function resume() {
            if (event_added === true) {
                return false;
            }
            el.addEventListener('touchstart', events.ontouchstart, false);
            el.addEventListener('touchmove', events.ontouchmove, false);
            el.addEventListener('touchend', events.ontouchend, false);
            win.addEventListener('resize', events.onresize, false);
            event_added = true;
        }
        return {
            getCurrent: function() {
                return current_slide;
            },
            getLength: function() {
                return (last_slide + 1);
            },
            go: slideTo,
            next: function() {
                if (current_slide === last_slide) {
                    slideTo({
                        slide: 0
                    });
                } else {
                    slideTo({
                        slide: (current_slide + 1)
                    });
                }
            },
            pause: pause,
            resume: resume
        };
    };
}(window));
this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};
Handlebars.registerPartial("cssClass", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.cssClass) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.cssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
Handlebars.registerPartial("for_id", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " for=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
Handlebars.registerPartial("id", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
Handlebars.registerPartial("items", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, options, self=this, helperMissing=helpers.helperMissing, functionType="function", blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var stack1, stack2, options;
  options = {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.safe || (depth0 && depth0.safe)),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "safe", depth0, options));
  if(stack2 || stack2 === 0) { return stack2; }
  else { return ''; }
  }
function program2(depth0,data) {
  
  var buffer = "";
  return buffer;
  }
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.items) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.items); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.items) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
Handlebars.registerPartial("style", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
this["Handlebars"]["templates"]["alert"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " floatingpanel-overlay-";
  if (stack1 = helpers.overlay) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.overlay); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1);
  return buffer;
  }
function program3(depth0,data) {
  
  
  return " floatingpanel-overlay-default";
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-overlay\"";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += "ui-corner-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "";
  buffer += " ui-body-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program11(depth0,data) {
  
  
  return " ui-overlay-shadow";
  }
function program13(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2;
  buffer += "<div class=\"ui-btn-";
  if (stack1 = helpers.close) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.close); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + " ui-btn";
  stack2 = ((stack1 = ((stack1 = (depth1 && depth1.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.programWithDepth(14, program14, data, depth0),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = ((stack1 = ((stack1 = (depth1 && depth1.shadow)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ui-btn-corner-all ui-btn-icon-notext\"";
  stack2 = ((stack1 = ((stack1 = (depth1 && depth1.id)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "><span class=\"ui-btn-inner\"><span class=\"ui-btn-text\">Close</span><span class=\"ui-icon ui-icon-delete ui-icon-shadow\">&nbsp;</span></span></div>";
  return buffer;
  }
function program14(depth0,data,depth1) {
  
  var buffer = "", stack1, options;
  buffer += " ui-btn-up-";
  options = {hash:{
    'default': (depth0)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.get || (depth0 && depth0.get)),stack1 ? stack1.call(depth0, ((stack1 = (depth1 && depth1.header)),stack1 == null || stack1 === false ? stack1 : stack1.ui), options) : helperMissing.call(depth0, "get", ((stack1 = (depth1 && depth1.header)),stack1 == null || stack1 === false ? stack1 : stack1.ui), options)));
  return buffer;
  }
function program16(depth0,data) {
  
  
  return " ui-shadow";
  }
function program18(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-close\"";
  return buffer;
  }
function program20(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<div class=\"ui-corner-top ui-header";
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(21, program21, data, depth0),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  stack2 = ((stack1 = ((stack1 = ((stack1 = (depth0 && depth0.header)),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div>";
  return buffer;
  }
function program21(depth0,data,depth1) {
  
  var buffer = "", stack1, options;
  buffer += " ui-bar-";
  options = {hash:{
    'default': (depth0)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.get || (depth0 && depth0.get)),stack1 ? stack1.call(depth0, ((stack1 = (depth1 && depth1.header)),stack1 == null || stack1 === false ? stack1 : stack1.ui), options) : helperMissing.call(depth0, "get", ((stack1 = (depth1 && depth1.header)),stack1 == null || stack1 === false ? stack1 : stack1.ui), options)));
  return buffer;
  }
function program23(depth0,data) {
  
  var buffer = "";
  buffer += "<h1 class=\"ui-title\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h1>";
  return buffer;
  }
  buffer += "<div class=\"floatingpanel-overlay";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.overlay), {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "></div><div class=\"ui-popup-container ui-popup-active\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "><div class=\"";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.corner) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.corner); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.corner) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ui-popup";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack1 = helpers.shadow) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.shadow); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.shadow) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" style=\"border: 0px !important;\">";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.close), {hash:{},inverse:self.noop,fn:self.programWithDepth(13, program13, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.header), {hash:{},inverse:self.noop,fn:self.program(20, program20, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<div class=\"ui-corner-bottom ui-content";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div></div></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["popup"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;
function program1(depth0,data) {
  
  
  return " hidden";
  }
function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " floatingpanel-overlay-";
  if (stack1 = helpers.overlay) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.overlay); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1);
  return buffer;
  }
function program5(depth0,data) {
  
  
  return " floatingpanel-overlay-default";
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-overlay\"";
  return buffer;
  }
function program9(depth0,data) {
  
  
  return " ui-overlay-shadow";
  }
function program11(depth0,data) {
  
  var buffer = "";
  buffer += " ui-corner-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program13(depth0,data) {
  
  var buffer = "";
  buffer += "margin-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + ": 0px;";
  return buffer;
  }
function program15(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }
function program17(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2;
  buffer += "<div class=\"ui-header";
  stack2 = ((stack1 = ((stack1 = (depth1 && depth1.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  stack2 = ((stack1 = ((stack1 = ((stack1 = (depth0 && depth0.header)),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(20, program20, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.header)),stack1 == null || stack1 === false ? stack1 : stack1.close), {hash:{},inverse:self.noop,fn:self.programWithDepth(22, program22, data, depth1),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div>";
  return buffer;
  }
function program18(depth0,data) {
  
  var buffer = "";
  buffer += " ui-bar-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program20(depth0,data) {
  
  var buffer = "";
  buffer += "<h1 class=\"ui-title\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h1>";
  return buffer;
  }
function program22(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2;
  buffer += "<a href=\"javascript:;\" class=\"ui-btn-"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.header)),stack1 == null || stack1 === false ? stack1 : stack1.close)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " ui-btn";
  stack2 = ((stack1 = ((stack1 = (depth2 && depth2.shadow)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = ((stack1 = ((stack1 = (depth2 && depth2.corner)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(25, program25, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ui-btn-icon-notext";
  stack2 = ((stack1 = ((stack1 = (depth2 && depth2.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(27, program27, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  stack2 = ((stack1 = ((stack1 = (depth2 && depth2.id)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(29, program29, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "><span class=\"ui-btn-inner\"><span class=\"ui-btn-text\">Close</span><span class=\"ui-icon ui-icon-delete ui-icon-shadow\">&nbsp;</span></span></a>";
  return buffer;
  }
function program23(depth0,data) {
  
  
  return " ui-shadow";
  }
function program25(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-corner-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program27(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-up-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program29(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-close\"";
  return buffer;
  }
function program31(depth0,data) {
  
  var buffer = "";
  buffer += " ui-body-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
  buffer += "<div class=\"floatingpanel-overlay";
  options = {hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.hidden), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hidden), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.overlay), {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.id); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.id) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "></div><div class=\"ui-dialog-contain";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack2 = helpers.shadow) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.shadow); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.shadow) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack2 = helpers.corner) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.corner); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.corner) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.hidden), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hidden), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" style=\"z-index: 10000;";
  options = {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data};
  if (stack2 = helpers.actionSheet) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.actionSheet); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.actionSheet) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data};
  if (stack2 = helpers.style) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.style); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.style) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  stack2 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ">";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.header), {hash:{},inverse:self.noop,fn:self.programWithDepth(17, program17, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<div class=\"ui-content";
  options = {hash:{},inverse:self.noop,fn:self.program(31, program31, data),data:data};
  if (stack2 = helpers.ui) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.ui); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.ui) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  stack2 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["page"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " ui-body-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
  buffer += "<div class=\"ui-page ui-page-active";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.cssClass) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.cssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["container"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, self=this, functionType="function", blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  
  return " hidden";
  }
  buffer += "<div class=\"container";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.hidden) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.hidden); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["footer"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " ui-bar-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " ui-footer-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
  buffer += "<div class=\"ui-footer";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.position) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.position); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.position) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["tabs"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  
  return " ui-mini";
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<li class=\"ui-block-";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getIndexLetter || (depth0 && depth0.getIndexLetter)),stack1 ? stack1.call(depth0, ((stack1 = data),stack1 == null || stack1 === false ? stack1 : stack1.index), options) : helperMissing.call(depth0, "getIndexLetter", ((stack1 = data),stack1 == null || stack1 === false ? stack1 : stack1.index), options)))
    + "\">";
  options = {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.safe || (depth0 && depth0.safe)),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "safe", depth0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</li>";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "";
  return buffer;
  }
  buffer += "<div class=\"ui-navbar";
  options = {hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.mini), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.mini), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  stack2 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.id); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.id) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "><ul class=\"ui-grid-";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getLengthLetter || (depth0 && depth0.getLengthLetter)),stack1 ? stack1.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "getLengthLetter", (depth0 && depth0.items), options)))
    + "\">";
  stack2 = helpers.each.call(depth0, (depth0 && depth0.items), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</ul></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["button"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-up-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program3(depth0,data) {
  
  
  return " ui-shadow";
  }
function program5(depth0,data) {
  
  
  return " hidden";
  }
function program7(depth0,data) {
  
  
  return " ui-disabled";
  }
function program9(depth0,data) {
  
  
  return " ui-btn-corner-all";
  }
function program11(depth0,data) {
  
  
  return " ui-mini";
  }
function program13(depth0,data) {
  
  
  return " ui-mini ui-btn-icon-notext";
  }
function program15(depth0,data) {
  
  
  return " ui-btn-inline";
  }
function program17(depth0,data) {
  
  var stack1;
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.iconPos), {hash:{},inverse:self.program(20, program20, data),fn:self.program(18, program18, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }
function program18(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " ui-btn-icon-";
  if (stack1 = helpers.iconPos) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.iconPos); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1);
  return buffer;
  }
function program20(depth0,data) {
  
  
  return " ui-btn-icon-left";
  }
function program22(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program24(depth0,data) {
  
  var buffer = "";
  buffer += " target=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program26(depth0,data) {
  
  var buffer = "";
  buffer += " href=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program28(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.text); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  return escapeExpression(stack1);
  }
function program30(depth0,data) {
  
  
  return "&nbsp;";
  }
function program32(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<span class=\"";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.iconCssClass), {hash:{},inverse:self.program(35, program35, data),fn:self.program(33, program33, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " override-inline-btn-icon\">";
  options = {hash:{},inverse:self.noop,fn:self.program(37, program37, data),data:data};
  stack2 = ((stack1 = helpers.geticon || (depth0 && depth0.geticon)),stack1 ? stack1.call(depth0, (depth0 && depth0.icon), options) : helperMissing.call(depth0, "geticon", (depth0 && depth0.icon), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>";
  return buffer;
  }
function program33(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.iconCssClass) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.iconCssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  return escapeExpression(stack1);
  }
function program35(depth0,data) {
  
  
  return "icon";
  }
function program37(depth0,data) {
  
  var buffer = "";
  return buffer;
  }
  buffer += "<a class=\"ui-btn";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.shadow) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.shadow); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.shadow) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack1 = helpers.hidden) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.hidden); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.noop,fn:self.program(9, program9, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.roundCorners), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.roundCorners), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': ("medium")
  },inverse:self.noop,fn:self.program(11, program11, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.size), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.size), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': ("small")
  },inverse:self.noop,fn:self.program(13, program13, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.size), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.size), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.noop,fn:self.program(15, program15, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.inline), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.inline), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.icon), {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data};
  if (stack2 = helpers.style) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.style); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.style) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(24, program24, data),data:data};
  if (stack2 = helpers.target) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.target); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.target) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(26, program26, data),data:data};
  if (stack2 = helpers.href) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.href); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.href) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "><span class=\"ui-btn-inner\"><span class=\"ui-btn-text\">";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.text), {hash:{},inverse:self.program(30, program30, data),fn:self.program(28, program28, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.icon), {hash:{},inverse:self.noop,fn:self.program(32, program32, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span></a>";
  return buffer;
  });
this["Handlebars"]["templates"]["tabbutton"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, options;
  buffer += " ui-btn-icon-";
  options = {hash:{
    'default': ("top"),
    'choices': ("top|bottom|left|right")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.get || (depth1 && depth1.get)),stack1 ? stack1.call(depth0, (depth1 && depth1.iconPos), options) : helperMissing.call(depth0, "get", (depth1 && depth1.iconPos), options)));
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-up-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += "<span class=\"ui-btn-text\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<span class=\"icon override-tab-icon\">";
  options = {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data};
  stack2 = ((stack1 = helpers.geticon || (depth0 && depth0.geticon)),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "geticon", depth0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>";
  return buffer;
  }
function program10(depth0,data) {
  
  var buffer = "";
  return buffer;
  }
  buffer += "<a class=\"ui-btn ui-btn-inline";
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data};
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.icon); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "><span class=\"ui-btn-inner\">";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.text) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.text); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.text) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.icon); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span></a>";
  return buffer;
  });
this["Handlebars"]["templates"]["header"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " ui-bar-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += " ui-header-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "";
  buffer += "<h1 class=\"ui-title\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h1>";
  return buffer;
  }
  buffer += "<div class=\"ui-header";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.cssClass) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.cssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack1 = helpers.position) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.position); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.position) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.title); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.title) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["content"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;
  buffer += "<div class=\"ui-content";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["textfield"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, self=this, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  
  return "<div class=\"ui-field-contain ui-body ui-br\">";
  }
function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<label class=\"ui-input-text\"";
  stack1 = self.invokePartial(partials.for_id, 'for_id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</label>";
  return buffer;
  }
function program5(depth0,data) {
  
  
  return "ui-input-search";
  }
function program7(depth0,data) {
  
  
  return "ui-input-text";
  }
function program9(depth0,data) {
  
  
  return " ui-shadow-inset";
  }
function program11(depth0,data) {
  
  
  return "btn-";
  }
function program13(depth0,data) {
  
  var buffer = "";
  buffer += " ui-body-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program15(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program17(depth0,data) {
  
  var buffer = "";
  buffer += " ui-icon-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "field";
  return buffer;
  }
function program19(depth0,data) {
  
  
  return " ui-mini";
  }
function program21(depth0,data) {
  
  
  return " ui-disabled";
  }
function program23(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.type) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.type); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  return escapeExpression(stack1);
  }
function program25(depth0,data) {
  
  
  return "text";
  }
function program27(depth0,data) {
  
  var buffer = "";
  buffer += " autocomplete=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program29(depth0,data) {
  
  var buffer = "";
  buffer += " name=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program31(depth0,data) {
  
  var buffer = "";
  buffer += " pattern=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program33(depth0,data) {
  
  var buffer = "";
  buffer += " value=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program35(depth0,data) {
  
  var buffer = "";
  buffer += " placeholder=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program37(depth0,data) {
  
  var buffer = "";
  buffer += " mobile-textinput-disabled ui-state-disabled";
  return buffer;
  }
function program39(depth0,data) {
  
  
  return " disabled=\"disabled\"";
  }
function program41(depth0,data) {
  
  
  return " required";
  }
function program43(depth0,data) {
  
  
  return " readonly";
  }
function program45(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "<a href=\"javascript:;\" class=\"ui-input-clear ui-btn";
  options = {hash:{},inverse:self.noop,fn:self.program(46, program46, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ui-shadow ui-btn-corner-all ui-fullsize ui-btn-icon-notext";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.value), {hash:{},inverse:self.program(49, program49, data),fn:self.program(48, program48, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(51, program51, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "><span class=\"ui-btn-inner\"><span class=\"ui-btn-text\">clear text</span><span class=\"ui-icon ui-icon-delete ui-icon-shadow\">&nbsp;</span></span></a>";
  return buffer;
  }
function program46(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-up-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program48(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  options = {hash:{
    'compare': (((stack1 = (depth0 && depth0.value)),stack1 == null || stack1 === false ? stack1 : stack1.length)),
    'default': (1)
  },inverse:self.noop,fn:self.program(49, program49, data),data:data};
  stack2 = ((stack1 = helpers.if_gteq || (depth0 && depth0.if_gteq)),stack1 ? stack1.call(depth0, (depth0 && depth0.clearBtn), options) : helperMissing.call(depth0, "if_gteq", (depth0 && depth0.clearBtn), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  }
function program49(depth0,data) {
  
  var buffer = "";
  buffer += " ui-input-clear-hidden";
  return buffer;
  }
function program51(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-clearbtn\"";
  return buffer;
  }
function program53(depth0,data) {
  
  
  return "</div>";
  }
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.labelInline) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.labelInline); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.labelInline) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.label); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<div class=\"";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.icon), {hash:{},inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.shadow) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.shadow); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.shadow) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ui-";
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.icon); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "corner-all ui-btn-shadow";
  options = {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data};
  if (stack1 = helpers.cssClass) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.cssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data};
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.icon); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{
    'compare': ("small"),
    'default': ("medium")
  },inverse:self.noop,fn:self.program(19, program19, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.size), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.size), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(21, program21, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"><input type=\"";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.type), {hash:{},inverse:self.program(25, program25, data),fn:self.program(23, program23, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(27, program27, data),data:data};
  if (stack2 = helpers.autocomplete) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.autocomplete); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.autocomplete) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(29, program29, data),data:data};
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.name); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.name) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(31, program31, data),data:data};
  if (stack2 = helpers.pattern) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.pattern); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.pattern) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(33, program33, data),data:data};
  if (stack2 = helpers.value) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.value); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.value) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(35, program35, data),data:data};
  if (stack2 = helpers.placeHolder) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.placeHolder); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.placeHolder) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "class=\"ui-input-text";
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(37, program37, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data};
  if (stack2 = helpers.ui) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.ui); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.ui) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(39, program39, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(41, program41, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.required), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.required), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(43, program43, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.readonly), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.readonly), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ">";
  options = {hash:{
    'compare': (false),
    'default': (false)
  },inverse:self.noop,fn:self.program(45, program45, data),data:data};
  stack2 = ((stack1 = helpers.if_neq || (depth0 && depth0.if_neq)),stack1 ? stack1.call(depth0, (depth0 && depth0.clearBtn), options) : helperMissing.call(depth0, "if_neq", (depth0 && depth0.clearBtn), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div>";
  options = {hash:{},inverse:self.noop,fn:self.program(53, program53, data),data:data};
  if (stack2 = helpers.labelInline) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.labelInline); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.labelInline) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  });
this["Handlebars"]["templates"]["listbuffered"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;
function program1(depth0,data) {
  
  
  return " ui-listview-inset";
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " ui-listview-corner-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program5(depth0,data) {
  
  
  return " ui-shadow";
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program13(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<li class=\"ui-btn ui-li";
  stack2 = ((stack1 = ((stack1 = (depth1 && depth1.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  options = {hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data};
  stack2 = ((stack1 = helpers.safe || (depth0 && depth0.safe)),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "safe", depth0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</li>";
  return buffer;
  }
function program14(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-up-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program16(depth0,data) {
  
  var buffer = "";
  return buffer;
  }
function program18(depth0,data) {
  
  var buffer = "";
  buffer += " list-loader-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
  buffer += "<ul class=\"ui-listview";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.inset) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.inset); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.inset) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.corner) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.corner); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.corner) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack1 = helpers.shadow) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.shadow); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.shadow) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.cssClass) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.cssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.items), {hash:{},inverse:self.noop,fn:self.programWithDepth(13, program13, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</ul><div class=\"list-loader";
  options = {hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"><a class=\"list-loader-text\">Loading...</a></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["list"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;
function program1(depth0,data) {
  
  
  return " ui-listview-inset";
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " ui-corner-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program5(depth0,data) {
  
  
  return " ui-shadow";
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-loader\"";
  return buffer;
  }
function program13(depth0,data) {
  
  
  return " list-loader-hide";
  }
function program15(depth0,data) {
  
  var buffer = "";
  buffer += " list-loader-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
  buffer += "<ul class=\"ui-listview";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.inset) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.inset); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.inset) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.corner) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.corner); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.corner) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack1 = helpers.shadow) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.shadow); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.shadow) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.cssClass) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.cssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "></ul><div ";
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"list-loader";
  options = {hash:{
    'compare': (false),
    'defaults': (true)
  },inverse:self.noop,fn:self.program(13, program13, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.isLoading), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.isLoading), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data};
  if (stack2 = helpers.ui) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.ui); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.ui) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"><a class=\"list-loader-text\">Loading...</a></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["floatingpanel"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  
  return " hidden";
  }
function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " floatingpanel-overlay-";
  if (stack1 = helpers.overlay) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.overlay); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1);
  return buffer;
  }
function program5(depth0,data) {
  
  
  return " floatingpanel-overlay-default";
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-overlay\"";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "";
  buffer += " arrow-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "";
  buffer += " floatingpanel-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program13(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program15(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program17(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  options = {hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data};
  stack2 = ((stack1 = helpers.safe || (depth0 && depth0.safe)),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "safe", depth0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  }
function program18(depth0,data) {
  
  var buffer = "";
  return buffer;
  }
  buffer += "<div class=\"floatingpanel-overlay";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.hidden) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.hidden); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.overlay), {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "></div><div class=\"floatingpanel";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.arrow) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.arrow); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.arrow) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.hidden) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.hidden); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options = {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data};
  if (stack1 = helpers.items) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.items); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.items) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["collapsible"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " ui-collapsible-inset";
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " ui-corner-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += " ui-collapsible-collapsed";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " ui-collapsible-heading-collapsed";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-header\"";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "";
  buffer += " ui-mini";
  return buffer;
  }
function program13(depth0,data) {
  
  var buffer = "";
  buffer += " ui-fullsize";
  return buffer;
  }
function program15(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " ui-btn-up-"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.header)),stack1 == null || stack1 === false ? stack1 : stack1.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1));
  return buffer;
  }
function program17(depth0,data) {
  
  var buffer = "", stack1, options;
  options = {hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program18(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-up-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program20(depth0,data) {
  
  var buffer = "", stack1, options;
  options = {hash:{
    'default': ("plus")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.get || (depth0 && depth0.get)),stack1 ? stack1.call(depth0, (depth0 && depth0.iconCollapsed), options) : helperMissing.call(depth0, "get", (depth0 && depth0.iconCollapsed), options)));
  return buffer;
  }
function program22(depth0,data) {
  
  var buffer = "", stack1, options;
  options = {hash:{
    'default': ("minus")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.get || (depth0 && depth0.get)),stack1 ? stack1.call(depth0, (depth0 && depth0.iconExpanded), options) : helperMissing.call(depth0, "get", (depth0 && depth0.iconExpanded), options)));
  return buffer;
  }
function program24(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " ui-body-"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.content)),stack1 == null || stack1 === false ? stack1 : stack1.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1));
  return buffer;
  }
function program26(depth0,data) {
  
  var buffer = "", stack1, options;
  options = {hash:{},inverse:self.noop,fn:self.program(27, program27, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program27(depth0,data) {
  
  var buffer = "";
  buffer += " ui-body-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program29(depth0,data) {
  
  var buffer = "";
  buffer += " ui-collapsible-content-collapsed";
  return buffer;
  }
function program31(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-content\"";
  return buffer;
  }
  buffer += "<div class=\"ui-collapsible";
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.inset), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.inset), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack2 = helpers.corner) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.corner); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.corner) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ui-collapsible-themed-content";
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(5, program5, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.collapsed), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.collapsed), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  stack2 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "><h4 class=\"ui-collapsible-heading";
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.collapsed), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.collapsed), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.id); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.id) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "><a href=\"javascript:;\" class=\"ui-collapsible-heading-toggle ui-btn";
  options = {hash:{
    'compare': ("small"),
    'default': ("medium")
  },inverse:self.program(13, program13, data),fn:self.program(11, program11, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.size), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.size), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.header)),stack1 == null || stack1 === false ? stack1 : stack1.ui), {hash:{},inverse:self.program(17, program17, data),fn:self.program(15, program15, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ui-btn-icon-";
  options = {hash:{
    'default': ("left"),
    'choices': ("left|right")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.get || (depth0 && depth0.get)),stack1 ? stack1.call(depth0, (depth0 && depth0.iconPos), options) : helperMissing.call(depth0, "get", (depth0 && depth0.iconPos), options)))
    + "\"><span class=\"ui-btn-inner\"><span class=\"ui-btn-text\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.header)),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span><span class=\"ui-icon ui-icon-shadow ui-icon-";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.collapsed), {hash:{},inverse:self.program(22, program22, data),fn:self.program(20, program20, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">&nbsp;</span></span></a></h4><div class=\"ui-collapsible-content";
  stack2 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.content)),stack1 == null || stack1 === false ? stack1 : stack1.ui), {hash:{},inverse:self.program(26, program26, data),fn:self.program(24, program24, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(29, program29, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.collapsed), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.collapsed), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(31, program31, data),data:data};
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.id); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.id) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ">";
  stack2 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["carousel"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += "<div class=\"slide slide-image\" style=\"background-image: url("
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + ");\"></div>";
  return buffer;
  }
  buffer += "<div class=\"slider\"";
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "><div class=\"slides\">";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.images), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["loadmask"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " loadmask-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " loadmask-fullscreen";
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += " loadmask-no-fullscreen";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "";
  buffer += " loadmask-msg-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
  buffer += "<div class=\"loadmask";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.fullscreen), {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.id); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "><div class=\"loadmask-msg";
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack1 = helpers.ui) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.ui); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">Loading...</div></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["addtohome"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  
  return " android";
  }
function program3(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
  buffer += "<div class=\"addtohome";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.isAndroid) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.isAndroid); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.isAndroid) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.cssClass) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.cssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "><div class=\"addtohome-popup\"><div class=\"addtohome-icon\"><div class=\"addtohome-icon-image\" style=\"background-image: url(";
  if (stack1 = helpers.image) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.image); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + ");\">&nbsp;</div></div><div class=\"addtohome-content\">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div><div class=\"addtohome-close\"><span class=\"addtohome-closebtn\">&#10060;</span></div></div></div>";
  return buffer;
  });
this["Handlebars"]["templates"]["flash"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data) {
  
  var buffer = "";
  buffer += " flash-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program3(depth0,data) {
  
  
  return " hidden";
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }
  buffer += "<div class=\"flash";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  if (stack1 = helpers.type) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.type); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.type) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  if (stack1 = helpers.hidden) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.hidden); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack1 = helpers.cssClass) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.cssClass); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, options); }
  else { stack1 = (depth0 && depth0.style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, options) : stack1; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["homedetail"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;
function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "<h2>"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</h2><h4>"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.address)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</h4><p>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.nl2br || (depth0 && depth0.nl2br)),stack1 ? stack1.call(depth0, ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.description), options) : helperMissing.call(depth0, "nl2br", ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.description), options)))
    + "</p>";
  return buffer;
  }
  buffer += "<div";
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.data), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["moremenu"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  options = {hash:{
    'compare': ("divider")
  },inverse:self.programWithDepth(16, program16, data, depth1),fn:self.programWithDepth(2, program2, data, depth1),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.type), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.type), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  }
function program2(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<li class=\"ui-li ui-li-divider";
  stack2 = ((stack1 = ((stack1 = (depth2 && depth2.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.programWithDepth(3, program3, data, depth0),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack2 = helpers.bubble) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.bubble); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.bubble) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true)
  },inverse:self.noop,fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.$first), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.$first), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true)
  },inverse:self.noop,fn:self.program(9, program9, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.$last), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.$last), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  if (stack2 = helpers.label) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.label); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.label) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.bubble), {hash:{},inverse:self.noop,fn:self.programWithDepth(13, program13, data, depth2),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</li>";
  return buffer;
  }
function program3(depth0,data,depth1) {
  
  var buffer = "", stack1, options;
  buffer += " ui-bar-";
  options = {hash:{
    'default': (depth0)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.get || (depth0 && depth0.get)),stack1 ? stack1.call(depth0, (depth1 && depth1.ui), options) : helperMissing.call(depth0, "get", (depth1 && depth1.ui), options)));
  return buffer;
  }
function program5(depth0,data) {
  
  
  return " ui-li-has-count";
  }
function program7(depth0,data) {
  
  
  return " ui-first-child";
  }
function program9(depth0,data) {
  
  
  return " ui-last-child";
  }
function program11(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }
function program13(depth0,data,depth3) {
  
  var buffer = "", stack1, stack2;
  buffer += "<span class=\"ui-li-count";
  stack2 = ((stack1 = ((stack1 = (depth3 && depth3.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.programWithDepth(14, program14, data, depth0),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ui-btn-corner-all\">";
  if (stack2 = helpers.bubble) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.bubble); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</span>";
  return buffer;
  }
function program14(depth0,data,depth1) {
  
  var buffer = "", stack1, options;
  buffer += " ui-btn-up-";
  options = {hash:{
    'default': (depth0)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.get || (depth0 && depth0.get)),stack1 ? stack1.call(depth0, (depth1 && depth1.ui), options) : helperMissing.call(depth0, "get", (depth1 && depth1.ui), options)));
  return buffer;
  }
function program16(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<li class=\"list-item ui-btn";
  stack2 = ((stack1 = ((stack1 = (depth2 && depth2.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.programWithDepth(14, program14, data, depth0),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ui-btn-icon-right ui-li-has-icon ui-li-has-arrow ui-li";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  if (stack2 = helpers.bubble) { stack2 = stack2.call(depth0, options); }
  else { stack2 = (depth0 && depth0.bubble); stack2 = typeof stack2 === functionType ? stack2.call(depth0, options) : stack2; }
  if (!helpers.bubble) { stack2 = blockHelperMissing.call(depth0, stack2, options); }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true)
  },inverse:self.noop,fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.$first), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.$first), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{
    'compare': (true)
  },inverse:self.noop,fn:self.program(9, program9, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.$last), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.$last), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"><div class=\"ui-btn-inner ui-li\"><div class=\"ui-btn-text\"><a href=\"";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.route), {hash:{},inverse:self.program(19, program19, data),fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" class=\"ui-link-inherit\" style=\"text-decoration: none;\"><span class=\"icon ui-li-icon ui-corner-none xui-li-thumb more-menu-icon\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.safe || (depth0 && depth0.safe)),stack1 ? stack1.call(depth0, (depth0 && depth0.labelIcon), options) : helperMissing.call(depth0, "safe", (depth0 && depth0.labelIcon), options)))
    + "</span>";
  if (stack2 = helpers.label) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.label); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2);
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.bubble), {hash:{},inverse:self.noop,fn:self.programWithDepth(13, program13, data, depth2),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a></div><span class=\"ui-icon ui-icon-shadow icon news-item-arrow\">&#59234;</div></div></li>";
  return buffer;
  }
function program17(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "#";
  if (stack1 = helpers.route) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.route); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1);
  return buffer;
  }
function program19(depth0,data) {
  
  
  return "javascript:;";
  }
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data};
  stack2 = ((stack1 = helpers.foreach || (depth0 && depth0.foreach)),stack1 ? stack1.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  });
this["Handlebars"]["templates"]["newsdetail"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this, functionType="function";
function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<p class=\"news-detail-title\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>";
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(2, program2, data, depth0),data:data};
  stack2 = ((stack1 = helpers.hasImage || (depth0 && depth0.hasImage)),stack1 ? stack1.call(depth0, ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), options) : helperMissing.call(depth0, "hasImage", ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<p class=\"news-detail-content\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.nl2br || (depth0 && depth0.nl2br)),stack1 ? stack1.call(depth0, ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.data), options) : helperMissing.call(depth0, "nl2br", ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.data), options)))
    + "</p>";
  return buffer;
  }
function program2(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<div class=\"news-detail-medias-grid ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getImageGridType || (depth1 && depth1.getImageGridType)),stack1 ? stack1.call(depth0, ((stack1 = (depth1 && depth1.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), options) : helperMissing.call(depth0, "getImageGridType", ((stack1 = (depth1 && depth1.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), options)))
    + "\">";
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(3, program3, data, depth0),data:data};
  stack2 = ((stack1 = helpers.foreach || (depth0 && depth0.foreach)),stack1 ? stack1.call(depth0, ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), options) : helperMissing.call(depth0, "foreach", ((stack1 = (depth0 && depth0.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div>";
  return buffer;
  }
function program3(depth0,data,depth1) {
  
  var buffer = "", stack1, options;
  buffer += "<div class=\"news-detail-medias zoomable ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getImageRowType || (depth1 && depth1.getImageRowType)),stack1 ? stack1.call(depth0, ((stack1 = (depth1 && depth1.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), (depth0 && depth0.$index), options) : helperMissing.call(depth0, "getImageRowType", ((stack1 = (depth1 && depth1.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), (depth0 && depth0.$index), options)))
    + "\" style=\"background-image:url(";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getImage || (depth1 && depth1.getImage)),stack1 ? stack1.call(depth0, ((stack1 = (depth1 && depth1.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), (depth0 && depth0.$index), options) : helperMissing.call(depth0, "getImage", ((stack1 = (depth1 && depth1.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), (depth0 && depth0.$index), options)))
    + ");\" data-full-image=";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getFullImage || (depth1 && depth1.getFullImage)),stack1 ? stack1.call(depth0, ((stack1 = (depth1 && depth1.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), (depth0 && depth0.$index), options) : helperMissing.call(depth0, "getFullImage", ((stack1 = (depth1 && depth1.data)),stack1 == null || stack1 === false ? stack1 : stack1.content_medias), (depth0 && depth0.$index), options)))
    + "></div>";
  return buffer;
  }
  buffer += "<div id=\"newsdetail-item\" class=\"news-detail\">";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.data), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
this["Handlebars"]["templates"]["newsdetaildate"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;
function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<tr><th class=\"label\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers['t'] || (depth0 && depth0['t'])),stack1 ? stack1.call(depth0, "Start", options) : helperMissing.call(depth0, "t", "Start", options)))
    + "</th><td>";
  if (stack2 = helpers.start_time) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.start_time); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</td></tr>";
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<tr><th class=\"label\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers['t'] || (depth0 && depth0['t'])),stack1 ? stack1.call(depth0, "End", options) : helperMissing.call(depth0, "t", "End", options)))
    + "</th><td>";
  if (stack2 = helpers.end_time) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.end_time); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</td></tr>";
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<tr><th class=\"label\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers['t'] || (depth0 && depth0['t'])),stack1 ? stack1.call(depth0, "Total seats", options) : helperMissing.call(depth0, "t", "Total seats", options)))
    + "</th><td>";
  if (stack2 = helpers.total_seat) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.total_seat); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</td></tr>";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<tr><th class=\"label\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers['t'] || (depth0 && depth0['t'])),stack1 ? stack1.call(depth0, "Available seats", options) : helperMissing.call(depth0, "t", "Available seats", options)))
    + "</th><td class=\"available_seat\">";
  if (stack2 = helpers.available_seat) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.available_seat); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</td></tr>";
  return buffer;
  }
  buffer += "<table class=\"phone-compare ui-shadow table-stroke ui-table ui-table-columntoggle\"><tbody>";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.start_time), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.end_time), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.total_seat), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.available_seat), {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</tbody></table>";
  return buffer;
  });
this["Handlebars"]["templates"]["newsdetailmap"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;
function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "<div class=\"news-detail-map-image\" style=\"background-image:url(";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getUrl || (depth0 && depth0.getUrl)),stack1 ? stack1.call(depth0, (depth0 && depth0.medias), options) : helperMissing.call(depth0, "getUrl", (depth0 && depth0.medias), options)))
    + ");\"></div>";
  return buffer;
  }
  buffer += "<table class=\"phone-compare ui-shadow table-stroke ui-table ui-table-columntoggle\"><tbody><tr><th class=\"label\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers['t'] || (depth0 && depth0['t'])),stack1 ? stack1.call(depth0, "Address", options) : helperMissing.call(depth0, "t", "Address", options)))
    + "</th><td>";
  if (stack2 = helpers.adress) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.adress); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</td></tr></tbody></table>";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.hasMapImage || (depth0 && depth0.hasMapImage)),stack1 ? stack1.call(depth0, (depth0 && depth0.medias), options) : helperMissing.call(depth0, "hasMapImage", (depth0 && depth0.medias), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  });
this["Handlebars"]["templates"]["newssearchcategorylist"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;
function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2;
  buffer += "<li class=\"list-item";
  stack2 = ((stack1 = ((stack1 = (depth1 && depth1.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" data-id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.id); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"><div class=\"icon pastille pastille-";
  if (stack2 = helpers.color) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.color); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">&#128506;</div>";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.name); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</li>";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "";
  buffer += " list-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-item";
  return buffer;
  }
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data};
  stack2 = ((stack1 = helpers.foreach || (depth0 && depth0.foreach)),stack1 ? stack1.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  });
this["Handlebars"]["templates"]["newssearchlist"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<li class=\"list-item ui-btn ui-li";
  stack2 = ((stack1 = ((stack1 = (depth1 && depth1.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ui-btn-icon-right ui-li-has-arrow\" data-id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.id); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"><div class=\"ui-btn-inner ui-li\">";
  stack2 = ((stack1 = ((stack1 = ((stack1 = (depth0 && depth0.category)),stack1 == null || stack1 === false ? stack1 : stack1.color)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<div class=\"ui-btn-text\"><a class=\"ui-link-inherit\">";
  options = {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.hasThumbnail || (depth0 && depth0.hasThumbnail)),stack1 ? stack1.call(depth0, (depth0 && depth0.content_medias), options) : helperMissing.call(depth0, "hasThumbnail", (depth0 && depth0.content_medias), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<h2 class=\"ui-li-heading news-list-title\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.wordwrap || (depth0 && depth0.wordwrap)),stack1 ? stack1.call(depth0, (depth0 && depth0.title), 130, options) : helperMissing.call(depth0, "wordwrap", (depth0 && depth0.title), 130, options)))
    + "</h2><p class=\"ui-li-desc\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.substr || (depth0 && depth0.substr)),stack1 ? stack1.call(depth0, (depth0 && depth0.created_at), 10, options) : helperMissing.call(depth0, "substr", (depth0 && depth0.created_at), 10, options)))
    + "</p></a></div>";
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(8, program8, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.is_favorite), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.is_favorite), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<span class=\"ui-icon ui-icon-shadow icon news-item-arrow\">&#59234;</span></div></li>";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-up-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program4(depth0,data) {
  
  var buffer = "";
  buffer += "<div class=\"news-item-color news-item-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\">&nbsp;</div>";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "<div class=\"news-image-thumb ul-li-thumb\" style=\"background-image: url(";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getThumbnail || (depth0 && depth0.getThumbnail)),stack1 ? stack1.call(depth0, (depth0 && depth0.content_medias), options) : helperMissing.call(depth0, "getThumbnail", (depth0 && depth0.content_medias), options)))
    + ");\"></div>";
  return buffer;
  }
function program8(depth0,data) {
  
  var buffer = "";
  buffer += "<span class=\"icon news-item-favorite\">&#9734;</span>";
  return buffer;
  }
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data};
  stack2 = ((stack1 = helpers.foreach || (depth0 && depth0.foreach)),stack1 ? stack1.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  });
this["Handlebars"]["templates"]["userfavoriteslist"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this, blockHelperMissing=helpers.blockHelperMissing;
function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<li class=\"list-item ui-btn ui-li";
  stack2 = ((stack1 = ((stack1 = (depth1 && depth1.ui)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ui-btn-icon-right ui-li-has-arrow\" data-id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.id); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"><div class=\"ui-btn-inner ui-li\">";
  stack2 = ((stack1 = ((stack1 = ((stack1 = (depth0 && depth0.category)),stack1 == null || stack1 === false ? stack1 : stack1.color)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1)),blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data}));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<div class=\"ui-btn-text\"><a class=\"ui-link-inherit\">";
  options = {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.hasThumbnail || (depth0 && depth0.hasThumbnail)),stack1 ? stack1.call(depth0, (depth0 && depth0.content_medias), options) : helperMissing.call(depth0, "hasThumbnail", (depth0 && depth0.content_medias), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<h2 class=\"ui-li-heading news-list-title\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.wordwrap || (depth0 && depth0.wordwrap)),stack1 ? stack1.call(depth0, (depth0 && depth0.title), 130, options) : helperMissing.call(depth0, "wordwrap", (depth0 && depth0.title), 130, options)))
    + "</h2><p class=\"ui-li-desc\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.substr || (depth0 && depth0.substr)),stack1 ? stack1.call(depth0, (depth0 && depth0.created_at), 10, options) : helperMissing.call(depth0, "substr", (depth0 && depth0.created_at), 10, options)))
    + "</p></a></div>";
  options = {hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(8, program8, data),data:data};
  stack2 = ((stack1 = helpers.if_eq || (depth0 && depth0.if_eq)),stack1 ? stack1.call(depth0, (depth0 && depth0.is_favorite), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.is_favorite), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<span class=\"ui-icon ui-icon-shadow icon news-item-arrow\">&#59234;</div></div></li>";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "";
  buffer += " ui-btn-up-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }
function program4(depth0,data) {
  
  var buffer = "";
  buffer += "<div class=\"news-item-color news-item-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\">&nbsp;</div>";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "<div class=\"news-image-thumb ul-li-thumb\" style=\"background-image: url(";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getThumbnail || (depth0 && depth0.getThumbnail)),stack1 ? stack1.call(depth0, (depth0 && depth0.content_medias), options) : helperMissing.call(depth0, "getThumbnail", (depth0 && depth0.content_medias), options)))
    + ");\"></div>";
  return buffer;
  }
function program8(depth0,data) {
  
  var buffer = "";
  buffer += "<span class=\"icon news-item-favorite\">&#9734;</span>";
  return buffer;
  }
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data};
  stack2 = ((stack1 = helpers.foreach || (depth0 && depth0.foreach)),stack1 ? stack1.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  });
this["Handlebars"]["templates"]["userprofile"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  
  return "<form>\n  <h2 class=\"form-title-row\">Text fields</h2>\n  <fieldset>\n    <label class=\"form-row form-text-row\">\n      <!-- Note: there must be no whitespace between the <span> and the <input>. -->\n      <span class=\"form-text-label\">Subject:</span><input type=\"text\" class=\"form-text-input\" value=\"Hi\">\n      <!-- If you want the label to be black, use: <span class=\"form-text-label form-text-label-black\"> -->\n      <!-- If you want the label to be fixed-width, use: <span class=\"form-text-label width-20 (or 30, 40, 50)\"> -->\n    </label>\n    <label class=\"form-row form-text-row\">\n      <textarea class=\"form-text-textarea\" placeholder=\"Message…\"></textarea>\n    </label>\n  </fieldset>\n\n  <h2 class=\"form-title-row\">Validation</h2>\n  <fieldset>\n    <label class=\"form-row form-text-row valid\">\n      <span class=\"form-text-label width-20\">Email:</span><input type=\"email\" class=\"form-text-input\" value=\"valid@email\">\n      <span class=\"form-hint\">Good job!</span>\n    </label>\n    <label class=\"form-row form-text-row invalid\">\n      <span class=\"form-text-label width-20\">URL:</span><input type=\"url\" class=\"form-text-input\" value=\"invalid url\">\n      <span class=\"form-hint\">Doesn't look like a URL.</span>\n    </label>\n  </fieldset>\n\n  <div class=\"form-submit\">\n    <button type=\"submit\" class=\"button button-green icon-check\">Send</button>\n    <button type=\"reset\" class=\"button button-red icon-delete\">Cancel</button>\n  </div>\n</form>";
  });
(function() {
    if (!Function.prototype.bind) {
        Function.prototype.bind = function(oThis) {
            if (typeof this !== 'function') {
                
                throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
            }
            var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                EmptyFn = function() {},
                fBound = function() {
                    return fToBind.apply(this instanceof EmptyFn ? this : oThis || window,
                        aArgs.concat(Array.prototype.slice.call(arguments)));
                };
            EmptyFn.prototype = this.prototype;
            fBound.prototype = new EmptyFn();
            return fBound;
        };
    }
}());
(function() {
    
    var slice = Array.prototype.slice,
        _xtypes = {};
    var _xtype = function(xtype, obj) {
        if (obj) {
            _xtypes[xtype] = obj;
        } else {
            return (_xtypes[xtype] ||
                console.warn('[core] xtype "' + xtype +
                    '" does not exists.'));
        }
    };
    var _createSubclass = function(props) {
        props = props || {};
        var key,
            realConstructor,
            superclass = props.superclass.prototype;
        if (props.hasOwnProperty('constructor')) {
            realConstructor = props.constructor;
        } else if (typeof superclass.constructor === 'function') {
            realConstructor = superclass.constructor;
        } else {
            realConstructor = function() {};
        }
        function constructor() {
            var self = this;
            if (!(self instanceof constructor)) {
                throw new Error('[core] Please use "new" when initializing Fs classes');
            }
            realConstructor.apply(self, arguments);
        }
        constructor.prototype = Object.create(superclass);
        constructor.prototype.constructor = constructor;
        _extend(constructor, {
            parent: superclass,
            subclass: function(obj) {
                var sclass;
                obj = obj || {};
                obj.superclass = this;
                sclass = _createSubclass(obj);
                if (obj.xtype) {
                    _xtype(obj.xtype, sclass);
                }
                return sclass;
            }
        });
        for (key in props) {
            if (key !== 'constructor' &&
                key !== 'superclass') {
                constructor.prototype[key] = props[key];
            }
        }
        return constructor;
    };
    var _extend = function() {
        var i = -1,
            args = slice.call(arguments),
            l = args.length,
            object = args.shift();
        while (++i < l) {
            var key,
                props = args[i];
            for (key in props) {
                object[key] = props[key];
            }
        }
        return object;
    };
    var Fs = {
        views: {},
        events: {},
        data: {},
        engines: {},
        config: {},
        helpers: {},
        xtype: _xtype,
        
        version: '{{version}}',
        
        subclass: function(obj) {
            var sclass;
            obj = obj || {};
            obj.superclass = function() {};
            sclass = _createSubclass(obj);
            if (obj.xtype) {
                _xtype(obj.xtype, sclass);
            }
            return sclass;
        }
    };
    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' &&
            module.exports) {
            exports = module.exports = Fs;
        }
        exports.Fs = Fs;
    } else if (typeof define === 'function' &&
        define.amd) {
        define(function() {
            return Fs;
        });
    } else {
        window.Fs = Fs;
    }
})();
Fs.debug = new (function() {
    var _profiles = {},
        _debug = false,
        _whitelist = [];
    var isWhitelist = function(prefix) {
        if (_whitelist.length &&
            _whitelist.indexOf(prefix) === -1) {
            return false;
        }
        return true;
    };
    return Fs.subclass({
        
        log: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.log.apply(console, args);
            return this;
        },
        error: function(prefix) {
            if (!_debug) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.error.apply(console, args);
            return this;
        },
        warn: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.warn.apply(console, args);
            return this;
        },
        
        set: function(debug) {
            _debug = (debug === true);
        }
    });
}())();
Fs.utils = (function() {
    'use strict';
    var _table = '00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D';
    return {
        
        applyIf: function(obj, base, keys) {
            var i = keys.length, key;
            while (i--) {
                key = keys[i];
                if (typeof obj[key] === 'undefined' &&
                    typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },
        
        applyIfAuto: function(obj, base) {
            var key;
            for (key in base) {
                if (typeof obj[key] === 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },
        
        apply: function(obj, base, keys) {
            var i = keys.length, key;
            while (i--) {
                key = keys[i];
                if (typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },
        
        applyAuto: function(obj1, obj2) {
            var attrname, obj3 = {};
            for (attrname in obj1) {
                obj3[attrname] = obj1[attrname];
            }
            for (attrname in obj2) {
                obj3[attrname] = obj2[attrname];
            }
            return obj3;
        },
        
        arraySlice: function(array, from, to) {
            var result = [],
                i = (from ? (from - 1) : 0),
                length = (to ? to : array.length);
            while (++i < length) {
                result.push(array[i]);
            }
            return result;
        },
        
        capitalize: function(str) {
            return (str.charAt(0).toUpperCase() + str.slice(1));
        },
        
        crc32: function(str, crc) {
            if (crc == window.undefined) {
                crc = 0;
            }
            var i = 0,
                iTop = str.length,
                n = 0, 
                x = 0; 
            crc = crc ^ (-1);
            for (; i < iTop; i++ ) {
                n = ( crc ^ str.charCodeAt(i)) & 0xFF;
                x = '0x' + _table.substr(n * 9, 8);
                crc = (crc >>> 8) ^ x;
            }
            return crc ^ (-1);
        }
    };
}());
Fs.Storage = new (function() {
    'use strict';
    var _store = localStorage,
        _prefix = '';
    
    function _setPrefix(prefix) {
        if (typeof prefix === 'string') {
            _prefix = prefix;
            return true;
        }
        return false;
    }
    
    function _getPrefix(prefix) {
        if (typeof prefix === 'string') {
            return prefix;
        }
        return _prefix;
    }
    
    function _setItem(name, value, prefix) {
        return (_store.setItem(_getPrefix(prefix) + name,
            JSON.stringify(value)));
    }
    
    function _getItem(name, prefix) {
        var value = _store.getItem(_getPrefix(prefix) + name);
        
        if (typeof value === 'string') {
            return JSON.parse(value);
        }
        return false;
    }
    
    function _removeItem(name, prefix) {
        return _store.removeItem(_getPrefix(prefix) + name);
    }
    
    function _empty(prefix) {
        prefix = _getPrefix(prefix);
        for (var name in _store) {
            if (!prefix || !name.indexOf(prefix)) {
                _removeItem(name, '');
            }
        }
    }
    return Fs.subclass({
        setPrefix: _setPrefix,
        getPrefix: _getPrefix,
        setItem: _setItem,
        getItem: _getItem,
        removeItem: _removeItem,
        empty: _empty
    });
}())();
Fs.Selector = new (function() {
    
    
    var _scope,
        _elUid = 0,
        _win = window;
    
    return Fs.subclass({
        
        constructor: function(gscope) {
            _scope = gscope;
        },
        
        generateId: function(prefix) {
            ++_elUid;
            prefix = prefix || 'fs';
            return (prefix + _elUid);
        },
        
        get: function(selector, root) {
            root = root || _scope;
            return root.querySelector(selector);
        },
        
        gets: function(selector, root) {
            root = root || _scope;
            return root.querySelectorAll(selector);
        },
        
        hasClass: function(el, cls) {
            
            if (!el || typeof el !== 'object') {
                console.warn('EL is not good', el);
                return false;
            }
            
            return ((' ' + el.className + ' ').indexOf(' ' + cls + ' ') !== -1);
        },
        
        addClass: function(el, cls) {
            if (!this.hasClass(el, cls)) {
                
                el.className += ' ' + cls;
                el.className = el.className.replace(/^\s+|\s+$/g, '');
                return true;
            }
            return false;
        },
        
        addClasses: function(el, cls) {
            var c = cls.split(' '),
                len = c.length;
            while (len--) {
                this.addClass(el, c[len]);
            }
            return true;
        },
        
        removeClass: function(el, cls) {
            if (this.hasClass(el, cls)) {
                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                el.className = el.className.replace(reg, ' ').replace(/^\s+|\s+$/g, '');
                return true;
            }
            return false;
        },
        
        removeClasses: function(el, cls) {
            var c = cls.split(' '),
                len = c.length;
            while (len--) {
                this.removeClass(el, c[len]);
            }
            return true;
        },
        
        redraw: function(el) {
            el.style.display = 'none';
            var h = el.offsetHeight;
            el.style.display = 'block';
        },
        
        removeEl: function(el) {
            if (typeof el === 'string') {
                el = this.get(el);
            }
            el.parentNode.removeChild(el);
        },
        
        removeHtml: function(el) {
            
            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }
        },
        
        findParentByTag: function(el, tagName) {
            while (el) {
                if (el.tagName === tagName) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },
        
        findParentByClass: function(el, className) {
            while (el) {
                if (this.hasClass(el, className)) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },
        
        addHtml: function(el, html) {
            el.insertAdjacentHTML('beforeEnd', html);
        },
        
        updateHtml: function(el, html) {
            this.removeHtml(el);
            this.addHtml(el, html);
        },
        
        scrollToEl: function(el, offsetX, offsetY) {
            offsetX = offsetX || 0;
            offsetY = offsetY || 0;
            _win.scrollTo(el.offsetLeft + offsetX,
                el.offsetTop + offsetY);
        }
    });
}())(document);
Fs.Request = new (function() {
    'use strict';
    
    var jsonp_id = 0,
        _debug = Fs.debug;
    var _jsonToString = function(json, prefix) {
        var key,
            val,
            result = '',
            first = true;
        for (key in json) {
            val = json[key];
            if (first === false) {
                result += '&';
            } else {
                first = false;
            }
            result += key + '=' + encodeURIComponent(val);
        }
        _debug.log('request', 'jsonToString result', result);
        return (prefix ? prefix : '') + result;
    };
    
    return Fs.subclass({
        
        ajax: function(url, options) {
            var request,
                callback = (typeof options.callback === 'function' ?
                    options.callback.bind(options.scope || window) : null);
                options.method = options.method || 'GET';
                options.data = options.data || '';
            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }
            function stateChange() {
                if (request.readyState === 4 &&
                    callback) {
                    callback((request.status === 200 ||
                        request.status === 304) ? true : false,
                        request.responseText);
                }
            }
            request = new XMLHttpRequest();
            request.onreadystatechange = stateChange;
            request.open(options.method, url, true);
            if (options.method !== 'GET' && options.data) {
                request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                request.setRequestHeader('Content-type',
                    'application/x-www-form-urlencoded');
                request.setRequestHeader('Connection', 'close');
            }
            request.send(options.data);
        },
        
        jsonp: function(url, options) {
            var callback_name = ['jsonp', ++jsonp_id].join(''),
                callback = options.callback.bind(options.scope || window),
                script = document.createElement('script'),
                clean = function() {
                    document.body.removeChild(script);
                    delete window[callback_name];
                },
                error = function() {
                    _debug.warn('request', 'JSONP request error', url, options, arguments);
                    clean();
                    if (callback) {
                        callback(false);
                    }
                },
                success = function(data) {
                    clean();
                    if (callback) {
                        callback(true, data);
                    }
                };
            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }
            window[callback_name] = success;
            script.onerror = error;
            script.src = url.replace('{callback}', callback_name);
            document.body.appendChild(script);
        },
        
        memory: function(data, options, store) {
            var i, length, callback,
                result = {},
                idProp = store.getReaderId(),
                totalProp = store.getReaderTotal(),
                successProp = store.getReaderSuccess(),
                rootProp = store.getReaderRoot();
            callback = options.callback.bind(options.scope || window);
            if (!data) {
                data = store.config.data || [];
            }
            length = data.length;
            if (data[0] && typeof data[0][idProp] === 'undefined') {
                for (i = 0; i < length; ) {
                    data[i][idProp] = ++i;
                }
            }
            result[totalProp] = length;
            result[successProp] = true;
            result[rootProp] = data;
            if (callback) {
                callback(true, result);
            }
        }
    });
}())();
Fs.History = new (function() {
    'use strict';
    
    var _fs = Fs,
        _win = window,
        scope = _win,
        _storage = _fs.Storage,
        _isStandalone = (_win.navigator && _win.navigator.standalone === true),
        curhash = _win.location.hash.substr(1),
        routes = [],
        _history = [],
        _maxLength = 20,
        _curLength = 0;
    function runCallbacks(self, location) {
        
        if (_isStandalone) {
            _storage.setItem('location', location, _win.location.host);
        }
        var regexp,
            route,
            i = -1,
            length = routes.length;
        self.curhash = location;
        while (++i < length) {
            route = routes[i];
            if (route.regexp.test(location)) {
                route.callback(location);
            }
        }
        return true;
    }
    var onHashChange = function(event) {
        var here = _win.location.hash.substr(1);
        _curLength = _history.push(here);
        if (_curLength > _maxLength) {
            _history.shift();
        }
        runCallbacks(this, here);
    };
    
    return _fs.subclass({
        
        constructor: function(gscope) {
            
            this.defaultRoute = '';
            scope = gscope || _win;
            onHashChange = onHashChange.bind(this);
            scope.addEventListener('hashchange', onHashChange);
        },
        
        setDefaultRoute: function(path) {
            this.defaultRoute = path;
        },
        
        start: function() {
            var hash = curhash;
            curhash = '';
            
            if (_isStandalone) {
                hash = _storage.getItem('location', _win.location.host) || this.defaultRoute;
            }
            runCallbacks(this, hash);
        },
        
        stop: function() {
            scope.removeEventListener('hashchange', onHashChange);
        },
        
        here: function(encoded) {
            var hash = _win.location.hash.substr(1);
            return (encoded ? encodeURIComponent(hash) : hash);
        },
        
        get: function(index, encoded) {
            var pos = _curLength - 1 + index;
            if (pos >= _curLength) {
                return this.here(encoded);
            } else if (pos < 0) {
                pos = 0;
            }
            return (encoded ? encodeURIComponent(_history[pos]) : _history[pos]);
        },
        
        back: function(index) {
            index = (typeof index !== 'number' ? -1 : index);
            _win.history.go(index);
        },
        
        navigate: function(location) {
            _win.location.hash = '#' + location;
        },
        
        route: function(route, callback) {
            routes.push({
                regexp: route,
                callback: callback
            });
        }
    });
}())(window);
Fs.Router = (function() {
    'use strict';
    
    var _fs = Fs,
        _history = _fs.History,
        optionalParam = /\((.*?)\)/g,
        namedParam = /(\(\?)?:\w+/g,
        splatParam = /\*\w+/g,
        escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g;
    function _extractParameters(route, fragment) {
        return route.exec(fragment).slice(1);
    }
    function _routeToRegExp(route) {
        route = route.replace(escapeRegExp, '\\$&')
            .replace(optionalParam, '(?:$1)?')
            .replace(namedParam, function(match, optional) {
                return optional ? match : '([^\/]+)';
            })
            .replace(splatParam, '(.*?)');
        return new RegExp('^' + route + '$');
    }
    function _prepareBefore(self) {
        if (self.before) {
            var pattern;
            self.beforeRegexp = {};
            for (pattern in self.before) {
                
                self.beforeRegexp[pattern] = new RegExp(pattern);
            }
        }
    }
    function _getBefore(self, funcname) {
        if (self.before) {
            var pattern;
            for (pattern in self.before) {
                if (self.beforeRegexp[pattern].test(funcname) === true) {
                
                    var func = self[self.before[pattern]];
                    return (func ? func : false);
                }
            }
        }
        return false;
    }
    
    return _fs.subclass({
        
        
        
        constructor: function() {
            var self = this;
            _prepareBefore(self);
            if (self.routes) {
                var pattern;
                for (pattern in self.routes) {
                    self.route(pattern, self.routes[pattern]);
                }
            }
        },
        
        route: function(path, foo) {
            var before,
                self = this,
                callback = self[foo],
                history = _history;
            if (callback) {
                path = _routeToRegExp(path);
                before = _getBefore(self, foo);
                var cb = function(fragment) {
                    var args = _extractParameters(path, fragment);
                    callback.apply(self, args);
                },
                beforeCb = function(fragment) {
                    before.call(self, function() {
                        return cb(fragment);
                    });
                };
                if (before) {
                    history.route(path, beforeCb);
                } else {
                    history.route(path, cb);
                }
            }
            return self;
        }
    });
}());
Fs.Event = (function() {
    'use strict';
    var _fs = Fs,
        _utils = _fs.utils,
        _parent = _fs;
    function _setupEvents(self) {
        if (typeof self.events !== 'object') {
            
            
            self.events = {};
            self.uids = 0;
            
            
            self.pqueue = {};
            
            
            self.equeue = {};
        }
    }
    function _numSort(a, b) {
        return (b - a);
    }
    function _count_listeners(self, name) {
        var e = self.events[name];
        if (e) {
            var total = 0,
                keys = Object.keys(self.events[name]),
                i = keys.length;
            while (i--) {
                total += e[keys[i]].length;
            }
            return total;
        }
        return 0;
    }
    
    return _parent.subclass({
        
        priority: {
            CORE: 1000,
            VIEWS: 900,
            DEFAULT: 800
        },
        
        defaultPriority: 800,
        
        constructor: function(opts) {
            
            var self = this;
            if (opts && opts.listeners) {
                self.listeners = opts.listeners;
                self.wasListened = {};
            }
            _setupEvents(self);
            
        },
        
        setupListeners: function(eventNames, renderer) {
            var self = this;
            if (self.listeners) {
                var name,
                    scope = self.listeners.scope || self,
                    i = eventNames.length;
                renderer = renderer || self;
                while (i--) {
                    name = eventNames[i];
                    if (self.listeners[name] &&
                        !self.wasListened[name]) {
                        self.wasListened[name] = true;
                        renderer.on(name, self.listeners[name], scope);
                    }
                }
            }
        },
        
        hasListener: function(name) {
            if (typeof this.listeners === 'object' &&
                typeof this.listeners[name] === 'function') {
                return true;
            }
            return false;
        },
        
        getListener: function(name) {
            return this.listeners[name];
        },
        
        fire: function(name) {
            var self = this,
                p = self.pqueue[name];
            if (p) {
                p = self.pqueue[name].slice(0);
                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1);
                while (++i < plen) {
                    events = self.events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;
                    while (++j < elen) {
                        e = self.equeue['u' + events[j]];
                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function');
                        } else if (e[2].apply(e[3], args) === false) {
                            return false;
                        }
                    }
                }
                return true;
            }
        },
        fireScope: function(name) {
            var self = this,
                p = self.pqueue[name];
            if (p) {
                p = self.pqueue[name].slice(0);
                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1);
                while (++i < plen) {
                    events = self.events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;
                    while (++j < elen) {
                        e = self.equeue['u' + events[j]];
                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function');
                        } else if (e[2].apply(e[3], [e[3]].concat(args)) === false) {
                            return false;
                        }
                    }
                }
                return true;
            }
        },
        
        on: function(name, cb, scope, priority) {
            var self = this;
            priority = priority || self.defaultPriority;
            if (typeof self.events[name] === 'undefined') {
                self.events[name] = {};
            }
            if (typeof self.events[name]['p' + priority] === 'undefined') {
                self.events[name]['p' + priority] = [];
            }
            if (typeof self.pqueue[name] === 'undefined') {
                self.pqueue[name] = [];
            }
            if (self.pqueue[name].indexOf(priority) === -1) {
                self.pqueue[name].push(priority);
                self.pqueue[name].sort(_numSort);
            }
            scope = scope || window;
            self.equeue['u' + (++self.uids)] = [name, priority, cb, scope];
            self.events[name]['p' + priority].push(self.uids);
            return self.uids;
        },
        
        off: function(uid) {
            var self = this,
                e = self.equeue['u' + uid];
            if (e) {
                var cb,
                    length,
                    name = e[0],
                    priority = e[1],
                    listeners = self.events[name]['p' + priority];
                listeners.splice(listeners.indexOf(uid), 1);
                length = listeners.length;
                delete self.equeue['u' + uid];
                if (!length) {
                    
                    delete self.events[name]['p' + priority];
                    self.pqueue[name].splice(self.pqueue[name].indexOf(priority), 1);
                    if (!self.pqueue[name].length) {
                        delete self.pqueue[name];
                        delete self.events[name];
                        return 0;
                    }
                }
                return _count_listeners(self, name);
            }
            return false;
        }
    });
}());
Fs.Templates = new (function() {
    'use strict';
    
    var _fs = Fs,
        _handlebars = Handlebars,
        _request = _fs.Request;
    _handlebars.currentParent = [];
    
    
    _handlebars.registerHelper('render', function(config) {
        var html,
            parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = new (_fs.xtype(config.xtype))(config),
            renderer = ((parent.renderer && parent.renderer.el) ? parent.abstractRenderer : parent.renderer);
        
        if (!renderer) {
            renderer = parent.abstractRenderer = new _fs.Event();
        }
        html = item.compile(renderer.config, renderer);
        return new _handlebars.SafeString(html);
    });
    
    
    _handlebars.registerHelper('config', function(context, options) {
        var parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = (parent.items ? parent.items[context] : undefined);
        if (typeof item === 'object' && item.config) {
            return options.fn(item.config);
        }
        return options.fn(this);
    });
    
    
    _handlebars.registerHelper('safe', function(str) {
        return new _handlebars.SafeString(str);
    });
    _handlebars.registerHelper('length', function(array) {
        return array.length;
    });
    _handlebars.registerHelper('geticon', function(icon) {
        return icon;
    });
    _handlebars.registerHelper('wordwrap', function(str, len) {
        if (str.length > len) {
            return str.substring(0, len) + '...';
        }
        return str;
    });
    _handlebars.registerHelper('substr', function(str, len) {
        if (str.length <= len) {
            return str;
        }
        return str.substring(0, len);
    });
    _handlebars.registerHelper('foreach', function(arr, options) {
        var length = arr.length;
        if (options.inverse && !length) {
            return options.inverse(this);
        }
        return arr.map(function(item, index) {
            if (typeof item !== 'object') {
                item = {
                    original: item
                };
            }
            item.$index = index;
            item.$first = index === 0;
            item.$last  = index === (length - 1);
            return options.fn(item);
        }).join('');
    });
    
    _handlebars.registerHelper('if_eq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context == options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    
    _handlebars.registerHelper('if_neq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context != options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    
    _handlebars.registerHelper('if_gteq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context >= options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    
    _handlebars.registerHelper('get', function(context, options) {
        var hash = options.hash;
        if (typeof context === 'undefined' ||
            (hash.choices &&
            hash.choices.indexOf(context) === -1)) {
            return hash['default'];
        }
        return context;
    });
    function loadtpl(self, name, cb) {
        if (_handlebars.templates[name]) {
            cb(_handlebars.templates[name]);
        } else {
            var url = self.path + name + '.handlebars';
            _request.ajax(url, {
                scope: self,
                callback: function(success, data) {
                    if (success) {
                        _handlebars.templates[name] = self.compile(data);
                        return cb(_handlebars.templates[name]);
                    }
                    console.warn('Error while loading template "' + url + '".');
                    return cb(false);
                }
            });
        }
    }
    function loadtpls(self, names, cb) {
        var results = {},
            i = names.length,
            remaining = names.length;
        function loaded(tplname) {
            return function(tpl) {
                results[tplname] = tpl;
                if (!(--remaining)) {
                    cb(results);
                }
            };
        }
        while (i--) {
            loadtpl(self, names[i], loaded(names[i]));
        }
    }
    return _fs.subclass({
        
        constructor: function() {
            this.path = '';
        },
        
        setPath: function(path) {
            this.path = path;
        },
        
        get: function(name) {
            return _handlebars.templates[name];
        },
        
        load: function(names, cb) {
            if (typeof names === 'object') {
                return loadtpls(this, names, cb);
            }
            return loadtpl(this, names, cb);
        },
        
        compile: function(source) {
            if (typeof source === 'function') {
                return source;
            } else if (typeof _handlebars.templates[source] === 'function') {
                return _handlebars.templates[source];
            } else if (_handlebars.compile) {
                return _handlebars.compile(source);
            }
            return function() {
                return source;
            };
        }
    });
}())();
Fs.Settings = (function() {
    'use strict';
    var _fs = Fs,
        _parent = _fs.Event;
    return _parent.subclass({
        
        constructor: function(config) {
            this.vars = config || {};
            _parent.prototype.constructor.apply(this, arguments);
        },
        
        set: function(name, value) {
            var oldValue = this.get(name);
            this.vars[name] = value;
            this.fire('settingchanged', this, name, value, oldValue);
            this.fire(name + 'changed', this, name, value, oldValue);
        },
        
        get: function(name, defaultValue) {
            if (this.vars[name]) {
                return this.vars[name];
            }
            return defaultValue;
        },
        
        gets: function() {
            var result = [],
                i = -1,
                length = arguments.length;
            while (++i < length) {
                result.push(this.get(arguments[i]));
            }
            return result;
        },
        
        empty: function(name) {
            if (typeof this.vars[name] !== 'undefined') {
                delete this.vars[name];
                this.fire(name + 'empty', this, name);
            }
        }
    });
}());
Fs.App = (function() {
    'use strict';
    var _fs = Fs,
        _debug = _fs.debug,
        _storage = _fs.Storage,
        _engines = _fs.engines;
    function _get_engine(name, defaultEngine) {
        var engine = _engines[name];
        if (engine) {
            return engine;
        }
        if (name) {
            _debug.error('app', 'Given engine "' + name +
                '" does not exists. Using "' + defaultEngine + '" instead.');
        }
        return _engines[defaultEngine];
    }
    function _register_listener(self) {
        function ready() {
            if (!self.domReady) {
                document.removeEventListener('DOMContentLoaded', ready);
            }
            self.domReady = true;
            if (self.templateLoaded === true) {
                self.config.onready();
            }
        }
        document.addEventListener('DOMContentLoaded', ready);
    }
    
    
    
    
    
    
    return _fs.subclass({
        defaultEngine: 'Homemade',
        templateLoaded: true,
        domReady: false,
        
        constructor: function(opts) {
            var self = this;
            self.config = opts || {};
            if (typeof self.config.debug !== 'undefined') {
                _debug.set(self.config.debug);
            }
            if (typeof self.config.defaultRoute === 'string') {
                _fs.History.setDefaultRoute(self.config.defaultRoute);
            }
            if (self.config.require && self.config.require.templates) {
                self.templateLoaded = false;
                _fs.Templates.load(self.config.require.templates, function() {
                    self.templateLoaded = true;
                    if (self.domReady === true &&
                        self.config.onready) {
                        self.config.onready();
                    }
                });
            }
            if (self.config.onready) {
                _register_listener(self);
            }
            
            _fs.config.engine = _get_engine(self.config.engine, self.defaultEngine);
            if (self.config.ui) {
                _fs.config.ui = self.config.ui;
            }
        }
    });
})();
Fs.events.Abstract = (function() {
    'use strict';
    var _fs = Fs,
        _debug = _fs.debug,
        _eventsAttached = {},
        _events = {},
        _noHandler = function() {
            _debug.warn(this.xtype, 'No handler is define !');
        };
    function _global_handler(name) {
        return function(e) {
            var handler = _events[name][e.target.id];
            if (handler) {
                handler(e);
            }
        };
    }
    function _global_attach(self) {
        if (!_eventsAttached[self.eventName]) {
            _eventsAttached[self.eventName] = true;
            self.globalFwd.addEventListener(self.eventName,
                _global_handler(self.eventName), self.useCapture);
        }
        if (!self.el.id || !self.el.id.length) {
            self.el.id = _fs.Selector.generateId();
        }
        if (!_events[self.eventName]) {
            _events[self.eventName] = {};
        }
        _events[self.eventName][self.el.id] = self.handler;
    }
    function _global_detach(self) {
        delete _events[self.eventName][self.el.id];
        if (_eventsAttached[self.eventName] &&
            !_events[self.eventName].length) {
            _eventsAttached[self.eventName] = false;
            self.globalFwd.removeEventListener(self.eventName,
                _global_handler, self.useCapture);
        }
    }
    return _fs.subclass({
        xtype: 'events.abstract',
        
        useCapture: false,
        
        autoAttach: false,
        
        eventName: false,
        
        defaultScope: false,
        
        defaultHandler: false,
        
        defaultEl: false,
        
        globalFwd: false,
        
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            _fs.utils.apply(self, opts, [
                'useCapture',
                'autoAttach',
                'globalFwd'
            ]);
            if (!self.eventName && opts.eventName) {
                self.eventName = opts.eventName;
            }
            self.defaultEl = opts.el || window;
            self.defaultScope = (opts.scope ? opts.scope : self);
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },
        
        attach: function(cmp, handler, scope) {
            var self = this;
            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            if (self.eventName) {
                if (self.globalFwd) {
                    _global_attach(self);
                } else {
                    self.el.addEventListener(self.eventName,
                        self.handler, self.useCapture);
                }
            }
        },
        
        detach: function(cmp) {
            var self = this;
            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (self.eventName) {
                if (self.globalFwd) {
                    _global_detach(self);
                } else {
                    self.el.removeEventListener(self.eventName,
                        self.handler, self.useCapture);
                }
            }
        }
    });
})();
Fs.events.Click = (function() {
    'use strict';
    
    var _fs = Fs,
        _win = window,
        _fastclick = _win.FastClick;
        
    function _no_handler() {
        console.warn('click', 'No handler is define !');
    }
    _win.addEventListener('load', function() {
        _fastclick.attach(document.body);
    }, false);
    function _stop_scroll(e) {
        e.preventDefault();
    }
    
    return _fs.subclass({
        xtype: 'events.click',
        
        defaultDisableScrolling: false,
        
        defaultUseCapture: false,
        
        autoAttach: false,
        
        defaultScope: false,
        
        defaultHandler: false,
        
        defaultEl: false,
        
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            self.disableScrolling = opts.disableScrolling || self.defaultDisableScrolling;
            self.useCapture = opts.useCapture || self.defaultUseCapture;
            _fs.utils.apply(self, opts, [
                'autoAttach'
            ]);
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);
            
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },
        
        attach: function(cmp, handler, scope) {
            var self = this;
            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            self.el.addEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.addEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        },
        
        detach: function(cmp) {
            var self = this;
            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            self.el.removeEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.removeEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        }
    });
}());
Fs.events.Scroll = (function() {
    'use strict';
    
    var _lastEvent,
        _intervalId,
        _fs = Fs,
        _debug = _fs.debug,
        _window = window,
        _doc = document,
        _screenHeight = _doc.height,
        _eventsAttached = false,
        _scrolled = false,
        _events = new _fs.Event();
    function _noHandler() {
        _debug.error('scroll', '[Fs.events.Scroll] No handler is define !');
    }
    function _poolHandler() {
        if (_scrolled === true) {
            _scrolled = false;
            _events.fire('scroll', _lastEvent);
        }
    }
    function _windowScrollListener() {
        _scrolled = true;
        _lastEvent = event;
    }
    function _windowResizeListener() {
        _screenHeight = _doc.height;
    }
    
    return _fs.subclass({
        xtype: 'events.scroll',
        defaultInterval: 167, 
        useCapture: false,
        autoAttach: false,
        
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            _fs.utils.apply(self, opts, [
                'useCapture',
                'autoAttach'
            ]);
            self.interval = opts.interval || self.defaultInterval;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },
        attach: function(cmp, handler, scope) {
            var self = this;
            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('scroll',
                    _windowScrollListener, false);
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _intervalId = _window.setInterval(_poolHandler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            self.euid = _events.on('scroll', self.handler, scope);
        },
        detach: function(cmp) {
            var self = this;
            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            var length = _events.off(self.euid);
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (length === 0) {
                self.reset();
            }
        },
        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('scroll',
                _windowScrollListener, false);
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_intervalId);
        }
    });
})();
Fs.events.Resize = (function() {
    'use strict';
    
    var _lastEvent,
        _intervalId,
        _window = window,
        _eventsAttached = false,
        _resized = false,
        _fs = Fs,
        _debug = _fs.debug,
        _events = new _fs.Event();
    function _no_handler() {
        _debug.warn('events.resize', 'No handler is define !');
    }
    function _pool_handler() {
        if (_resized === true) {
            _resized = false;
            _events.fire('resize', _lastEvent);
        }
    }
    function _window_resize_listener(event) {
        _resized = true;
        _lastEvent = event;
    }
    
    return _fs.subclass({
        xtype: 'events.resize',
        defaultInterval: 500,
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            self.interval = opts.interval || self.defaultInterval;
            self.attached = false;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);
        },
        attach: function(cmp, handler, scope) {
            var self = this;
            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('resize',
                    _window_resize_listener, false);
                _intervalId = _window.setInterval(_pool_handler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            self.euid = _events.on('resize', self.handler, scope);
        },
        detach: function(cmp) {
            var self = this;
            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el;
            var length = _events.off(self.euid);
            delete self.handler;
            if (length === 0) {
                self.reset();
            }
        },
        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('resize',
                _window_resize_listener, false);
            _window.clearInterval(_intervalId);
        }
    });
})();
Fs.events.Nobounce = (function() {
    
    var _lastEvent,
        _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _doc = document,
        _parent = _fs;
    var _isParentNoBounce = function(el, className) {
        while (el) {
            if (_selector.hasClass(el, className)) {
                return true;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };
    var _disableBounce = function(e) {
        _debug.log('nobounce', 'disable bounce ?');
        if (_isParentNoBounce(e.target, this.className)) {
            _debug.log('nobounce', 'disabling bounce !');
            e.preventDefault();
        }
    };
    
    return _parent.subclass({
        defaultClassName: 'nobounce',
        defaultAutoAttach: true,
        defaultEl: _doc,
        constructor: function(opts) {
            opts = opts || {};
            this.autoAttach = opts.autoAttach || this.defaultAutoAttach;
            this.className = opts.className || this.defaultClassName;
            this.el = opts.el || this.defaultEl;
            this.attached = false;
            this.handler = _disableBounce.bind(this);
            if (this.autoAttach === true) {
                this.attach(this.el);
            }
        },
        attach: function(cmp) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            this.el = cmp || this.el;
            this.el.addEventListener('touchmove', this.handler, false);
        },
        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            this.el = cmp || this.el;
            this.el.removeEventListener('touchmove', this.handler, false);
        }
    });
})();
Fs.events.Focus = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.events.Abstract;
    
    return _parent.subclass({
        xtype: 'events.focus',
        
        eventName: 'focusin',
        
        globalFwd: window,
        
        constructor: _parent.prototype.constructor
    });
}());
Fs.events.Blur = (function() {
    
    var _fs = Fs,
        _parent = _fs.events.Abstract;
    
    return _parent.subclass({
        xtype: 'events.blur',
        
        eventName: 'focusout',
        
        globalFwd: window,
        
        constructor: _parent.prototype.constructor
    });
}());
Fs.events.Keypress = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.events.Abstract;
    
    return _parent.subclass({
        xtype: 'events.keypress',
        
        eventName: 'keypress',
        
        globalFwd: window,
        
        constructor: _parent.prototype.constructor
    });
}());
Fs.engines.JqueryMobile = (function() {
    'use strict';
    
    var _handlebars = Handlebars,
        _fs = Fs,
        _events = {},
        
        _views = _fs.views,
        _fsevent = _fs.events,
        _parent = _fs,
        _selector = _fs.Selector,
        _tpls = _handlebars.templates;
    
    
    var _win = window,
        _body = document.body;
    _win.addEventListener('orientationchange', function() {
        var sid = setTimeout(function() {
            _win.scrollTo(_body.scrollLeft, _body.scrollTop);
            clearTimeout(sid);
        }, 0);
    }, false);
    
    
    _handlebars.registerHelper('getIndexLetter', function(index) {
        return String.fromCharCode(97 + (index % 26));
    });
    _handlebars.registerHelper('getLengthLetter', function(array) {
        var length = array.length;
        if (length < 2) {
            return 'a';
        }
        return String.fromCharCode(95 + length);
    });
    
    function _removeUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }
        var len = ui.length;
        while (len--) {
            _selector.removeClass(el, pattern.replace('{{ui}}', ui[len]));
        }
    }
    function _addUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }
        var len = ui.length;
        while (len--) {
            _selector.addClass(el, pattern.replace('{{ui}}', ui[len]));
        }
    }
    function updateMarginTop(self) {
        if (self.el) {
            return;
        }
        var el = self.el,
            height = el.offsetHeight / 2;
        el.style.marginTop = '-' + height + 'px';
    }
    _events.alert = {
        show: function(self) {
            if (self.el) {
                updateMarginTop(self);
            } else {
                var sid = setTimeout(function() {
                    updateMarginTop(self);
                    clearTimeout(sid);
                }, 150);
            }
        }
    };
    _events.tabbuton = {
        activate: function() {
            // !TODO
        },
        deactivate: function() {
            // !TODO
        }
    };
    _events.collapsible = {
        afterrender: function(self) {
            self.iconEl = _selector.get('.ui-icon', self.el);
            self.iconCollapsed = 'ui-icon-' + (self.config.iconCollapsed || 'plus');
            self.iconExpanded = 'ui-icon-' + (self.config.iconExpanded || 'minus');
        },
        collapse: function(self) {
            _selector.addClass(self.headerEl, 'ui-collapsible-heading-collapsed');
            _selector.addClass(self.contentEl, 'ui-collapsible-content-collapsed');
            _selector.addClass(self.el, 'ui-collapsible-collapsed');
            // icon
            _selector.removeClass(self.iconEl, self.iconExpanded);
            _selector.addClass(self.iconEl, self.iconCollapsed);
        },
        expand: function(self) {
            _selector.removeClass(self.headerEl, 'ui-collapsible-heading-collapsed');
            _selector.removeClass(self.contentEl, 'ui-collapsible-content-collapsed');
            _selector.removeClass(self.el, 'ui-collapsible-collapsed');
            // icon
            _selector.removeClass(self.iconEl, self.iconCollapsed);
            _selector.addClass(self.iconEl, self.iconExpanded);
        }
    };
    _handlebars.registerHelper('getLabelFromValue', function(value, values) {
        var item,
            i = values.length;
        while (i--) {
            item = values[i];
            if (item.value === value) {
                return item.label;
            }
        }
        return value;
    });
    _events.select = {
        change: function(self, item) {
            var textEl = _selector.get('.ui-btn-text', self.uiEl);
            if (textEl) {
                textEl.innerHTML = item.label;
            }
        },
        loading: function(cmp, isLoading) {
            var textEl = _selector.get('.ui-btn-text', cmp.uiEl);
            if (isLoading) {
                cmp.setDisabled(true);
                if (cmp.config.emptyText) {
                    textEl.innerHTML = 'Loading';
                }
            } else {
                cmp.setDisabled(false);
                if (cmp.config.emptyText) {
                    textEl.innerHTML = cmp.config.emptyText;
                }
            }
        },
        disabled: function(cmp, isDisabled) {
            var btnEl = _selector.get('.ui-btn', cmp.uiEl);
            if (isDisabled) {
                _selector.addClass(btnEl, 'ui-disabled');
            } else {
                _selector.removeClass(btnEl, 'ui-disabled');
            }
        }
    };
    _events.button = {
        disabled: function(cmp, isDisabled) {
            if (isDisabled) {
                _selector.addClass(cmp.el, 'ui-disabled');
            } else {
                _selector.removeClass(cmp.el, 'ui-disabled');
            }
        }
    };
    var _setUIBtn = function(ui) {
        _removeUI('ui-btn-up-{{ui}}', this.config.ui, this.el);
        _addUI('ui-btn-up-{{ui}}', ui, this.el);
        this.config.ui = ui;
    };
    if (_views.Button) {
        _views.Button.prototype.setUI = _setUIBtn;
    }
    if (_views.TabButton) {
        _views.TabButton.prototype.setUI = _setUIBtn;
    }
    var _setUIBar = function(ui) {
        _removeUI('ui-bar-{{ui}}', this.config.ui, this.el);
        _addUI('ui-bar-{{ui}}', ui, this.el);
        this.config.ui = ui;
    };
    if (_views.Header) {
        _views.Header.prototype.setUI = _setUIBar;
    }
    if (_views.Footer) {
        _views.Footer.prototype.setUI = _setUIBar;
    }
    if (_views.Toolbar) {
        _views.Toolbar.prototype.setUI = _setUIBar;
    }
    _events.radiobutton = {
        check: function() {
            if (!this.iconEl) {
                this.iconEl = _selector.get('.ui-icon', this.uiEl);
            }
            _selector.removeClass(this.iconEl, 'ui-icon-radio-off');
            _selector.addClass(this.iconEl, 'ui-icon-radio-on');
            _selector.removeClass(this.uiEl, 'ui-radio-off');
            _selector.addClass(this.uiEl, 'ui-radio-on');
        },
        uncheck: function() {
            if (!this.iconEl) {
                this.iconEl = _selector.get('.ui-icon', this.uiEl);
            }
            _selector.removeClass(this.iconEl, 'ui-icon-radio-on');
            _selector.addClass(this.iconEl, 'ui-icon-radio-off');
            _selector.removeClass(this.uiEl, 'ui-radio-on');
            _selector.addClass(this.uiEl, 'ui-radio-off');
        }
    };
    _events.panel = {
        afterrender: function() {
            overthrow.set();
        },
        expand: function() {
            var page = _selector.get('.ui-page-active');
            _selector.addClass(page, 'active');
            _selector.addClass(this.el, 'active');
        },
        collapse: function() {
            var page = _selector.get('.ui-page-active');
            _selector.removeClass(page, 'active');
            _selector.removeClass(this.el, 'active');
        }
    };
    _handlebars.registerHelper('isFlipSwitchOptionActive', function(values, index, value, options) {
        value = value || values[0].value;
        var current = values[index].value;
        if (current === value) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    _events.flipswitch = {
        afterrender: function() {
            var on = _selector.get('.ui-btn-active', this.uiEl),
                off = _selector.get('.ui-btn-down-c', this.uiEl),
                handle = _selector.get('.ui-slider-handle', this.uiEl);
            this.on('check', function() {
                on.style.width = '100%';
                off.style.width = '0%';
                handle.style.left = '100%';
            }, this, this.priority.VIEWS);
            this.on('uncheck', function() {
                on.style.width = '0%';
                off.style.width = '100%';
                handle.style.left = '0%';
            }, this, this.priority.VIEWS);
        }
    };
    _events.textarea = {
        afterrender: function() {
            this.eventFocus = new _fsevent.Focus({
                autoAttach: true,
                el: this.el,
                scope: this,
                handler: function() {
                    _selector.addClass(this.el, 'ui-focus');
                }
            });
            this.eventBlur = new _fsevent.Blur({
                autoAttach: true,
                el: this.el,
                scope: this,
                handler: function() {
                    _selector.removeClass(this.el, 'ui-focus');
                }
            });
        }
    };
    _events.checkbox = {
        afterrender: function() {
            var iconEl = _selector.get('.ui-icon', this.uiEl);
            this.on('check', function() {
                _selector.removeClass(this.uiEl, 'ui-checkbox-off');
                _selector.removeClass(iconEl, 'ui-icon-checkbox-off');
                _selector.addClass(this.uiEl, 'ui-checkbox-on');
                _selector.addClass(iconEl, 'ui-icon-checkbox-on');
            }, this, this.priority.VIEWS);
            this.on('uncheck', function() {
                _selector.removeClass(this.uiEl, 'ui-checkbox-on');
                _selector.removeClass(iconEl, 'ui-icon-checkbox-on');
                _selector.addClass(this.uiEl, 'ui-checkbox-off');
                _selector.addClass(iconEl, 'ui-icon-checkbox-off');
            }, this, this.priority.VIEWS);
        }
    };
    _events.textfield = {
        afterrender: function(self) {
            var parentNode = self.el.parentNode;
            self.eventFocus = new _fsevent.Focus({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function() {
                    _selector.addClass(parentNode, 'ui-focus');
                }
            });
            self.eventBlur = new _fsevent.Blur({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function() {
                    _selector.removeClass(parentNode, 'ui-focus');
                }
            });
            if (self.config.clearBtn) {
                self.clearBtnEl = document.getElementById(self.config.id + '-clearbtn');
                var keyupHandler = function() {
                    if (self.config.clearBtn <= self.el.value.length) {
                        _selector.removeClass(self.clearBtnEl, 'ui-input-clear-hidden');
                    } else {
                        _selector.addClass(self.clearBtnEl, 'ui-input-clear-hidden');
                    }
                };
                self.eventKeyup = new _fsevent.Abstract({
                    eventName: 'keyup',
                    autoAttach: true,
                    el: self.el,
                    globalFwd: window,
                    handler: keyupHandler
                });
                self.eventClick = new _fsevent.Click();
                self.eventClick.attach(self.clearBtnEl, function() {
                    if (_selector.hasClass(self.clearBtnEl, 'ui-disabled')) {
                        return false;
                    }
                    self.el.value = '';
                    keyupHandler.call(self);
                }, self);
            }
        },
        disabled: function(cmp, isDisabled) {
            var s = _selector;
            if (isDisabled) {
                s.addClass(cmp.el, 'ui-disabled');
                if (cmp.clearBtnEl) {
                    s.addClass(cmp.clearBtnEl, 'ui-disabled');
                }
            } else {
                s.removeClass(cmp.el, 'ui-disabled');
                if (cmp.clearBtnEl) {
                    s.removeClass(cmp.clearBtnEl, 'ui-disabled');
                }
            }
        }
    };
    return _parent.subclass({
        xtype: 'jquerymobile',
        className: 'Fs.engines.JqueryMobile',
        version: '1.3.1',
        constructor: function() {
            var s = _fs.Selector,
                html = s.get('html'),
                body = s.get('body');
            s.addClass(html, 'ui-mobile');
            s.addClass(body, 'ui-mobile-viewport');
        },
        getTpl: function(name) {
            return (_tpls[name] ||
                console.error(this.className + ' engine: Template "%s" does not exists.', name));
        },
        getEvents: function(name) {
            return _events[name];
        }
    });
}());
Fs.helpers.Env = (function() {
    var _env = platform,
        _win = window;
    // document.addEventListener('deviceready', function() {
    //     document.removeEventListener('deviceready', arguments.callee, false);
    //     _env.phonegap = true;
    // }, false);
    if (_win.cordova || _win.PhoneGap || _win.phonegap) {
        _env.phonegap = true;
    } else {
        _env.phonegap = false;
    }
    return _env;
}());
Fs.helpers.Network = (function() {
    'use strict';
    var _fs = Fs,
        _parent = _fs,
        _env = _fs.helpers.Env,
        _events = new _fs.Event(),
        _nav = navigator,
        _online = (_nav && typeof _nav.onLine === 'boolean' ? _nav.onLine : true),
        _scope = (_env.phonegap ? document : window);
    function offline() {
        _online = false;
        _events.fire('offline');
        _events.fire('status', false);
    }
    function online() {
        _online = true;
        _events.fire('online');
        _events.fire('status', true);
    }
    function ready() {
        _scope.removeEventListener('deviceready', ready, false);
        _online = (_nav && typeof _nav.onLine === 'boolean' ? _nav.onLine : true);
        _scope.addEventListener('offline', offline, false);
        _scope.addEventListener('online', online, false);
    }
    if (_env.phonegap) {
        _scope.addEventListener('deviceready', ready, false);
    } else {
        _scope.addEventListener('offline', offline, false);
        _scope.addEventListener('online', online, false);
    }
    var Network = _parent.subclass({
        constructor: function() {
        },
        isOnline: function() {
            return _online;
        },
        status: function(callback, scope, priority) {
            return _events.on('status', callback, scope, priority);
        },
        online: function(callback, scope, priority) {
            return _events.on('online', callback, scope, priority);
        },
        offline: function(callback, scope, priority) {
            return _events.on('offline', callback, scope, priority);
        },
        detach: function(uid) {
            return _events.off(uid);
        }
    });
    return (new Network());
}());
/**
 * @class Fs.views.Template
 * @extends Fs.Event
 * @xtype template
 *
 * Base view template class.
 */
Fs.views.Template = (function() {
    'use strict';
    var _fs = Fs,
        _gconf = _fs.config,
        _parent = _fs.Event,
        _selector = _fs.Selector,
        _handlebars = Handlebars;
        //_observer = _win.MutationObserver || _win.WebKitMutationObserver
    function _registerEngineEvents(self) {
        if (!self.renderer ||
            self.engineEventsRegistered) {
            return false;
        }
        self.engineEventsRegistered = true;
        var i, e, events = self.engine.getEvents(self.xtype);
        for (i in events) {
            e = events[i];
            if (typeof e === 'function') {
                if (i.indexOf('render') >= 0) {
                    self.renderer.on(i, events[i], self, self.priority.VIEWS);
                } else {
                    self.on(i, events[i], self, self.priority.VIEWS);
                }
            }
        }
        return true;
    }
    /*
    function _registerEngineOverrides() {
        if (this.engineOverridesRegistered) {
            return false;
        }
        this.engineOverridesRegistered = true;
        var i, o, overrides = this.engine.getOverrides(this.xtype);
        for (i in overrides) {
            o = overrides[i];
            if (typeof o === 'function') {
                this[i] = o;
            }
        }
        return true;
    };
    */
    return _parent.subclass({
        className: 'Fs.views.Template',
        xtype: 'template',
        /**
         * New template.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {
            var self = this;
            opts = opts || {
                template: '',
                config: {},
                items: []
            };
            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtpl = opts.xtpl;
            }
            /**
             * @cfg {Object} config (optional) Template configuration.
             * This variable will be the scope while executing template.
             */
            self.config = opts.config || {};
            if (!self.config.id) {
                self.config.id = _selector.generateId();
            }
            // <testing>
            //delete opts.config;
            //self.config = opts;
            // </testing>
            if (opts.defaults) {
                self.defaults = opts.defaults;
            }
            
            self.items = opts.items || [];
            if (!self.items.indexOf) {
                self.items = [self.items];
            }
            self.data = self.config;
            self.data.items = [];
            
            if (!opts.template) {
                opts.template = self.setEngine(opts, _gconf).getTpl(self.xtpl || self.xtype);
            } else {
                self.setEngine(opts, _gconf);
            }
            if (!opts.ui && _gconf.ui) {
                opts.ui = _gconf.ui;
            }
            if (opts.ref) {
                self.ref = opts.ref;
            }
            if (opts.refScope) {
                self.refScope = opts.refScope;
            }
            self.tpl = opts.template;
            self.html = '';
            self.renderToEl = '';
            self.el = '';
            self.engineEventsRegistered = false;
            _parent.prototype.constructor.apply(self, arguments);
        },
        getEngine: function(config, gconfig) {
            if (config && config.engine) {
                return config.engine;
            } else if (gconfig) {
                return gconfig.engine;
            }
            return this.engine;
        },
        setEngine: function(config, gconfig) {
            var self = this;
            self.engine = self.getEngine(config, gconfig);
            self.engine = new self.engine();
            return self.engine;
        },
        
        compile: function(parentConfig, renderer) {
            var item, tpl,
                self = this,
                i = -1,
                items = self.items,
                length = self.items.length;
            if (parentConfig) {
                if (_gconf.ui && !parentConfig.ui) {
                    parentConfig.ui = _gconf.ui;
                }
                _fs.utils.applyIf(self.config, parentConfig, ['ui']);
            }
            while (++i < length) {
                item = items[i];
                if (self.defaults && typeof item === 'object') {
                    _fs.utils.applyIfAuto(item, self.defaults);
                }
                if (typeof item === 'object' &&
                    typeof item.xtype === 'string' &&
                    !(item instanceof _parent)) {
                    
                    
                    item = new (_fs.xtype(item.xtype))(item);
                    item.parent = self;
                }
                if (typeof item === 'string') {
                    tpl = _fs.Templates.compile(item);
                    self.data.items[i] = tpl(self.config);
                } else if (typeof item.compile === 'function') {
                    item.parent = self;
                    self.data.items[i] = item.compile(self.config, renderer || self);
                } else {
                    self.data.items[i] = item;
                }
            }
            _handlebars.currentParent.push(self);
            self.html = self.tpl(self.data);
            _handlebars.currentParent.pop();
            if (!self.renderer) {
                self.renderer = renderer || self;
                if (self.ref && !self.refScope) {
                    self.refScope = self.renderer;
                }
            }
            
            
            
            if (self.ref &&
                !self.refScope[self.ref]) {
                self.refScope[self.ref] = self;
            }
            
            self.__arID = self.renderer.on('afterrender', function() {
                self.el = document.getElementById(self.config.id);
                
                self.renderer.off(self.__arID);
                delete self.__arID;
            }, self, self.priority.CORE);
            self.setupListeners([
                
                'aftercompile',
                
                'afterrender'
            ], self.renderer);
            self.fire('aftercompile', self, self.renderer);
            _registerEngineEvents(self);
            return self.html;
        },
        
        render: function(renderToEl, position) {
            var self = this;
            position = position || 'beforeEnd';
            if (typeof renderToEl === 'string') {
                
                self.renderToEl = _fs.Selector.get(renderToEl);
            } else {
                self.renderToEl = renderToEl;
            }
            self.renderToEl.insertAdjacentHTML(position, self.html);
            self.fireScope('afterrender', self);
            if (!self.el) {
                self.el = document.getElementById(self.config.id);
            }
        },
        remove: function() {
            if (this.el) {
                this.el.parentNode.removeChild(this.el);
                delete this.el;
                
                
            } else {
                console.warn('Element cannot been removed because it has not been rendered !', this);
            }
        },
        
        setUI: function() {
            console.warn('setUI is not implemented for xtype "',
                this.xtype, '" and engine "', this.getEngine().xtype, '"');
        }
    });
}());
Fs.views.Alert = (function() {
    'use strict';
    
    var _fs = Fs,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;
    function _afterrender(self) {
        var id = self.config.id;
        self.setupListeners(['show', 'hide'], self);
        
        self.overlayEl = document.getElementById(id + '-overlay');
        self.closeEl = document.getElementById(id + '-close');
        self.clickEvent = new _fs.events.Click({
            disableScrolling: true
        });
        if (self.config.overlayHide !== false) {
            self.clickEvent.attach(self.overlayEl, self.hide, self);
        } else {
            self.clickEvent.attach(self.overlayEl, function() {}, self);
        }
        if (self.closeEl) {
            self.closeEvent = new _fs.events.Click({
                disableScrolling: true
            });
            self.closeEvent.attach(self.closeEl, self.hide, self);
        }
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }
    
    return _parent.subclass({
        className: 'Fs.views.Alert',
        xtype: 'alert',
        constructor: function(opts) {
            var self = this;
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },
        show: function() {
            var self = this;
            if (self.el) {
                _selector.removeClass(self.el, 'hidden');
                _selector.removeClass(self.overlayEl, 'hidden');
            }
            self.fire('show', self);
        },
        hide: function() {
            var self = this;
            if (self.el) {
                _selector.addClass(self.el, 'hidden');
                _selector.addClass(self.overlayEl, 'hidden');
            }
            self.fire('hide', self);
        }
    });
})();
Fs.helpers.Updater = (function() {
    var _alert,
        _fs = Fs,
        _net = _fs.helpers.Network,
        _request = _fs.Request,
        _storage = _fs.Storage,
        _parent = _fs.Event,
        _env = _fs.helpers.Env,
        _family = _env.os.family.toLowerCase(),
        _android = (_family.indexOf('android') > -1),
        _ios = (_family.indexOf('ios') > -1);
    function get_alert(self) {
        if (!_alert) {
            _alert = new _fs.views.Alert({
                config: {
                    header: {
                        title: 'Update',
                        ui: 'f'
                    },
                    style: 'position: fixed; top: 50%; left: 50%; margin-left: -140px; width: 280px; margin-top: -140px;'
                },
                listeners: {
                    scope: self,
                    show: function(self) {
                        self.el.style.marginTop = '-' + (self.el.offsetHeight / 2) + 'px';
                    }
                },
                items: [[
                    'An update is available for this application.'
                ].join(''), {
                    xtype: 'button',
                    config: {
                        ui: 'c',
                        text: 'OK',
                        inline: false
                    },
                    scope: self,
                    handler: function() {
                        _alert.hide();
                    }
                }]
            });
            _alert.compile();
            _alert.render('body', 'afterbegin');
        }
        return _alert;
    }
    function get_update(self) {
        self.request(self.url, {
            callback: function(success, data) {
                if (success) {
                    self.data = data;
                    _checkUpdate(self);
                    if (typeof self.sid === 'undefined') {
                        self.sid = setInterval(function() {
                            _checkUpdate(self);
                        }, self.interval);
                    }
                }
            }
        });
    }
    var _checkUpdate = function(self) {
        var data = self.data;
        self = self || this;
        if (self.currentVersion !== data.version) {
            get_alert(self).show();
        }
    };
    return _parent.subclass({
        defaultInverval: (86400 * 1000), // check every day
        constructor: function(opts) {
            var self = this;
            self.eids = {};
            self.currentVersion = opts.currentVersion;
            self.interval = opts.interval || self.defaultInverval;
            self.url = opts.url;
            self.request = _fs.Request[self.proxy] || _fs.Request.jsonp;
            _parent.prototype.constructor.call(self, opts);
            self.eids.online = _net.online(_checkUpdate, self);
            if (_net.isOnline()) {
                get_update(self);
            }
        }
    });
}());
/* i18n */
Fs.helpers.i18n = (function(gscope) {
    'use strict';
    var _fs = Fs,
        // stores all languages available (after load)
        _langs = {},
        // stores current language
        _currentLang = {},
        // activated language
        _lang = 'en',
        _defaultScope = gscope,
        _handlebars = Handlebars,
        _parent = _fs;
    function _t(str) {
        return (_currentLang[str] || str);
    }
    if (_handlebars) {
        _handlebars.registerHelper('t', _t);
    }
    _defaultScope.t = _t;
    return _parent.subclass({
        setLang: function(name) {
            _lang = name;
            _currentLang = _langs[_lang];
        },
        addLang: function(name, value, setDefault) {
            _langs[name] = value;
            if (setDefault) {
                this.setLang(name);
            }
        }
    });
}(this));
/* views/popup.js */
Fs.views.Popup = (function() {
    'use strict';
    
    var _fs = Fs,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;
    function _afterrender(self) {
        var id = self.config.id;
        self.overlayEl = document.getElementById(id + '-overlay');
        self.closeEl = document.getElementById(id + '-close');
        if (self.config.overlayHide !== false) {
            self.clickEvent = new _fs.events.Click({
                disableScrolling: true,
                autoAttach: true,
                el: self.overlayEl,
                scope: self,
                handler: self.hide
            });
        }
        if (self.closeEl) {
            self.closeEvent = new _fs.events.Click({
                disableScrolling: true,
                autoAttach: true,
                el: self.closeEl,
                scope: self,
                handler: self.hide
            });
        }
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }
    
    return _parent.subclass({
        className: 'Fs.views.Popup',
        xtype: 'popup',
        constructor: function(opts) {
            var self = this;
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },
        show: function() {
            var self = this;
            if (self.el) {
                _selector.removeClass(self.el, 'hidden');
                _selector.removeClass(self.overlayEl, 'hidden');
            }
            self.fire('show', self);
        },
        hide: function() {
            var self = this;
            if (self.el) {
                _selector.addClass(self.el, 'hidden');
                _selector.addClass(self.overlayEl, 'hidden');
            }
            self.fire('hide', self);
        }
    });
})();
Fs.views.Page = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.views.Template;
    
    return _parent.subclass({
        className: 'Fs.views.Page',
        xtype: 'page'
    });
})();
Fs.views.Container = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.views.Template;
    
    return _parent.subclass({
        className: 'Fs.views.Container',
        xtype: 'container'
    });
})();
Fs.views.Footer = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.views.Template;
    
    return _parent.subclass({
        className: 'Fs.views.Footer',
        xtype: 'footer'
    });
})();
Fs.views.Tabs = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.views.Template;
    function _afterrender(self) {
        self.eventClick = new _fs.events.Click({
            autoAttach: true,
            el: self.el,
            scope: self,
            handler: self.handler
        });
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }
    
    return _parent.subclass({
        className: 'Fs.views.Tabs',
        xtype: 'tabs',
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            if (opts.handler) {
                self.handler = opts.handler;
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },
        handler: function(event) {
            var self = this,
                route = self.config.route;
            self.fire('select', self, event);
            if (route) {
                _fs.History.navigate(route);
            }
        }
    });
})();
Fs.views.Button = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.views.Template;
    function _afterrender(self) {
        self.eventClick = new _fs.events.Click({
            autoAttach: true,
            el: self.el,
            scope: self,
            handler: self.handler
        });
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }
    
    return _parent.subclass({
        className: 'Fs.views.Button',
        xtype: 'button',
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            if (opts.handler) {
                self.handler = (opts.scope ?
                    opts.handler.bind(opts.scope) : opts.handler);
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },
        handler: function(evt) {
            var self = this,
                route = self.config.route;
            self.fire('click', self, evt);
            if (route) {
                _fs.History.navigate(route);
            }
        },
        setDisabled: function(disabled) {
            var self = this;
            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        }
    });
})();
Fs.views.TabButton = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.views.Button;
    
    return _parent.subclass({
        className: 'Fs.views.TabButton',
        xtype: 'tabbutton',
        handler: _parent.prototype.handler,
        activate: function() {
            this.active = true;
            this.fire('activate', this);
        },
        deactivate: function() {
            this.active = false;
            this.fire('deactivate', this);
        }
    });
})();
Fs.views.Header = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.views.Template;
    
    return _parent.subclass({
        className: 'Fs.views.Header',
        xtype: 'header'
    });
})();
Fs.views.Abstract = (function() {
    'use strict';
    
    var _fs = Fs,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;
    
    return _parent.subclass({
        className: 'Fs.views.Abstract',
        xtype: 'abstract',
        constructor: function(opts) {
            opts = opts || {};
            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtype = opts.xtpl;
            }
            _parent.prototype.constructor.call(this, opts);
        }
    });
})();
Fs.views.Flash = (function() {
    var _fs = Fs,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;
    document.addEventListener('DOMContentLoaded', function() {
        var style = document.createElement('style');
        style.innerHTML = [
            '.flash-alert {',
                'color: rgb(185, 74, 72);',
                'background-color: rgb(242, 222, 222);',
                'border-color: rgb(238, 211, 215);',
            '}',
            '.flash-warning {',
                'color: rgb(192, 152, 83);',
                'background-color: rgb(252, 248, 227);',
                'border-color: rgb(251, 238, 213);',
            '}',
            '.flash {',
                'padding: 5px 7px;',
                'font-size: 14px;',
                'line-height: 18px;',
                'border-radius: 4px;',
            '}',
            '.flash .icon {',
                'margin: 0 5px 0 5px;',
                'font-size: 30px;',
                'line-height: 18px;',
                'display: inline-block;',
            '}'
        ].join('');
        style.id = 'flash-style';
        document.head.appendChild(style);
        document.removeEventListener('DOMContentLoaded', arguments.callee, false);
    }, false);
    var _onafterrender = function() {
        if (this.hideonclick === true) {
            this.clickEvent = new _fs.events.Click({
                el: this.el,
                scope: this,
                handler: this.hide
            });
        }
    };
    var _onaftercompile = function() {
        this.off(this.eid);
        this.renderer.on('afterrender', _onafterrender,
            this, this.priority.VIEWS);
        this.renderer.on('show', _onshow,
            this, this.priority.VIEWS);
        this.renderer.on('hide', _onhide,
            this, this.priority.VIEWS);
    };
    var _onshow = function() {
        this.clickEvent.attach();
    };
    var _onhide = function() {
        this.clickEvent.detach();
    };
    return _parent.subclass({
        xtype: 'flash',
        className: 'Fs.views.Flash',
        defaultHideonclick: false,
        constructor: function(opts) {
            if (typeof opts.hideonclick === 'boolean') {
                this.hideonclick = opts.hideonclick;
            } else {
                this.hideonclick = this.defaultHideonclick;
            }
            _parent.prototype.constructor.call(this, opts);
            if (this.hideonclick === true) {
                this.eid = this.on('aftercompile', _onaftercompile,
                    this, this.priority.VIEWS);
            }
        },
        show: function() {
            _selector.removeClass(this.el, 'hidden');
        },
        hide: function() {
            _selector.addClass(this.el, 'hidden');
        }
    });
})();
Fs.views.Textfield = (function() {
    'use strict';
    
    var _fs = Fs,
        
        _parent = _fs.views.Template;
    
    return _parent.subclass({
        className: 'Fs.views.Textfield',
        xtype: 'textfield',
        setDisabled: function(disabled) {
            var self = this;
            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        }
    });
})();
Fs.data.Store = (function() {
    'use strict';
    var _fs = Fs,
        _storage = _fs.Storage,
        _debug = _fs.debug,
        _fsrequest = _fs.Request,
        _parent = _fs.Event,
        _win = window,
        _online = _win.navigator.onLine;
    _win.addEventListener('online', function() {
        _online = true;
    }, false);
    _win.addEventListener('offline', function() {
        _online = false;
    }, false);
    
    
    return _parent.subclass({
        xtype: 'store',
        defaultEnableHashTable: false,
        defaultReaderRoot: 'data',
        defaultReaderTotal: 'total',
        defaultReaderSuccess: 'success',
        defaultReaderId: 'id',
        defaultStorageCache: false,
        
        constructor: function(config) {
            this.config = config || {};
            this.totalRecords = config.totalRecords || 0;
            this.loadedRecords = 0;
            this.records = [];
            
            this.isLoading = false;
            
            this.enableHashTable = config.enableHashTable || this.defaultEnableHashTable;
            
            this.storageCache = config.storageCache || this.defaultStorageCache;
            if (this.enableHashTable) {
                this.hashRecords = {};
            }
            this.rootProperty = this.getReaderRoot(this.defaultReaderRoot);
            this.totalProperty = this.getReaderTotal(this.defaultReaderTotal);
            this.successProperty = this.getReaderSuccess(this.defaultReaderSuccess);
            this.idProperty = this.getReaderId(this.defaultReaderId);
            _parent.prototype.constructor.apply(this, arguments);
            this.setupListeners([
                
                'load',
                
                'afterload',
                
                'beforeload',
                
                'loaderror'
            ]);
        },
        
        load: function(opts) {
            opts = _fs.utils.applyIfAuto(opts || {}, this.getProxyOpts());
            this.isLoading = true;
            var request,
                self = this,
                type = this.getProxyType(),
                finalCb = opts.callback;
            this.fire('beforeload', this, opts);
            var cb = function(success, data) {
                var total,
                    newRecords = [];
                if (success && data[self.successProperty]) {
                    total = data[self.totalProperty];
                    if (typeof total !== 'undefined') {
                        self.totalRecords = total;
                    }
                    newRecords = data[self.rootProperty];
                    if (self.enableHashTable) {
                        var recordId, length = newRecords.length;
                        while (length--) {
                            recordId = newRecords[length][self.idProperty];
                            self.hashRecords['r' + recordId] = self.loadedRecords + length;
                        }
                    }
                    self.loadedRecords += newRecords.length;
                    self.records = self.records.concat(newRecords);
                } else {
                    self.fire('loaderror', success, self, data);
                }
                self.fire('load', success, self, newRecords);
                if (finalCb) {
                    finalCb(success, self, newRecords);
                }
                self.fire('afterload', success, self, newRecords);
                self.isLoading = false;
            };
            opts.url = opts.url || this.getProxyUrl();
            if (this.storageCache) {
                var key = JSON.stringify(_fs.utils.crc32(JSON.stringify(opts))),
                    results = _storage.getItem(key);
                if (_online === false && results) {
                    setTimeout(function() {
                        cb(true, results);
                    }, 10);
                    return true;
                } else {
                    var tmp = cb;
                    cb = function(success, data) {
                        if (success && data[self.successProperty]) {
                            _storage.setItem(key, data);
                        }
                        tmp(success, data);
                    };
                }
            }
            
            
            
            opts.callback = cb;
            if (typeof _fsrequest[type] === 'function') {
                request = _fsrequest[type];
            } else {
                request = _fsrequest.ajax;
            }
            return request(opts.url, opts, this);
        },
        
        addRecord: function(record, fromStart) {
            var recordId = record[this.idProperty];
            if (this.enableHashTable) {
                this.hashRecords['r' + recordId] = this.loadedRecords;
            }
            if (fromStart === true) {
                this.records = [record].concat(this.records);
                if (this.enableHashTable) {
                    var rec,
                        i = this.loadedRecords + 1;
                    while (i--) {
                        rec = this.records[i];
                        this.hashRecords['r' + rec[this.idProperty]] = i;
                    }
                }
            } else {
                this.records = this.records.concat([record]);
            }
            this.loadedRecords += 1;
            this.totalRecords += 1;
        },
        
        reset: function() {
            this.records = [];
            this.totalRecords = 0;
            this.loadedRecords = 0;
            if (this.enableHashTable) {
                this.hashRecords = {};
            }
        },
        
        
        
        
        
        getRecord: function(recId) {
            
            if (!this.enableHashTable) {
                _debug.error('store', 'You must enable hash table for using store.getRecord by setting enableHashTable: true in store config.');
                return;
            }
            
            return this.records[this.hashRecords['r' + recId]];
        },
        
        getAt: function(index) {
            return this.records[index];
        },
        setProxyOpts: function(opts) {
            if (!this.config.proxy) {
                this.config.proxy = {};
            }
            this.config.proxy.opts = opts;
            return true;
        },
        getProxyOpts: function(defaultValue) {
            defaultValue = defaultValue || {};
            var p = this.config.proxy;
            if (typeof p === 'object') {
                return p.opts || defaultValue;
            }
            return defaultValue;
        },
        getProxyUrl: function(defaultValue) {
            defaultValue = defaultValue || '';
            var p = this.config.proxy;
            if (typeof p === 'object') {
                return p.url || defaultValue;
            }
            return defaultValue;
        },
        getProxyCache: function(defaultValue) {
            defaultValue = defaultValue || true;
            var p = this.config.proxy;
            if (typeof p === 'object') {
                return (p.cache === false ? false : defaultValue);
            }
            return defaultValue;
        },
        getProxyType: function(defaultValue) {
            defaultValue = defaultValue || 'ajax';
            var p = this.config.proxy;
            if (typeof p === 'object') {
                return p.type || defaultValue;
            }
            return defaultValue;
        },
        getReaderRoot: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderRoot;
            var reader = this.config.reader;
            if (typeof reader === 'object') {
                return reader.rootProperty || defaultValue;
            }
            return defaultValue;
        },
        getReaderTotal: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderTotal;
            var reader = this.config.reader;
            if (typeof reader === 'object') {
                return reader.totalProperty || defaultValue;
            }
            return defaultValue;
        },
        getReaderSuccess: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderSuccess;
            var reader = this.config.reader;
            if (typeof reader === 'object') {
                return reader.successProperty || defaultValue;
            }
            return defaultValue;
        },
        getReaderId: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderId;
            var reader = this.config.reader;
            if (typeof reader === 'object') {
                return reader.idProperty || defaultValue;
            }
            return defaultValue;
        }
    });
}());
Fs.data.PagingStore = (function() {
    'use strict';
    var _fs = Fs,
        _parent = _fs.data.Store;
    return _parent.subclass({
        xtype: 'pagingstore',
        
        constructor: function(config) {
            config = config || {};
            
            this.currentPage = config.currentPage || 0;
            
            this.recordPerPage = config.recordPerPage || 10;
            _parent.prototype.constructor.call(this, config);
        },
        
        loadPage: function(page, opts) {
            opts = opts || {};
            opts.url = opts.url || this.getProxyUrl();
            var nb = this.recordPerPage,
                paging = 'start=' + (page * nb) +
                    '&limit=' + nb;
            if (opts.url.indexOf('?') !== -1) {
                opts.url += '&' + paging;
            } else {
                opts.url += '?' + paging;
            }
            return this.load(opts);
        },
        
        nextPage: function(opts) {
            return this.loadPage(this.currentPage++, opts);
        },
        
        previousPage: function(opts) {
            return this.loadPage(this.currentPage--, opts);
        },
        
        reset: function() {
            this.currentPage = 0;
            _parent.prototype.reset.apply(this, arguments);
        }
    });
}());
Fs.views.ListBuffered = (function() {
    'use strict';
    
    var _window = window,
        _fs = Fs,
        _events = _fs.events,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;
    
    function _onshow(self) {
        self.scrollEvent.attach();
        if (self.clickEvent) {
            self.clickEvent.attach();
        }
    }
    function _onhide(self) {
        self.scrollEvent.detach();
        if (self.clickEvent) {
            self.clickEvent.detach();
        }
    }
    function _afterrender(self) {
        self.loadingEl = self.el.parentNode.querySelector('.list-loader');
        if (self.hasListener('itemselect')) {
            self.clickEvent = new _events.Click({
                el: self.el,
                scope: (self.listeners.scope ? self.listeners.scope : undefined),
                handler: self.getListener('itemselect')
            });
        }
        self.scrollEvent = new _events.Scroll({
            el: self.el,
            scope: self,
            handler: self.scrollHandler
        });
        self.loadNext();
        self.renderer.on('show', _onshow, self, self.priority.VIEWS);
        self.renderer.on('hide', _onhide, self, self.priority.VIEWS);
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }
    
    return _parent.subclass({
        
        deltaPixels: 100,
        className: 'Fs.views.ListBuffered',
        xtype: 'listbuffered',
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            self.store = opts.store;
            if (opts.listeners) {
                self.listeners = opts.listeners;
            }
            self.emptyTpl = opts.emptyTpl || false;
            self.isLoading = false;
            if (typeof opts.loading === 'boolean') {
                self.loading = opts.loading;
            } else {
                self.loading = true;
            }
            if (typeof opts.emptyBeforeLoad === 'boolean') {
                self.emptyBeforeLoad = opts.emptyBeforeLoad;
            } else {
                self.emptyBeforeLoad = true;
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
            self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = self.engine.getTpl(self.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    self.itemTpl = engineItemTpl;
                }
            }
        },
        getItemFromEvent: function(evt) {
            return _selector.findParentByClass(evt.target, 'list-item');
        },
        scrollHandler: function() {
            var p = document.body,
                
                screenHeight = _window.screen.availHeight,
                scrollHeight = p.scrollHeight - this.deltaPixels,
                totalScroll = p.scrollTop + screenHeight;
            if (totalScroll >= scrollHeight) {
                this.loadNext();
            }
        },
        reload: function() {
            var self = this;
            if (self.isLoading === true) {
                return false;
            }
            self.isLoading = true;
            if (self.emptyBeforeLoad === true) {
                self.el.innerHTML = '';
            } else {
                self.needEmpty = true;
            }
            if (self.loading === true) {
                self.showLoading();
            } else {
                self.hideLoading();
            }
            self.store.reset();
            self.store.nextPage({
                callback: self.getNext.bind(self)
            });
        },
        loadNext: function() {
            var self = this;
            if (self.isLoading === true) {
                return false;
            } else if (self.store.loadedRecords &&
                self.store.loadedRecords === self.store.totalRecords) {
                self.hideLoading();
                return false;
            }
            self.isLoading = true;
            self.showLoading();
            self.store.nextPage({
                callback: self.getNext.bind(self)
            });
        },
        getNext: function(success, store, newRecords) {
            var conf,
                self = this,
                itemTpl = this.itemTpl || _itemTpl;
            if (self.needEmpty === true) {
                self.el.innerHTML = '';
                delete self.needEmpty;
            }
            conf = self.config;
            conf.items = newRecords;
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(conf));
            
            
            
            _selector.redraw(self.el);
            
            if (store.loadedRecords === store.totalRecords) {
                self.hideLoading();
            } else if (store.totalRecords && self.loading === false) {
                self.showLoading();
            }
            self.isLoading = false;
            if (!store.totalRecords) {
                self.scrollEvent.detach();
                if (self.emptyTpl) {
                    _selector.updateHtml(self.el, self.emptyTpl);
                }
            }
        },
        showLoading: function() {
            _selector.removeClass(this.loadingEl, 'list-loader-hide');
        },
        hideLoading: function() {
            _selector.addClass(this.loadingEl, 'list-loader-hide');
        }
    });
})();
Fs.views.List = (function() {
    'use strict';
    
    var _fs = Fs,
        _handlebars = Handlebars,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;
    
    function _afterrender(self) {
        var id = self.config.id;
        self.loadingEl = document.getElementById(id + '-loader');
        if (self.hasListener('itemselect')) {
            self.clickEvent = new _fs.events.Click({
                autoAttach: true,
                el: self.el,
                scope: (self.listeners.scope ? self.listeners.scope : undefined),
                handler: self.getListener('itemselect')
            });
        }
        if (self.autoload) {
            self.load();
        }
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }
    
    return _parent.subclass({
        className: 'Fs.views.List',
        xtype: 'list',
        defaultAutoload: true,
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            self.store = opts.store;
            if (opts.listeners) {
                self.listeners = opts.listeners;
            }
            self.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : self.defaultAutoload);
            if (opts.config) {
                opts.config.isLoading = self.autoload;
            }
            self.emptyTpl = opts.emptyTpl || false;
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
            self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = self.engine.getTpl(self.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    self.itemTpl = engineItemTpl;
                }
            }
        },
        getItemFromEvent: function(evt) {
            return _selector.findParentByClass(evt.target, 'list-item');
        },
        reload: function(items) {
            var self = this;
            if (self.isLoading === true) {
                return false;
            }
            self.el.innerHTML = '';
            
            if (self.store) {
                self.showLoading();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.items);
            }
        },
        showLoading: function() {
            var self = this;
            self.isLoading = true;
            _selector.removeClass(self.loadingEl, 'list-loader-hide');
        },
        hideLoading: function() {
            var self = this;
            self.isLoading = false;
            _selector.addClass(self.loadingEl, 'list-loader-hide');
        },
        load: function(items) {
            var self = this;
            if (self.isLoading === true) {
                return false;
            }
            if (self.store) {
                self.showLoading();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.items);
            }
        },
        loadItems: function(items) {
            var self = this,
                itemTpl = self.itemTpl,
                config = _fs.utils.applyAuto(self.config, {});
            config.items = items;
            _handlebars.currentParent.push(self);
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            _handlebars.currentParent.pop();
            if (self.abstractRenderer) {
                self.abstractRenderer.fireScope('afterrender', self.renderer);
            }
            self.hideLoading();
        },
        storeLoaded: function(success, store, newRecords) {
            var self = this;
            self.loadItems(newRecords);
            if (!store.totalRecords && self.emptyTpl) {
                _selector.updateHtml(self.el, self.emptyTpl);
            }
        }
    });
})();
Fs.views.FloatingPanel = (function() {
    
    var _fs = Fs,
        _selector = _fs.Selector,
        _win = window,
        _parent = _fs.views.Template;
    function _afterrender(self) {
        self.setupListeners(['show', 'hide'], self);
        self.overlayEl = document.getElementById(self.config.id + '-overlay');
        self.clickEvent = new _fs.events.Click({
            disableScrolling: true,
            el: self.overlayEl,
            scope: self,
            handler: self.hide
        });
        self.resizeEvent = new _fs.events.Resize({
            el: self.el,
            scope: self,
            handler: self.resize
        });
        if (self.hidden === false) {
            self.clickEvent.attach();
            self.resizeEvent.attach();
            self.resize();
        }
        self.on('show', _onshow, self, self.priority.VIEWS);
        self.on('hide', _onhide, self, self.priority.VIEWS);
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }
    function _onshow(self) {
        self.clickEvent.attach();
        self.resizeEvent.attach();
    }
    function _onhide(self) {
        self.clickEvent.detach();
        self.resizeEvent.detach();
    }
    function _find_parent_item(el) {
        while (el) {
            if (_selector.hasClass(el, 'floatingpanel')) {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    }
    function _stop_window_scroll(e) {
        if (_find_parent_item(e.target) === false) {
            if (e.stopPropagation) {
                e.stopPropagation();
            } else {
                e.cancelBubble = true;
            }
            if (e.preventDefault) {
                e.preventDefault();
            } else {
                e.returnValue = false;
            }
            return false;
        }
    }
    
    return _parent.subclass({
        className: 'Fs.views.FloatingPanel',
        xtype: 'floatingpanel',
        constructor: function(opts) {
            var self = this;
            self.hidden = (opts && opts.config && opts.config.hidden === true ? true : false);
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },
        show: function() {
            var self = this;
            self.hidden = false;
            self.resize();
            _selector.removeClass(self.el, 'hidden');
            _selector.removeClass(self.overlayEl, 'hidden');
            _selector.addClass(document.body, 'scroll-stop');
            _win.addEventListener('touchstart', _stop_window_scroll, false);
            self.fire('show', self);
        },
        hide: function() {
            var self = this;
            self.hidden = true;
            _selector.addClass(self.el, 'hidden');
            _selector.addClass(self.overlayEl, 'hidden');
            _selector.removeClass(document.body, 'scroll-stop');
            _win.removeEventListener('touchstart', _stop_window_scroll, false);
            self.fire('hide', self);
        },
        resize: function() {
            var self = this;
            if (self.hidden === false && !self.el.offsetWidth) {
                var sid = setTimeout(function() {
                    self.resize();
                    clearTimeout(sid);
                }, 100);
                return false;
            }
            var leftMargin,
                elWidth = self.el.offsetWidth,
                containerWidth = document.body.offsetWidth;
            leftMargin = (containerWidth - elWidth) / 2;
            self.el.style.left = leftMargin + 'px';
            return true;
        }
    });
})();
/* views/collapsible.js */
Fs.views.Collapsible = (function() {
    'use strict';
    
    var _fs = Fs,
        _parent = _fs.views.Template;
    function _afterrender(self) {
        var id = self.config.id;
        self.headerEl = document.getElementById(id + '-header');
        self.contentEl = document.getElementById(id + '-content');
        if (self.canCollapse) {
            self.eventClick = new Fs.events.Click({
                autoAttach: true,
                el: self.headerEl,
                scope: self,
                handler: self.handler
            });
        }
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }
    
    return _parent.subclass({
        className: 'Fs.views.Collapsible',
        xtype: 'collapsible',
        constructor: function(opts) {
            var self = this;
            opts = opts || {};
            opts.config = opts.config || {};
            self.collapsed = opts.config.collapsed || false;
            if (typeof opts.config.canCollapse !== 'undefined') {
                self.canCollapse = opts.config.canCollapse;
            } else {
                self.canCollapse = true;
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },
        toggle: function() {
            var self = this;
            if (self.collapsed) {
                return self.expand();
            }
            return self.collapse();
        },
        collapse: function() {
            var self = this;
            self.collapsed = true;
            self.fire('collapse', self);
        },
        expand: function() {
            var self = this;
            self.collapsed = false;
            self.fire('expand', self);
        },
        handler: function() {
            this.toggle();
        }
    });
})();
Fs.views.Carousel = (function() {
    'use strict';
    
    var _fs = Fs,
        _win = window,
        _touchSlider = _win.touchSlider,
        _parent = _fs.views.Template;
    function _afterrender(self) {
        var opts = {
            el: self.el
            
        };
        if (typeof self.height !== 'object') {
            opts.height = self.height;
        }
        if (self.animation) {
            opts.duration = self.animation;
            opts.afterslide = function() {
                if (self.stopAuto !== true && self.hidden === false) {
                    setTimeout(function() {
                        if (self.stopAuto !== true && self.hidden === false) {
                            self.slider.next();
                        }
                    }, 2000);
                }
            };
            opts.userslide = function() {
                self.stopAuto = true;
            };
        }
        self.slider = _touchSlider(opts);
    }
    function _onshow(self) {
        self.hidden = false;
        self.slider.resume();
        if (self.stopAuto !== true && self.animation) {
            var sid = setTimeout(function() {
                self.slider.next();
                clearTimeout(sid);
            }, 2500);
        }
    }
    function _onhide(self) {
        self.hidden = true;
        self.slider.pause();
    }
    function _aftercompile(self, renderer) {
        if (self.showEID) {
            renderer.off(self.showEID);
        }
        if (self.hideEID) {
            renderer.off(self.hideEID);
        }
        self.off(self.eid);
        self.arEID = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
        self.showEID = renderer.on('show', _onshow,
            self, self.priority.VIEWS);
        self.hideEID = renderer.on('hide', _onhide,
            self, self.priority.VIEWS);
    }
    
    return _parent.subclass({
        className: 'Fs.views.Carousel',
        xtype: 'carousel',
        constructor: function(opts) {
            var self = this;
            opts = opts || { config: {}};
            opts.config = opts.config || {};
            if (opts.config.animation) {
                self.animation = opts.config.animation;
            }
            if (opts.config.canFullscreen) {
                self.canFullscreen = opts.config.canFullscreen;
            }
            if (opts.config.height) {
                self.height = opts.config.height;
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        }
    });
})();
Fs.views.LoadMask = (function() {
    'use strict';
    var _fs = Fs,
        _selector = _fs.Selector,
        _parent = Fs.views.Template;
    return _parent.subclass({
        className: 'Fs.views.LoadMask',
        xtype: 'loadmask',
        show: function() {
            var self = this;
            _selector.removeClass(self.el, 'hidden');
            self.fire('show', self);
        },
        hide: function() {
            var self = this;
            _selector.addClass(this.el, 'hidden');
            self.fire('hide', self);
        }
    });
}());
Fs.views.AddToHome = (function() {
    'use strict';
    var _fs = Fs,
        _priority = _fs.Event.prototype.priority,
        _parent = _fs.views.Template;
    function _afterrender(self) {
        var close = function() {
            if (self.timeout) {
                var timeout = (new Date()).getTime() + (self.timeout * 1000.0);
                _fs.Storage.setItem('addtohome', timeout, window.location.host);
            }
            _fs.Selector.addClass(self.el, 'hidden');
        };
        self.closeEl = self.el.querySelector('.addtohome-close');
        self.clickEvent = new _fs.events.Click({
            autoAttach: true,
            el: self.closeEl,
            scope: self,
            handler: close
        });
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, _priority.VIEWS);
    }
    function canAddToHome() {
        var standalone = window.navigator.standalone;
        if (typeof standalone === 'boolean' &&
            standalone !== true &&
            (/iphone|ipod|ipad/gi).test(navigator.platform) &&
            (/Safari/i).test(navigator.appVersion)) {
            
            
            var isTimeout = _fs.Storage.getItem('addtohome', window.location.host),
                now = (new Date()).getTime();
            if (!isTimeout || isTimeout < now) {
                return true;
            }
        }
        return false;
    }
    window.canAddToHome = canAddToHome;
    return _parent.subclass({
        xtype: 'addtohome',
        className: 'Fs.views.AddToHome',
        constructor: function(opts) {
            var self = this;
            opts = opts || {
                config: {}
            };
            self.renderTo = opts.renderTo || null;
            self.timeout = opts.timeout || false;
            if ((/Android/i).test(navigator.appVersion)) {
                opts.config.isAndroid = true;
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, _priority.VIEWS);
            if (self.renderTo) {
                self.compile();
                self.render(self.renderTo);
            }
        }
    });
}());
Fs.views.Pull = (function() {
    'use strict';
    var _fs = Fs,
        _parent = _fs.views.Template;
    function _dom_loaded() {
        var style = document.createElement('style');
        style.innerHTML = [
            '.pull {',
                'padding: 0;',
                'height: 50px;',
                'text-align: center;',
            '}',
            '.pull-icon {',
                'display: none;',
                'font-size: 60px;',
                'margin-right: 10px;',
                'line-height: 50px;',
                'text-align: right;',
                'height: 50px;',
                'vertical-align: middle;',
                '-webkit-transform: rotate(0deg);',
                '-webkit-transition-property: color;',
                '-webkit-transition-duration: .5s;',
            '}',
            '.pull-show {',
                'display: inline-block;',
            '}',
            '.pull-text {',
                'text-align: left;',
                'display: inline-block;',
                'line-height: 50px;',
                'height: 50px;',
                'vertical-align: middle;',
            '}',
            '.loading-icon {',
                'display: none;',
                'font-size: 60px;',
                'margin-right: 10px;',
                'line-height: 50px;',
                'text-align: right;',
                'height: 50px;',
                'vertical-align: middle;',
            '}',
            '.pull-loading {',
                'display: inline-block;',
                '-webkit-animation: loadingrotate .85s linear infinite;',
            '}',
            '@-webkit-keyframes loadingrotate {',
                'to { -webkit-transform: rotate(360deg); }',
            '}'
        ].join('');
        style.id = 'pull-style';
        document.head.appendChild(style);
        document.removeEventListener('DOMContentLoaded', _dom_loaded, false);
    }
    document.addEventListener('DOMContentLoaded', _dom_loaded, false);
    function _tpl(config) {
        var tpl = [
            '<div id="{{id}}" class="pull">',
                '<div class="pull-text">',
                    '<span class="icon pull-icon pull-show">{{pullIcon}}</span>',
                    '<span class="icon loading-icon">{{loadingIcon}}</span>',
                '</div>',
            '</div>'
        ].join('');
        tpl = tpl.replace('{{id}}', config.id);
        tpl = tpl.replace('{{pullIcon}}', config.pullIcon);
        tpl = tpl.replace('{{loadingIcon}}', config.loadingIcon);
        return tpl;
    }
    function _onshow(self) {
        if (!self.eventAdded) {
            var parent = self.el.parentNode;
            parent.addEventListener('touchend', self.etouchend, false);
            parent.addEventListener('touchstart', self.etouchstart, false);
            parent.addEventListener('touchmove', self.etouchmove, false);
            self.eventAdded = true;
        }
    }
    function _onhide(self) {
        if (self.eventAdded) {
            var parent = self.el.parentNode;
            parent.removeEventListener('touchend', self.etouchend, false);
            parent.removeEventListener('touchstart', self.etouchstart, false);
            parent.removeEventListener('touchmove', self.etouchmove, false);
            self.eventAdded = false;
        }
    }
    function _afterrender(self) {
        var win = window,
            initialStart = null,
            previousY = null;
        self.el.parentNode.style['-webkit-transform'] = 'translate(0, -' +
            self.height + 'px) translateZ(0)';
        
        
        
        self.iconEl = self.el.querySelector('.pull-icon');
        self.loadingIconEl = self.el.querySelector('.loading-icon');
        self.currentState = 0;
        self.defaultColor = self.iconEl.style.color;
        self.etouchend = function() {
            if (self.isLoading === true) {
                return;
            }
            self.iconEl.style.color = self.defaultColor;
            self.setLoading(self.currentState >= 1 ? true : false);
        }.bind(self);
        self.etouchstart = function(e) {
            previousY = e.touches[0].screenY;
            initialStart = previousY;
            self.currentState = 0;
        }.bind(self);
        self.etouchmove = function(e) {
            var y = e.touches[0].screenY;
            if (self.isLoading === true) {
                return;
            }
            if (win.scrollY < 10) {
                var pos = Math.abs(initialStart - y);
                if (y < initialStart) {
                    pos = 0;
                } else if (win.scrollY < 5) {
                    
                    e.preventDefault();
                    e.stopPropagation();
                }
                pos = pos * self.sensibility;
                var state = (pos - self.offset) / self.height,
                    deg = (180 / self.height) * (pos - self.offset);
                
                
                
                if (state > 1) {
                    state = 1;
                    deg = 180;
                } else if (state < 0) {
                    state = 0;
                    deg = 0;
                }
                if (self.currentState < 1 && state >= 1) {
                    
                    self.iconEl.style.color = self.releaseColor;
                } else if (self.currentState >= 1 && state < 1) {
                    
                    self.iconEl.style.color = self.defaultColor;
                }
                self.currentState = state;
                self.iconEl.style['-webkit-transform'] = 'rotate(' + deg + 'deg)';
                self.el.parentNode.style['-webkit-transform'] = 'translate(0, ' + (-self.height + pos) + 'px) translateZ(0)';
            }
        }.bind(self);
    }
    function _aftercompile(self, renderer) {
        self.off(self.eid);
        renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
        renderer.on('show', _onshow,
            self, self.priority.VIEWS);
        renderer.on('hide', _onhide,
            self, self.priority.VIEWS);
    }
    return _parent.subclass({
        xtype: 'pull',
        className: 'Fs.views.Pull',
        constructor: function(opts) {
            var self = this;
            opts.template = _tpl;
            opts.config = opts.config || {};
            if (typeof opts.config.loadingIcon === 'undefined') {
                opts.config.loadingIcon = '&#128260;';
            }
            if (typeof opts.config.pullIcon === 'undefined') {
                opts.config.pullIcon = '&#59224;';
            }
            self.height = opts.config.height || 50;
            self.sensibility = opts.config.sensibility || 0.75;
            self.offset = opts.config.offset || 30;
            self.releaseColor = opts.config.releaseColor || '#ddd';
            self.loadingIcon = opts.config.loadingIcon;
            self.pullIcon = opts.config.pullIcon;
            _parent.prototype.constructor.call(self, opts);
            self.setupListeners(['loading']);
            self.eid = self.on('aftercompile', _aftercompile,
                    self, self.priority.VIEWS);
        },
        setLoading: function(loading) {
            var self = this,
                selector = _fs.Selector;
            self.isLoading = (loading === true ? true : false);
            if (self.isLoading === true) {
                self.el.parentNode.style['-webkit-transform'] = 'translate(0, 0) translateZ(0)';
                selector.removeClass(self.iconEl, 'pull-show');
                selector.addClass(self.loadingIconEl, 'pull-loading');
                self.fire('loading');
            } else {
                selector.addClass(self.iconEl, 'pull-show');
                selector.removeClass(self.loadingIconEl, 'pull-loading');
                self.currentState = 0;
                self.el.parentNode.style['-webkit-transform'] = 'translate(0, -' +
                    self.height + 'px) translateZ(0)';
                self.iconEl.style['-webkit-transform'] = 'rotate(0deg)';
            }
        }
    });
})();
G_APP_NAME = "意得捷优APP";
G_COMPANY_NAME = "北京意得捷优科技有限公司";
G_APP_VERSION = "1.3.0";
Fs.views.ImageZoom = (function() {
    var _fs = Fs,
        _parent = _fs.views.Template;
    var panning = false,
        zooming = false,
        startX0,
        startY0,
        startX1,
        startY1,
        endX0,
        endY0,
        endX1,
        endY1,
        startDistanceBetweenFingers,
        endDistanceBetweenFingers,
        pinchRatio,
        imgWidth, 
        imgHeight, 
        currentContinuousZoom = 1.0,
        currentOffsetX = -100,
        currentOffsetY = -100,
        currentWidth = imgWidth,
        currentHeight = imgHeight,
        newContinuousZoom,
        newHeight,
        newWidth,
        newOffsetX,
        newOffsetY,
        centerPointStartX,
        centerPointStartY,
        centerPointEndX,
        centerPointEndY,
        translateFromZoomingX,
        translateFromZoomingY,
        translateFromTranslatingX,
        translateFromTranslatingY,
        translateTotalX,
        translateTotalY,
        percentageOfImageAtPinchPointX,
        percentageOfImageAtPinchPointY,
        theImage;
    document.addEventListener('DOMContentLoaded', function() {
        var style = document.createElement('style');
        style.innerHTML = [
            'body.hasoverlay { background: #000 !important; overflow: hidden !important; }',
            '.imagezoom-overlay {',
                'z-index: 20000;',
                'width: 100%;',
                'height: 100%;',
                'position: fixed;',
                'top: 0px;',
                'left: 0px;',
                'background: #000;',
                'overflow: hidden;',
            '}',
            '.imagezoom-overlay .imagezoom-image {',
                'z-index: 20001;',
                'position: absolute;',
                
                
            '}'
        ].join('');
        style.id = 'imagezoom-style';
        document.head.appendChild(style);
        document.removeEventListener('DOMContentLoaded', arguments.callee, false);
    }, false);
    var _tpl = function(data) {
        var str = [
            '<div id="{id}" class="imagezoom-overlay{hidden}">',
                '<img class="imagezoom-image">',
            '</div>'
        ].join('');
        if (data.hidden === true) {
            str = str.replace('{hidden}', ' hidden');
        } else {
            str = str.replace('{hidden}', '');
        }
        return str.replace('{id}', data.id);
    };
    var _touchstart = function(e) {
        panning = false;
        zooming = false;
        if (e.touches.length == 1) {
            panning = true;
            startX0 = e.touches[0].pageX;
            startY0 = e.touches[0].pageY;
        }
        if (e.touches.length == 2) {
            zooming = true;
            startX0 = e.touches[0].pageX;
            startY0 = e.touches[0].pageY;
            startX1 = e.touches[1].pageX;
            startY1 = e.touches[1].pageY;
            centerPointStartX = ((startX0 + startX1) / 2.0);
            centerPointStartY = ((startY0 + startY1) / 2.0);
            percentageOfImageAtPinchPointX = (centerPointStartX - currentOffsetX) / currentWidth;
            percentageOfImageAtPinchPointY = (centerPointStartY - currentOffsetY) / currentHeight;
            startDistanceBetweenFingers = Math.sqrt(Math.pow((startX1 - startX0), 2) + Math.pow((startY1 - startY0), 2));
        }
    };
    var _touchmove = function(e) {
        var self = this;
        
        e.preventDefault();
        if (panning) {
            endX0 = e.touches[0].pageX;
            endY0 = e.touches[0].pageY;
            translateFromTranslatingX = endX0 - startX0;
            translateFromTranslatingY = endY0 - startY0;
            newOffsetX = currentOffsetX + translateFromTranslatingX;
            newOffsetY = currentOffsetY + translateFromTranslatingY;
            this.imgEl.style.left = newOffsetX + "px";
            this.imgEl.style.top = newOffsetY + "px";
        } else if (zooming) {
            
            endX0 = e.touches[0].pageX;
            endY0 = e.touches[0].pageY;
            endX1 = e.touches[1].pageX;
            endY1 = e.touches[1].pageY;
            
            endDistanceBetweenFingers = Math.sqrt( Math.pow((endX1-endX0),2) + Math.pow((endY1-endY0),2) );
            pinchRatio = endDistanceBetweenFingers / startDistanceBetweenFingers;
            newContinuousZoom = pinchRatio * currentContinuousZoom;
            newWidth = imgWidth * newContinuousZoom;
            newHeight  = imgHeight * newContinuousZoom;
            // Get the point between the two touches, relative to upper-left corner of image
            centerPointEndX = ((endX0 + endX1) / 2.0);
            centerPointEndY = ((endY0 + endY1) / 2.0);
            // This is the translation due to pinch-zooming
            translateFromZoomingX = (currentWidth - newWidth) * percentageOfImageAtPinchPointX;
            translateFromZoomingY = (currentHeight - newHeight) * percentageOfImageAtPinchPointY;
            // And this is the translation due to translation of the centerpoint between the two fingers
            translateFromTranslatingX = centerPointEndX - centerPointStartX;
            translateFromTranslatingY = centerPointEndY - centerPointStartY;
            // Total translation is from two components: (1) changing height and width from zooming and (2) from the two fingers translating in unity
            translateTotalX = translateFromZoomingX + translateFromTranslatingX;
            translateTotalY = translateFromZoomingY + translateFromTranslatingY;
            // the new offset is the old/current one plus the total translation component
            newOffsetX = currentOffsetX + translateTotalX;
            newOffsetY = currentOffsetY + translateTotalY;
            
            this.imgEl.style.left = newOffsetX + "px";
            this.imgEl.style.top = newOffsetY + "px";
            this.imgEl.width = newWidth;
            this.imgEl.height = newHeight;
        }
        
    };
    var _touchend = function(e) {
        if (panning) {
            panning = false;
            currentOffsetX = newOffsetX;
            currentOffsetY = newOffsetY;
        } else if (zooming) {
            zooming = false;
            currentOffsetX = newOffsetX;
            currentOffsetY = newOffsetY;
            currentWidth = newWidth;
            currentHeight = newHeight;
            currentContinuousZoom = newContinuousZoom;
        }
    };
    var _touchcancel = function(e) {
        if (this.panning) {
            this.panning = false;
        } else if (this.zooming) {
            this.zooming = false;
        }
    };
    
    var _transform = function() {
        var transform;
        if (window.navigator.userAgent.match(/Android 2.*/i) !== null) {
            transform = [
                'scale(', this.scale, ', ', this.scale, ') ',
                'translate(', this.posX, 'px,', this.posY, 'px) '
            ].join('');
        } else {
            transform = [
                'scale3d(', this.scale, ', ', this.scale, ', 1) ',
                'translate3d(', this.posX, 'px,', this.posY, 'px, 0) '
            ].join('');
        }
        this.imgEl.style.transform = transform;
        this.imgEl.style.oTransform = transform;
        this.imgEl.style.msTransform = transform;
        this.imgEl.style.mozTransform = transform;
        this.imgEl.style.webkitTransform = transform;
    };
    var _onimgload = function() {
        var self = this;
        var screenWidth = window.innerWidth,
            screenHeight = window.innerHeight;
        imgWidth = self.imgEl.width; 
        imgHeight = self.imgEl.height; 
        currentContinuousZoom = 1.0;
        if (imgWidth >= screenWidth) {
            currentOffsetX = 0;
            currentWidth = screenWidth;
        } else {
            currentOffsetX = (screenWidth - imgWidth) / 2.0;
            currentWidth = imgWidth;
        }
        currentContinuousZoom = (currentWidth / imgWidth);
        currentHeight = imgHeight * currentContinuousZoom;
        if (currentHeight >= screenHeight) {
            currentOffsetY = 0;
        } else {
            currentOffsetY = (screenHeight - currentHeight) / 2.0;
        }
        // currentOffsetX = 0;//-imgWidth / 2.0;
        
        
        
        self.imgEl.height = currentHeight;
        self.imgEl.width = currentWidth;
        self.imgEl.style.left = currentOffsetX + "px";
        self.imgEl.style.top = currentOffsetY + "px";
    };
    var _onafterrender = function() {
        var self = this;
        this.clickEvent = new _fs.events.Click({
            autoAttach: true,
            disableScrolling: true,
            el: this.el,
            scope: this,
            handler: function() {
                setTimeout(function() { self.fire('hide'); }, 1);
            }
        });
        this.imgEl = this.el.querySelector('.imagezoom-image');
        this._onimgload = _onimgload.bind(this);
        this.imgEl.addEventListener('load', this._onimgload, false);
        if (this.config.image) {
            this.imgEl.src = this.config.image;
        }
        this._touchstart = _touchstart.bind(this);
        this._touchmove = _touchmove.bind(this);
        this._touchend = _touchend.bind(this);
        this._touchcancel = _touchcancel.bind(this);
        this._onhistoryback = _onhistoryback.bind(this);
    };
    var _onhistoryback = function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        window.removeEventListener('hashchange', arguments.callee, false);
        this.hide();
        _fs.History.navigate(this.currentUrl);
        return false;
    };
    var _onshow = function() {
        if (this.lastSrc) {
            this.imgEl.setAttribute('src', this.lastSrc);
        }
        _fs.Selector.removeClass(this.el, 'hidden');
        this.clickEvent.attach();
        _fs.Selector.addClass(document.querySelector('body'), 'hasoverlay');
        
        this.currentUrl = _fs.History.here();
        this.imgEl.addEventListener('touchstart', this._touchstart, false);
        this.imgEl.addEventListener('touchmove', this._touchmove, false);
        this.imgEl.addEventListener('touchend', this._touchend, false);
        this.imgEl.addEventListener('touchcancel', this._touchcancel, false);
        window.addEventListener('hashchange', this._onhistoryback, false);
    };
    var _onhide = function() {
        this.lastSrc = this.imgEl.getAttribute('src');
        this.imgEl.removeAttribute('src');
        _fs.Selector.addClass(this.el, 'hidden');
        this.clickEvent.detach();
        _fs.Selector.removeClass(document.querySelector('body'), 'hasoverlay');
        this.imgEl.removeEventListener('touchstart', this._touchstart, false);
        this.imgEl.removeEventListener('touchmove', this._touchmove, false);
        this.imgEl.removeEventListener('touchend', this._touchend, false);
        this.imgEl.removeEventListener('touchcancel', this._touchcancel, false);
        window.removeEventListener('hashchange', this._onhistoryback, false);
    };
    var _onaftercompile = function() {
        this.off(this.eid);
        this.renderer.on('afterrender', _onafterrender,
            this, this.priority.VIEWS);
        if (!this.seid) {
            this.seid = this.renderer.on('show', _onshow,
                this, this.priority.VIEWS);
            this.heid = this.renderer.on('hide', _onhide,
                this, this.priority.VIEWS);
        }
    };
    return _parent.subclass({
        constructor: function(opts) {
            opts.template = _tpl;
            this.minScale = this.defaultMinScale = opts.minScale || 1.0;
            this.maxScale = this.defaultMaxScale = opts.maxScale || 3.0;
            _parent.prototype.constructor.call(this, opts);
            this.eid = this.on('aftercompile', _onaftercompile,
                this, this.priority.VIEWS);
        },
        updateImage: function(image) {
            delete this.lastSrc;
            this.imgEl.removeAttribute('width');
            this.imgEl.removeAttribute('height');
            this.imgEl.setAttribute('src', image);
            if (this.imgEl.complete && this.imgEl.naturalWidth !== 0) {
                this._onimgload();
            }
            return this;
        },
        
        show: function() {
            this.fire('show', this);
            return this;
        },
        
        hide: function() {
            this.fire('hide', this);
            return this;
        }
    });
})();
var App = {
    controllers: {},
    views: {},
    models: {},
    stores: {}
};
App.Settings = new Fs.Settings({
    serverUrl: 'http://' + window.location.host,
    
    updaterJson: '/updater.json',
    mapGet: '/pois.json',
    homeGet: '/company/get.json',
    contentSearch: '/contents.json',
    contentGet: '/contents/{id}.json',
    commentsGet: '/comments.json',
    categoryList: '/categories.json',
    userLogin: '/users/sign_in.json?user[email]={email}&user[password]={password}&user[remember_me]=true&_method=POST',
    userLogout: '/users/sign_out.json?_method=DELETE',
    userRegister: '/users.json?user[name]={name}&user[email]={email}&user[password]={password}&user[password_confirmation]={password_confirmation}&_method=POST',
    userProfileUpdate: '/users.json?user[name]={name}&user[company]={company}&user[email]={email}&user[phone]={phone}&user[role]={role}&user[current_password]={current_password}&_method=PUT',
    userFavorites: '/users/favorites.json',
    addFavorite: '/users/add_favorite.json?user[content_id]={id}',
    deleteFavorite: '/users/delete_favorite.json?user[content_id]={id}',
    subscribeEvent: '/contents/{id}/subscribe.json',
    unsubscribeEvent: '/contents/{id}/unsubscribe.json'
});
if (window.currentUser) {
    App.Settings.set('currentUser', window.currentUser);
}
Fs.Storage.setPrefix('moa-');
App.conf = {
    contact: {
        enabled: false,
        router: {
            routes: {
                '/more/contact': ['contact', 'more']
            }
        }
        
    },
    home: {
        enabled: true,
        router: {
            routes: {
                '/home': ['home', 'home']
            }
        },
        footer: {
            id: 'menu-home-btn',
            icon: '&#128214;',
            route: '/home',
            text: '主页'
        },
        templates: [
            'homedetail'
        ]
    },
    map: {
        enabled: true,
        router: {
            routes: {
                '/map': ['map', 'map']
            }
        },
        footer: {
            id: 'menu-map-btn',
            icon: '&#59172;',
            route: '/map',
            text: '地图'
        }
    },
    about: {
        enabled: true,
        router: {
            routes: {
                '/more/about': ['moreabout', 'more']
            }
        }
    },
    wechat: {
        enabled: true
    },
    favorites: {
        enabled: true,
        router: {
            routes: {
                '/user/favorites': ['userfavorites', 'favorites']
            },
            before: {
                '^userfavorites$': 'isAuthenticated'
            }
        },
        footer: {
            id: 'menu-favorites-btn',
            icon: '&#9733;',
            route: '/user/favorites',
            text: t('收藏')
        },
        templates: [
            'userfavoriteslist'
        ]
    },
    user: {
        enabled: true,
        router: {
            routes: {
                '/user/profile': ['userprofile', 'more'],
                '/user/login(/?back=:back)': ['userlogin', 'more'],
                '/user/register(/?back=:back)': ['userregister', 'more']
            },
            before: {
                '^userprofile$': 'isAuthenticated',
                '^user(login|register)$': 'isAlreadyAuthenticated'
            }
        }
    },
    enabled: function(name) {
        return (typeof this[name] === 'object' && this[name].enabled === true);
    }
};
Fs.fx = new (function() {
    var _fs = Fs,
        _win = window,
        _doc = document,
        _selector = _fs.Selector,
        _parent = _fs.Event,
        _settings = App.Settings,
        _isAnimating = false,
        _footer;
    var _isFailover = !('box-shadow' in _doc.body.style),
        link = _doc.createElement('link');
    link.type = 'text/css';
    link.media = 'screen';
    link.rel = 'stylesheet';
    
    if (_isFailover === true) {
        link.href = _settings.get('assets', '/assets') + '/animation-failover.min.css?' + _fs.version;
    } else {
        link.href = _settings.get('assets', '/assets') + '/animation.min.css?' + _fs.version;
    }
    link.id = 'fs-animation';
    _doc.head.appendChild(link);
    
    var _redraw = function(el) {
        _selector.addClass(el, 'repaint');
        var h = el.offsetHeight;
        _selector.removeClass(el, 'repaint');
    };
    
    
    var _effect = function(name, from, to, opts) {
        _isAnimating = true;
        opts = opts || {
            reverse: false,
            scope: window
        };
        if (typeof opts.directions !== 'object') {
            opts.directions = {
                from: 'in',
                to: 'out'
            };
        }
        if (!from && !to) {
            if (opts.callback) {
                opts.callback.call(opts.scope, false);
            }
            return false;
        }
        if (_isFailover === true) {
            if (opts.beforestart) {
                opts.beforestart.call(opts.scope);
            }
            if (opts.afterstart) {
                opts.afterstart.call(opts.scope);
            }
            setTimeout(function() {
                if (from) {
                    _selector.addClass(from, 'hidden');
                }
                if (to) {
                    _selector.removeClass(to, 'hidden');
                }
                if (opts.beforeend) {
                    opts.beforeend.call(opts.scope);
                }
                _isAnimating = false;
                if (opts.callback) {
                    opts.callback.call(opts.scope, true);
                }
            }, 1);
            return;
        }
        if (to) {
            to.addEventListener('webkitAnimationEnd', afteranim, false);
        } else {
            from.addEventListener('webkitAnimationEnd', afteranim, false);
        }
        if (to) {
            _selector.removeClass(to, 'hidden');
            _selector.addClasses(to, 'positioning container-animating ' +
                name + ' in' + (opts.reverse === true ? ' reverse' : ''));
            
        }
        if (from) {
            _selector.addClasses(from, 'positioning container-animating ' +
                name + ' out' + (opts.reverse === true ? ' reverse' : ''));
            
        }
        if (opts.beforestart) {
            opts.beforestart.call(opts.scope);
        }
        _selector.addClass(document.body, 'ui-mobile-viewport-transitioning');
        if (opts.afterstart) {
            opts.afterstart.call(opts.scope);
        }
        function afteranim(e) {
            if (from) {
                _selector.addClass(from, 'hidden');
                _selector.removeClasses(from, (name +
                    ' container-animating out positioning'));
                if (opts.reverse === true) {
                    _selector.removeClass(from, 'reverse');
                }
            }
            if (to) {
                _selector.removeClasses(to, (name +
                    ' in positioning container-animating'));
                if (opts.reverse === true) {
                    _selector.removeClass(to, 'reverse');
                }
            }
            if (e) {
                e.target.removeEventListener('webkitAnimationEnd', arguments.callee);
            }
            if (opts.beforeend) {
                opts.beforeend.call(opts.scope);
            }
            _selector.removeClass(document.body, 'ui-mobile-viewport-transitioning');
            _isAnimating = false;
            if (opts.callback) {
                opts.callback.call(opts.scope, true);
            }
        }
        
    };
    return _parent.subclass({
        constructor: function() {
            _parent.prototype.constructor.apply(this, arguments);
        },
        isAnimating: function() {
            return _isAnimating;
        },
        slide: function(from, to, opts) {
            _effect.call(this, 'slide', from, to, opts);
        },
        upslide: function(from, to, opts) {
            _effect.call(this, 'upslide', from, to, opts);
        },
        paraslide: function(from, to, opts) {
            _effect.call(this, 'paraslide', from, to, opts);
        },
        fade: function(from, to, opts) {
            _effect.call(this, 'fade', from, to, opts);
        },
        pop: function(from, to, opts) {
            _effect.call(this, 'pop', from, to, opts);
        },
        turn: function(from, to, opts) {
            _selector.addClass(document.body, 'viewport-turn');
            var cb = opts.callback;
            opts.callback = function(success) {
                _selector.removeClass(document.body, 'viewport-turn');
                if (cb) {
                    cb(success);
                }
            };
            _effect.call(this, 'turn', from, to, opts);
        },
        flip: function(from, to, opts) {
            _selector.addClass(document.body, 'viewport-flip');
            var cb = opts.callback;
            opts.callback = function(success) {
                _selector.removeClass(document.body, 'viewport-flip');
                if (cb) {
                    cb(success);
                }
            };
            _effect.call(this, 'flip', from, to, opts);
        }
    });
}())();
App.errors = (function() {
    var _id = 'error-alert',
        _alert,
        _titleEl,
        _contentEl,
        _callback,
        _fs = Fs,
        _history = _fs.History;
    var _getAlert = function() {
        if (!_alert) {
            _alert = new _fs.views.Alert({
                config: {
                    id: _id,
                    overlayHide: false,
                    ui: 'd',
                    header: {
                        title: 'Error',
                        ui: 'd'
                    },
                    style: 'position: fixed; top: 50%; left: 50%; margin-left: -140px; width: 280px; margin-top: -140px;'
                },
                listeners: {
                    scope: this,
                    show: function() {
                        _fs.Selector.addClass(document.getElementById('mainpage'), 'blur');
                    },
                    hide: function() {
                        _fs.Selector.removeClass(document.getElementById('mainpage'), 'blur');
                    }
                },
                items: [
                    '<p id="' + _id + '-content"></p>', {
                    xtype: 'button',
                    config: {
                        text: 'OK',
                        ui: 'c',
                        inline: false,
                        style: 'margin: 0 auto;'
                    },
                    handler: function() {
                        if (_callback) {
                            _callback();
                        }
                        _alert.hide();
                    }
                }]
            });
            _alert.compile();
            _alert.render('body', 'afterbegin');
            _titleEl = _fs.Selector.get('.ui-title', document.getElementById(_id));
            _contentEl = document.getElementById(_id + '-content');
        }
        return _alert;
    };
    var _showAlert = function(title, msg, callback) {
        if (!_alert) {
            _getAlert();
        }
        _callback = callback;
        _fs.Selector.updateHtml(_titleEl, title);
        _fs.Selector.updateHtml(_contentEl, _formatMessage(msg));
        _getAlert().show();
        _alert.el.style.marginTop = '-' + (_alert.el.offsetHeight / 2) + 'px';
    };
    var _formatMessage = function(msg) {
        var type = typeof msg;
        if (type === 'string') {
            return msg;
        } else if (type === 'object' &&
            typeof msg.join === 'function') {
            return msg.join('\n');
        } else {
            var i, result = '';
            for (i in msg) {
                result += '<b>' + i + '</b>: ' + msg[i].join(' and ') + '<br>';
            }
            return result;
        }
    };
    var _defaultBehavior = function(requestSuccess, responseData) {
        if (requestSuccess) {
            var message = responseData.message;
            if (responseData.logout) {
                _showAlert(t('Error'),
                    t('You has been logged out, please sign in again.'), function() {
                    App.Settings.set('currentUser', null);
                    _history.navigate('/login?back=' + encodeURIComponent(_history.here()));
                });
            } else {
                _showAlert(t('Response error'), t(message ? message : 'Unknown error.'));
            }
        } else {
            _showAlert(t('Server error'),
                t('Unable to reach server.'));
        }
    };
    var _onStoreError = function(requestSuccess, store, responseData) {
        return _defaultBehavior(requestSuccess, responseData);
    };
    var _onJsonpError = function(requestSuccess, responseData) {
        return _defaultBehavior(requestSuccess, responseData);
    };
    return {
        onStoreError: _onStoreError,
        onJsonpError: _onJsonpError,
        showAlert: _showAlert
    };
})();
/* success */
App.success = (function() {
    var _id = 'success-alert',
        _alert,
        _titleEl,
        _contentEl,
        _callback,
        _fs = Fs,
        _history = _fs.History;
    var _getAlert = function() {
        if (!_alert) {
            _alert = new _fs.views.Alert({
                config: {
                    id: _id,
                    overlayHide: false,
                    ui: 'd',
                    header: {
                        title: t('Success'),
                        ui: 'g'
                    },
                    style: 'position: fixed; top: 50%; left: 50%; margin-left: -140px; width: 280px; margin-top: -140px;'
                },
                listeners: {
                    scope: this,
                    show: function() {
                        _fs.Selector.addClass(document.getElementById('mainpage'), 'blur');
                    },
                    hide: function() {
                        _fs.Selector.removeClass(document.getElementById('mainpage'), 'blur');
                    }
                },
                items: [
                    '<p id="' + _id + '-content"></p>', {
                    xtype: 'button',
                    config: {
                        text: 'OK',
                        ui: 'c',
                        inline: false,
                        style: 'margin: 0 auto;'
                    },
                    handler: function() {
                        if (_callback) {
                            _callback();
                        }
                        _alert.hide();
                    }
                }]
            });
            _alert.compile();
            _alert.render('body', 'afterbegin');
            _titleEl = _fs.Selector.get('.ui-title', document.getElementById(_id));
            _contentEl = document.getElementById(_id + '-content');
        }
        return _alert;
    };
    var _showAlert = function(title, msg, callback) {
        if (!_alert) {
            _getAlert();
        }
        _callback = callback;
        _fs.Selector.updateHtml(_titleEl, title);
        _fs.Selector.updateHtml(_contentEl, _formatMessage(msg));
        _getAlert().show();
        _alert.el.style.marginTop = '-' + (_alert.el.offsetHeight / 2) + 'px';
    };
    var _formatMessage = function(msg) {
        var type = typeof msg;
        if (type === 'string') {
            return msg;
        } else if (type === 'object' &&
            typeof msg.join === 'function') {
            return msg.join('\n');
        } else {
            var i, result = '';
            for (i in msg) {
                result += '<b>' + i + '</b>: ' + msg[i].join(' and ') + '<br>';
            }
            return result;
        }
    };
    var _defaultBehavior = function(requestSuccess, responseData) {
        if (requestSuccess) {
            var message = responseData.message;
            _showAlert(t('Success'), t(message ? message : t('Operation done successfully.')));
        }
    };
    var _onStoreSuccess = function(requestSuccess, store, responseData) {
        return _defaultBehavior(requestSuccess, responseData);
    };
    var _onJsonpSuccess = function(requestSuccess, responseData) {
        return _defaultBehavior(requestSuccess, responseData);
    };
    return {
        onStoreSuccess: _onStoreSuccess,
        onJsonpSuccess: _onJsonpSuccess,
        showAlert: _showAlert
    };
})();
/* confirm */
App.confirm = (function() {
    var _id = 'confirm-alert',
        _alert,
        _titleEl,
        _contentEl,
        _callback,
        _fs = Fs,
        _history = _fs.History;
    var _getAlert = function() {
        if (!_alert) {
            _alert = new _fs.views.Alert({
                config: {
                    id: _id,
                    overlayHide: false,
                    ui: 'd',
                    header: {
                        title: t('Confirm'),
                        ui: 'd'
                    },
                    style: 'position: fixed; top: 50%; left: 50%; margin-left: -140px; width: 280px; margin-top: -140px;'
                },
                listeners: {
                    scope: this,
                    show: function() {
                        _fs.Selector.addClass(document.getElementById('mainpage'), 'blur');
                    },
                    hide: function() {
                        _fs.Selector.removeClass(document.getElementById('mainpage'), 'blur');
                    }
                },
                items: [
                    '<p id="' + _id + '-content"></p><div class="ui-grid-a"><div class="ui-block-a">', {
                    xtype: 'button',
                    config: {
                        text: t('Yes'),
                        ui: 'd',
                        inline: false
                    },
                    handler: function() {
                        if (_callback) {
                            _callback(true);
                        }
                        _alert.hide();
                    }
                }, '</div><div class="ui-block-b">', {
                    xtype: 'button',
                    config: {
                        text: t('No'),
                        ui: 'c',
                        inline: false
                    },
                    handler: function() {
                        if (_callback) {
                            _callback(false);
                        }
                        _alert.hide();
                    }
                }, '</div></div>']
            });
            _alert.compile();
            _alert.render('body', 'afterbegin');
            _titleEl = _fs.Selector.get('.ui-title', document.getElementById(_id));
            _contentEl = document.getElementById(_id + '-content');
        }
        return _alert;
    };
    var _show = function(title, msg, callback) {
        if (!_alert) {
            _getAlert();
        }
        _callback = callback;
        _fs.Selector.updateHtml(_titleEl, title);
        _fs.Selector.updateHtml(_contentEl, _formatmsg(msg));
        _getAlert().show();
        _alert.el.style.marginTop = '-' + (_alert.el.offsetHeight / 2) + 'px';
    };
    var _formatmsg = function(msg) {
        var type = typeof msg;
        if (type === 'string') {
            return msg;
        } else if (type === 'object' &&
            typeof msg.join === 'function') {
            return msg.join('\n');
        } else {
            var i, result = '';
            for (i in msg) {
                result += '<b>' + i + '</b>: ' + msg[i].join(' and ') + '<br>';
            }
            return result;
        }
    };
    return {
        show: _show
    };
})();
App.helpers = (function() {
    var _fs = Fs,
        _loadmaskHidden = true,
        _loadmask;
    var _getLoadmask = function() {
        if (!_loadmask) {
            _loadmask = new _fs.views.LoadMask({
                config: {
                    fullscreen: true
                }
            });
            _loadmask.compile();
            _loadmask.render('body', 'afterbegin');
        }
        return _loadmask;
    };
    var _showLoading = function() {
        _loadmaskHidden = false;
        _getLoadmask().show();
    };
    var _hideLoading = function() {
        _loadmaskHidden = true;
        _getLoadmask().hide();
    };
    var _isLoading = function() {
        return !_loadmaskHidden;
    };
    var _setDisabledAll = function(value, fields, obj) {
        var i = fields.length;
        while (i--) {
            obj[fields[i]].setDisabled(value);
        }
    };
    
    var _fieldsToJson = function(selector, root) {
        var field,
            result = {},
            fields = _fs.Selector.gets(selector, root),
            i = fields.length;
        while (i--) {
            field = fields[i];
            if (field.getAttribute('required') !== null &&
                !field.value.length) {
                return false;
            } else if (field.validity.valid === false) {
                return false;
            }
            result[field.getAttribute('name')] = field.value;
        }
        return result;
    };
    var _validateFields = function(selector, root) {
        var field,
            result = true,
            fields = _fs.Selector.gets(selector, root),
            i = fields.length;
        while (i--) {
            field = fields[i];
            if (field.getAttribute('required') !== null &&
                !field.value.length) {
                _fs.Selector.addClass(field.parentNode, 'fielderror');
                result = false;
            } else if (field.validity.valid === false) {
                _fs.Selector.addClass(field.parentNode, 'fielderror');
                result = false;
            }
        }
        return result;
    };
    var _jsonToFields = function(json, fields) {
        if (json) {
            var field, name, i = fields.length;
            while (i--) {
                field = fields[i];
                name = field.getAttribute('name');
                if (typeof json[name] !== 'undefined') {
                    field.value = json[name];
                } else {
                    field.value = '';
                }
            }
        }
    };
    var _replaceByValues = function(str, fields, encode) {
        var i;
        for (i in fields) {
            var value = fields[i];
            if (typeof encode === 'function') {
                value = encode(value);
            }
            str = str.replace('{' + i + '}', value);
        }
        return str;
    };
    
    return {
        fieldsToJson: _fieldsToJson,
        jsonToFields: _jsonToFields,
        validateFields: _validateFields,
        replaceByValues: _replaceByValues,
        setDisabledAll: _setDisabledAll,
        showLoading: _showLoading,
        hideLoading: _hideLoading,
        isLoading: _isLoading,
        lang: new _fs.helpers.i18n()
    };
})();
App.helpers.lang.addLang('cn', {
  'About':'关于',
  'About us':'关于我们',
  'Account':'账号',
  'Company':'公司',
  'Confirm password':'确认密码',
  'Current password':'现在的密码',
  'Email':'电子邮箱',
  'Event date':'日期',
  'Favorites':'收藏',
  'Full':'满',
  'Home':'主页',
  'Loading':'载入中',
  'Logged as':'登录为',
  'Login':'登录',
  'Logout':'退出',
  'Map':'地图',
  'More':'更多',
  'My profile':'我的信息',
  'Name':'姓名',
  'News':'新闻',
  'No favorites.':'没有收藏。',
  'No news.':'没有新闻',
  'Not opened yet':'未开发活动',
  'Password':'密码',
  'Phone':'电话号码',
  'Profile':'我的信息',
  'Register':'注册',
  'Response error':'信息错误',
  'Unknown error.':'服务器错误，代码 002',
  'Role':'职位',
  'Show all':'所有内容',
  'Sign in':'登录',
  'Subscribe to event':'报名',
  'Unable to reach server.':'服务器错误，代码 003',
  'Unsubscribe to event':'取消报名',
  'Update':'更新',
  'You has been logged out, please sign in again.':'请重新登录。',
  'You need to login to use favorites.': '请先登录再使用此功能。',
  'You need to login for add a favorite.': '请先登录再使用此功能。',
  'You need to login for register to an event.': '请先登录再使用此功能。',
  'Start':'开始',
  'End':'介绍',
  'Total seats':'一共位置',
  'Available seats':'剩下位置',
  'Address' : '地址',
  'or' : '或者',
  'Success': '成功',
  'You have successfully subscribed to this events.': '您已经成功报名这次活动。',
  'Operation done successfully.': '操作成功。',
  'Unsubscribe': '取消报名',
  'Are you sure you want to unsubscribe from this event ?': '是否确定取消这次的活动报名。',
  'Yes': '是',
  'No': '否',
  'Add to Favorites': '添加到最爱',
  'Remove from Favorites': '从最爱中移除',
  'Wechat Moment': '微信分享时刻',
  'Wechat Message': '微信短信',
  'Social': '社交',
  'Wechat error': '微信错误'
});
App.helpers.lang.setLang('cn');
App.views.Layout = (function() {
    var _tabs,
        _header,
        _footer,
        _fs = Fs,
        _iconPos = 'top',
        _footerId = 'page-footer',
        _showText = true,
        _selector = _fs.Selector,
        _parent = _fs.views.Page;
    var _createTabButtons = function() {
        var buttons = [], conf = App.conf;
        if (conf.enabled('home') === true) {
            buttons.push(_getTabButton('home'));
        }
        buttons.push({
            xtype: 'tabbutton',
            config: {
                id: 'menu-news-btn',
                icon: '&#9776;',
                route: '/news/search',
                text: (_showText ? '新闻' : undefined),
                iconPos: _iconPos
            }
        });
        if (conf.enabled('favorites') === true) {
            buttons.push(_getTabButton('favorites'));
        }
        if (conf.enabled('map') === true) {
            buttons.push(_getTabButton('map'));
        }
        if (conf.enabled('user') === true ||
            conf.enabled('about') === true) {
            buttons.push({
                xtype: 'tabbutton',
                config: {
                    id: 'menu-more-btn',
                    icon: '&#59170;',
                    route: '/more/menu',
                    text: (_showText ? '我的信息' : undefined),
                    iconPos: _iconPos
                }
            });
        }
        return buttons;
    };
    var _onafterrender = function() {
        var footerEl = document.getElementById(_footerId);
        document.addEventListener('focusin', function() {
            
            _selector.addClass(footerEl, 'hidden');
        }, false);
        document.addEventListener('focusout', function() {
            
            _selector.removeClass(footerEl, 'hidden');
        }, false);
    };
    var _getTabButton = function(name) {
        var conf = App.conf;
        var btn = {
            xtype: 'tabbutton',
            config: conf[name].footer
        };
        if (_showText === false) {
            delete btn.config.text;
        }
        btn.config.iconPos = _iconPos;
        return btn;
    };
    _tabs = {
        xtype: 'tabs',
        config: {
            id: 'main-menu',
            
            mini: false
        },
        items: _createTabButtons()
    };
    _footer = {
        xtype: 'footer',
        config: {
            ui: 'neutral',
            id: _footerId,
            position: 'fixed',
            cssClass: 'footer-transitioning'
        },
        items: _tabs
    };
    return _parent.subclass({
        constructor: function(opts) {
            opts = _fs.utils.applyIfAuto(opts || {}, {
                items: _footer
            });
            _parent.prototype.constructor.call(this, opts);
            this.on('afterrender', _onafterrender, this);
        }
    });
}());
App.views.Map = (function() {
    var _data,
        _self,
        _win = window,
        _doc = _win.document,
        _markers = [],
        _dataReady = false,
        _mapReady = false,
        _toolbarsHeight = 0,
        _fs = Fs,
        _settings = App.Settings,
        _url = _settings.gets('serverUrl', 'mapGet').join(''),
        _request = _fs.Request,
        _parent = _fs.views.Container;
    _win.baiduinit = function() {
        _mapReady = true;
        if (_dataReady === true) {
            _drawMap();
        }
    };
    _doc.addEventListener('DOMContentLoaded', function() {
        var style = _doc.createElement('style');
        style.innerHTML = [
            '.BMap_pop, .BMap_shadow {',
                'margin-top: -26px !important;',
            '}'
        ].join('');
        style.id = 'baidumap-infowin-fix';
        _doc.head.appendChild(style);
        _doc.removeEventListener('DOMContentLoaded', arguments.callee, false);
    }, false);
    var _drawMap = function() {
        var item,
            i = -1,
            bm = BMap,
            center = _data.center,
            data = _data.data,
            length = _data.total,
            map = new bm.Map('poimap');
        map.centerAndZoom(new bm.Point(center.longitude, center.latitude), 12);
        function showWindow(marker, item) {
            return function() {
                var html = '',
                    opts = {
                    minWidth: 200,
                    enableMessage: false,
                    title : item.name
                };
                if (typeof item.image === 'string' && item.image.length) {
                    html = [
                        html,
                        '<div style="margin: 5px auto; width: 95%; height: 130px; ',
                        'background-image: url(', _settings.get('serverUrl'), '/', item.image,
                        '); background-size: cover; background-position: center center; background-repeat: no-repeat;">&nbsp;</div>'
                    ].join('');
                }
                html = [html, '<p>', item.description, '</p>'].join('');
                var infoWindow = new bm.InfoWindow(html, opts);
                map.openInfoWindow(infoWindow, this.getPosition());
            };
        }
        while (++i < length) {
            item = data[i];
            _markers.push(new bm.Marker(new bm.Point(item.longitude, item.latitude)));
            _markers[i].addEventListener('click', showWindow(_markers[i], item));
            map.addOverlay(_markers[i]);
        }
    };
    var _onafterrender = function() {
        _toolbarsHeight = this.el.querySelector('.ui-header').offsetHeight +
            document.getElementById('page-footer').offsetHeight;
        _resizeMap();
        _request.jsonp(_url + '?callback={callback}', {
            scope: this,
            callback: function(success, data) {
                if (success === true &&
                    typeof data === 'object' &&
                    data.success === true) {
                    _data = data;
                    _dataReady = true;
                    if (_mapReady === true) {
                        _drawMap();
                    }
                } else {
                    App.errors.onJsonpError(success, data);
                }
            }
        });
    };
    var _resizeMap = function() {
        var height = window.innerHeight - _toolbarsHeight - 40 - 100;
        document.getElementById('poimap-viewport').style.height = height + 'px';
    };
    var _onshow = function() {
        if (!this.eventResize) {
            this.eventResize = new _fs.events.Resize();
        }
        this.eventResize.attach(window, _resizeMap, this);
        _resizeMap();
    };
    var _onhide = function() {
        if (this.eventResize) {
            this.eventResize.detach();
        }
    };
    return _parent.subclass({
        constructor: function(opts) {
            _self = this;
            opts.config.style = 'padding-bottom: 52px;';
            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        position: 'fixed',
                        title: t('Map')
                    }
                },
                '<div id="poimap-viewport" style="width: 100%; height: 300px;"><div id="poimap" style="position: absolute;width: 100%;height: 100%;overflow: hidden;margin:0; top: 50px; left: 0; padding: 0;"></div></div>'
                ]
            });
            this.on('afterrender', _onafterrender, this);
            this.on('show', _onshow, this);
            this.on('hide', _onhide, this);
        }
    });
}());
App.views.Contact = (function() {
    var _fs = Fs,
        _request = _fs.Request,
        _parent = _fs.views.Container;
    return _parent.subclass({
        constructor: function(opts) {
            opts.config.style = 'padding-bottom: 52px;';
            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Home')
                    }
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    
                    ref: 'content',
                    config: {
                        style: 'padding-top: 0px !important;'
                    },
                    items: [{
                        xtype: 'collapsible',
                        config: {
                            header: {
                                title: t('Map'),
                                ui: 'f'
                            },
                            iconPos: 'left',
                            size: 'small'
                        },
                        items: ['<p>PROUT</p>']
                    }, {
                        xtype: 'collapsible',
                        config: {
                            header: {
                                title: t('Direction'),
                                ui: 'f'
                            },
                            iconPos: 'left',
                            size: 'small'
                        },
                        items: ['<p>LOL</p>']
                    }]
                }]
            });
        }
    });
}());
App.views.Home = (function() {
    var _fs = Fs,
        _settings = App.Settings,
        _url = _settings.gets('serverUrl', 'homeGet').join(''),
        _request = _fs.Request,
        _parent = _fs.views.Container;
    var _onafterrender = function() {
        this.detailTpl.parentEl = this.detailTpl.el.parentNode;
        _request.jsonp(_url + '?callback={callback}', {
            scope: this,
            callback: function(success, data) {
                if (success === true &&
                    typeof data === 'object' &&
                    data.success === true) {
                    _updateLayout.call(this, data);
                } else {
                    App.errors.onJsonpError(success, data);
                }
            }
        });
    };
    var _updateLayout = function(data) {
        var self = this,
            i = -1,
            len = data.data.images.length,
            images = [],
            url = _settings.get('serverUrl');
        while (++i < len) {
            images.push('"' + url + '/' +
                data.data.images[i].url.replace('"', '\\"') + '"');
        }
        self.carousel = new _fs.views.Carousel({
            config: {
                style: 'border-bottom: 2px solid #ddd;',
                images: images,
                height: '250px',
                animation: 1000
            }
        });
        self.carousel.compile();
        self.carousel.render(self.content.el, 'beforebegin');
        
        self.on('show', function() {
            self.carousel.fireScope('show', self.renderer);
        });
        self.on('hide', function() {
            self.carousel.fireScope('hide', self.renderer);
        });
        self.carousel.fireScope('show', self.renderer);
        self.detailTpl.data = data;
        self.detailTpl.compile();
        self.detailTpl.render(self.content.el);
    };
    return _parent.subclass({
        constructor: function(opts) {
            this.detailTpl = new _fs.views.Template({
                template: _fs.Templates.get('homedetail')
            });
            opts.config.style = 'padding-bottom: 52px;';
            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Home'),
                        position: 'fixed'
                    }
                }, {
                    xtype: 'abstract',
                    xtpl: 'container',
                    items: {
                        xtype: 'abstract',
                        xtpl: 'content',
                        ref: 'content'
                    }
                }]
                
            });
            this.on('afterrender', _onafterrender, this);
        }
    });
}());
App.views.NewsSocial = (function() {
    var _record,
        _fs = Fs,
        _app = App,
        _helpers = _app.helpers,
        _parent = _fs.views.Popup,
        _settings = _app.Settings,
        _selector = _fs.Selector,
        _addFavUrl = _settings.gets('serverUrl', 'addFavorite').join(''),
        _delFavUrl = _settings.gets('serverUrl', 'deleteFavorite').join(''),
        _hasWechat = false,
        _appConf = _app.conf;
    function device_ready() {
        _hasWechat = (typeof window.Wechat === 'object' ? true : false);
        document.removeEventListener('deviceready', device_ready, false);
    }
    document.addEventListener('deviceready', device_ready, false);
    function afterrender(self) {
        
    }
    function record_update(self, record) {
        _record = record;
        if (_appConf.enabled('favorites')) {
            if (record.is_favorite === true) {
                _selector.addClass(self.addFavBtn.el, 'hidden');
                _selector.removeClass(self.delFavBtn.el, 'hidden');
            } else {
                _selector.removeClass(self.addFavBtn.el, 'hidden');
                _selector.addClass(self.delFavBtn.el, 'hidden');
            }
        }
    }
    var wechat_moment_share = function() {
        return wechat_share(this, 'TIMELINE');
    };
    var wechat_message_share = function() {
        return wechat_share(this, 'SESSION');
    };
    function wechat_share(self, shareType) {
        var wechat = window.Wechat,
            data = _record,
            synopsis = (data.synopsis || data.data.replace(/^\s+|\s+$/g, '').substr(0, 300).replace(/<[^>]+>/ig, '') + '...'),
            share = {
                title: data.title,
                thumb: _settings.get('serverUrl'),
                description: synopsis,
                media: {
                    type: wechat.Type.WEBPAGE,
                    webpageUrl: (_settings.get('serverUrl') +
                        '/?t=' + (new Date()).getTime() +
                        '#' + _fs.History.here())
                }
            };
        if (data.content_medias.length > 0) {
            share.thumb += data.content_medias[0].thumb;
        } else {
            share.thumb += '/assets/icons/Icon-72@2x.png';
        }
        wechat.share({
            message: share,
            scene: wechat.Scene[shareType]
        }, function () {
            self.hide();
        }, function (reason) {
            self.hide();
            _app.errors.showAlert(t('Wechat error'), reason);
        });
    }
    var _toggleFavorite = function() {
        if (_helpers.isLoading()) {
            return false;
        }
        var self = this,
            user = _settings.get('currentUser');
        if (typeof user !== 'object') {
            self.hide();
            _settings.set('flashlogin', t('You need to login for add a favorite.'));
            _fs.History.navigate('/user/login/?back=' + _fs.History.here(true));
            return false;
        }
        var isFav = _record.is_favorite,
            url = (isFav ? _delFavUrl : _addFavUrl);
        _helpers.showLoading();
        url = url.replace('{id}', _record.id) + '&callback={callback}';
        _fs.Request.jsonp(url, {
            scope: self,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _record.is_favorite = !isFav;
                    self.fire('favoriteUpdate', !isFav);
                    record_update(self, _record);
                } else {
                    _app.errors.onJsonpError(success, data);
                }
            }
        });
    };
    function show() {
        _selector.addClasses(document.querySelector('.ui-page-active'),
            'blur blur-header');
    }
    function hide() {
        _selector.removeClasses(document.querySelector('.ui-page-active'),
            'blur blur-header');
    }
    return _parent.subclass({
        isActive: function() {
            return (_appConf.enabled('favorites') ||
                (_hasWechat && _appConf.enabled('wechat')));
        },
        constructor: function(opts) {
            var self = this,
                items = [];
            if (_appConf.enabled('favorites') === true) {
                items.push({
                    xtype: 'button',
                    ref: 'addFavBtn',
                    config: {
                        ui: 'g',
                        icon: '&#9734;',
                        inline: false,
                        hidden: true,
                        text: t('Add to Favorites')
                    },
                    scope: self,
                    handler: _toggleFavorite
                });
                items.push({
                    xtype: 'button',
                    ref: 'delFavBtn',
                    config: {
                        ui: 'd',
                        icon: '&#9733;',
                        inline: false,
                        hidden: true,
                        text: t('Remove from Favorites')
                    },
                    scope: self,
                    handler: _toggleFavorite
                });
            }
            if (_hasWechat && _appConf.enabled('wechat') === true) {
                items.push({
                    xtype: 'button',
                    config: {
                        inline: false,
                        text: t('Wechat Moment'),
                        iconCssClass: 'wechat',
                        icon: '&#xE801;'
                    },
                    scope: self,
                    handler: wechat_moment_share
                });
                items.push({
                    xtype: 'button',
                    config: {
                        inline: false,
                        text: t('Wechat Message'),
                        iconCssClass: 'wechat',
                        icon: '&#xE800;'
                    },
                    scope: self,
                    handler: wechat_message_share
                });
            }
            opts = {
                config: {
                    
                    header: {
                        title: t('Social')
                    }
                },
                items: items,
                listeners: {
                    scope: self,
                    afterrender: afterrender
                }
            };
            _parent.prototype.constructor.call(self, opts);
            self.on('show', show, self);
            self.on('hide', hide, self);
            self.on('recordUpdate', record_update, self);
        }
    });
}());
App.views.Newsdetail = (function() {
    var _handlebars = Handlebars,
        _settings = App.Settings,
        _serverUrl = _settings.get('serverUrl');
    _handlebars.registerHelper('hasImage', function(context, options) {
        if (context && context.length) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    _handlebars.registerHelper('hasMapImage', function(context, options) {
        if (context.indexOf('missing.png') !== -1) {
            return options.inverse(this);
        }
        return options.fn(this);
    });
    _handlebars.registerHelper('getImageGridType', function(medias) {
        return 'ui-grid-b';
    });
    _handlebars.registerHelper('getImageRowType', function(medias, index) {
        var letters = ['a', 'b', 'c'];
        return ('ui-block-' + letters[index % 3]);
    });
    _handlebars.registerHelper('getImage', function(medias, index, size) {
        index = index || 0;
        if (typeof size !== 'string') {
            size = 'medium';
        }
        return '"' + _serverUrl + medias[index][size].replace('"', '\\"') + '"';
    });
    _handlebars.registerHelper('getFullImage', function(medias, index, size) {
        index = index || 0;
        if (typeof size !== 'string') {
            size = 'original';
        }
        return (_serverUrl + medias[index][size].replace('"', '\\"'));
    });
    _handlebars.registerHelper('getUrl', function(url) {
        return '"' + _serverUrl + url.replace('"', '\\"') + '"';
    });
    _handlebars.registerHelper('nl2br', function(str) {
        if (str.indexOf('<br>') === -1) {
            var breakTag = '<br />';
            str = (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }
        return new _handlebars.SafeString(str);
    });
    var _social,
        _mapTpl, _dateTpl,
        _id = 'newsdetail',
        _getUrl = _settings.gets('serverUrl', 'contentGet').join(''),
        _subscribeUrl = _settings.gets('serverUrl', 'subscribeEvent').join(''),
        _unsubscribeUrl = _settings.gets('serverUrl', 'unsubscribeEvent').join(''),
        _backUrl,
        _detailTpl,
        _titleEl,
        _detailTitleEl,
        _newsId,
        _curData,
        _fs = Fs,
        _win = window,
        _scrollSave = {},
        _storage = _fs.Storage,
        _helpers = App.helpers,
        _selector = _fs.Selector,
        _parent = _fs.views.Container;
    function get_social() {
        if (!_social) {
            _social = new App.views.NewsSocial();
            _social.on('favoriteUpdate', favorite_update);
            _social.compile();
            _social.render('body');
        }
        return _social;
    }
    function favorite_update(isFav) {
        _curData.is_favorite = isFav;
        _storage.setItem(_id + _newsId, _curData);
        _settings.set('newsupdated', true);
    }
    function show_social() {
        get_social().show();
    }
    var _onAfterRender = function(self) {
        var doc = document;
        _mapTpl = _fs.Templates.get('newsdetailmap');
        _dateTpl = _fs.Templates.get('newsdetaildate');
        _detailTpl.el = doc.getElementById(_id + '-item');
        _detailTpl.parentEl = _detailTpl.el.parentNode;
        _titleEl = doc.getElementById(_id + '-title');
        _detailTitleEl = doc.getElementById('news-detail-content-header')
            .querySelector('.ui-btn-text');
        self.registerButton.textEl = doc.getElementById(self.registerButton.config.id)
            .querySelector('.ui-btn-text');
        self.unregisterButton.textEl = doc.getElementById(self.unregisterButton.config.id)
            .querySelector('.ui-btn-text');
        self.zoomEvent = new _fs.events.Click({
            autoAttach: true,
            el: self.contentDetail.el,
            scope: self,
            handler: function(e) {
                if (_selector.hasClass(e.target, 'zoomable') === true) {
                    var img = e.target.getAttribute('data-full-image');
                    img = img.substring(0, img.length - 1);
                    var c = img.substr(0, 1) + img.substr(-1);
                    if (c === '""' || c === "''") {
                        img = img.substr(1, (img.length - 2));
                    }
                    if (!self.i) {
                        self.i = new _fs.views.ImageZoom({
                            config: {
                                hidden: true
                            }
                        });
                        self.i.compile();
                        self.i.render('body', 'beforeend');
                    }
                    self.i.updateImage(img).show();
                }
            }
        });
    };
    var _onShow = function(self, renderer, routeArgs) {
        _newsId = routeArgs[0];
        _backUrl = routeArgs[1];
        _loadNews(self);
    };
    var _loadNews = function(self, opts) {
        opts = opts || {
            resetScroll: false,
            forceReload: false,
            scope: self,
            callback: undefined
        };
        if (opts.resetScroll === true) {
            _scrollSave[_newsId] = 0;
        }
        if (opts.forceReload === false) {
            _detailLoading.call(self);
        }
        var news = _storage.getItem(_id + _newsId);
        if (opts.forceReload === true || !news) {
            var url = _getUrl.replace('{id}', _newsId);
            
            _fs.Request.jsonp(url + '?callback={callback}', {
                scope: self,
                callback: function(success, data) {
                    
                    if (typeof opts.callback === 'function') {
                        opts.callback.call(opts.scope || self,
                            (success && data.success));
                    }
                    if (success && data.success) {
                        _updateDetailEl.call(self, data.data, opts.forceReload);
                    } else {
                        App.errors.onJsonpError(success, data);
                    }
                }
            });
        } else {
            _updateDetailEl.call(self, news);
        }
    };
    var _onHide = function(scrollX, scrollY) {
        if (_newsId) {
            _scrollSave[_newsId] = scrollY || 0;
        }
    };
    var _detailLoading = function() {
        _detailTpl.parentEl.innerHTML = '';
        _titleEl.innerHTML = 'LOADING...';
        _selector.addClass(this.contentDetail.el, 'hidden');
        _selector.addClass(this.mapDetail.el, 'hidden');
        _selector.addClass(this.dateDetail.el, 'hidden');
        _selector.addClass(this.registerButton.el, 'hidden');
        _selector.addClass(this.unregisterButton.el, 'hidden');
    };
    var _updateDetailEl = function(data, forceReload) {
        _storage.setItem(_id + _newsId, data);
        _curData = data;
        _titleEl.innerHTML = data.title;
        _detailTpl.config.data = data;
        _detailTitleEl.innerHTML = data.created_at.substring(0, 10);
        _selector.removeClass(this.contentDetail.el, 'hidden');
        
        get_social().fireScope('recordUpdate', data);
        _updateLocation.call(this, data);
        _updateDate.call(this, data);
        _updateRegister.call(this, data);
        if (forceReload === true) {
            _detailTpl.parentEl.innerHTML = '';
        }
        _detailTpl.compile();
        _detailTpl.render(_detailTpl.parentEl);
        _win.scrollTo(0, _scrollSave[_newsId] || 0);
    };
    
    var _updateRegister = function(data) {
        var infos = (data && data.event_info ? data.event_info : null);
        if (infos) {
            var seatsEl = this.dateDetail.el.querySelector('.available_seat');
            if (seatsEl) {
                seatsEl.innerHTML = data.event_info.available_seat;
            }
            if (!infos.open_subscription) {
                this.registerButton.textEl.innerHTML = t('Not opened yet');
                this.registerButton.setDisabled(true);
            } else if (infos.is_full) {
                this.registerButton.textEl.innerHTML = t('Full');
                this.registerButton.setDisabled(true);
            } else {
                this.registerButton.textEl.innerHTML = t('Subscribe to event');
                this.registerButton.setDisabled(false);
            }
            if (infos.open_subscription && infos.has_subscribe) {
                _selector.removeClass(this.unregisterButton.el, 'hidden');
                _selector.addClass(this.registerButton.el, 'hidden');
            } else {
                _selector.removeClass(this.registerButton.el, 'hidden');
                _selector.addClass(this.unregisterButton.el, 'hidden');
            }
        } else {
            _selector.addClass(this.registerButton.el, 'hidden');
            _selector.addClass(this.unregisterButton.el, 'hidden');
        }
    };
    var _updateLocation = function(data) {
        var mapTextEl = _selector.get('.ui-btn-text', this.mapBtn.el);
        if (data && data.event_info && data.event_info.adress) {
            this.mapBtn.setDisabled(false);
            
            mapTextEl.innerHTML = '&nbsp;';
            this.mapDetail.contentEl.innerHTML = _mapTpl(data.event_info);
            _selector.removeClass(this.mapDetail.el, 'hidden');
        } else {
            this.mapBtn.setDisabled(true);
            
            mapTextEl.innerHTML = '&nbsp;';
            _selector.addClass(this.mapDetail.el, 'hidden');
        }
    };
    var _updateDate = function(data) {
        var dateTextEl = _selector.get('.ui-btn-text', this.dateBtn.el);
        if (data && data.event_info && data.event_info.start_time) {
            this.dateBtn.setDisabled(false);
            
            dateTextEl.innerHTML = '&nbsp;';
            this.dateDetail.contentEl.innerHTML = _dateTpl(data.event_info);
            _selector.removeClass(this.dateDetail.el, 'hidden');
        } else {
            this.dateBtn.setDisabled(true);
            
            dateTextEl.innerHTML = '&nbsp;';
            _selector.addClass(this.dateDetail.el, 'hidden');
        }
    };
    var _onRegisterClick = function() {
        var user = _settings.get('currentUser');
        if (typeof user !== 'object') {
            _settings.set('flashlogin', t('You need to login for register to an event.'));
            _fs.History.navigate('/user/login/?back=' + _fs.History.here(true));
            return;
        }
        _helpers.showLoading();
        var url = _subscribeUrl.replace('{id}',
            _curData.id) + '?callback={callback}';
        _fs.Request.jsonp(url, {
            scope: this,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _curData.is_favorite = true;
                    _curData.event_info.has_subscribe = true;
                    _curData.event_info.available_seat = data.data.available_seat;
                    _storage.setItem(_id + _newsId, _curData);
                    get_social().fireScope('recordUpdate', _curData);
                    _updateRegister.call(this, _curData);
                    App.success.onJsonpSuccess(success, data);
                } else {
                    App.errors.onJsonpError(success, data);
                }
            }
        });
    };
    var _onUnregisterClick = function() {
        _helpers.showLoading();
        var url = _unsubscribeUrl.replace('{id}',
            _curData.id) + '?callback={callback}';
        _fs.Request.jsonp(url, {
            scope: this,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _curData.event_info.has_subscribe = false;
                    _curData.event_info.available_seat = data.data.available_seat;
                    _storage.setItem(_id + _newsId, _curData);
                    _updateRegister.call(this, _curData);
                } else {
                    App.errors.onJsonpError(success, data);
                }
            }
        });
    };
    return _parent.subclass({
        fx: {
            hidefooter: true
        },
        constructor: function(opts) {
            var self = this;
            _detailTpl = new _fs.views.Template({
                template: _fs.Templates.get('newsdetail')
            });
            var headerItems = [{
                xtype: 'button',
                config: {
                    ui: 'f',
                    size: 'large',
                    cssClass: 'ui-btn-left simple-btn',
                    icon: '&#59233;'
                },
                scope: self,
                handler: function() {
                    _settings.set('anim', {
                        type: 'slide',
                        reverse: true
                    });
                    if (_backUrl) {
                        _fs.History.navigate(decodeURIComponent(_backUrl));
                    } else {
                        _fs.History.navigate('/news/search');
                    }
                }
            }];
            if (get_social().isActive() === true) {
                headerItems.push({
                    xtype: 'button',
                    ref: 'socialBtn',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-right simple-btn-large',
                        icon: '&#9206;'
                    },
                    scope: self,
                    handler: show_social
                });
            }
            headerItems.push(['<h1 id="', _id, '-title',
                '" class="ui-title"></h1>'].join(''));
            var items = [{
                xtype: 'header',
                config: {
                    ui: 'f',
                    position: 'fixed'
                },
                items: headerItems
            }, {
                xtype: 'abstract',
                config: {
                    style: 'padding-top: 7px;' 
                },
                ref: 'content',
                xtpl: 'content',
                items: [{
                    xtype: 'pull',
                    ref: 'pull',
                    listeners: {
                        scope: self,
                        loading: function() {
                            _loadNews(self, {
                                resetScroll: true,
                                forceReload: true,
                                scope: self,
                                callback: function() {
                                    self.pull.setLoading(false);
                                }
                            });
                        }
                    }
                }, {
                    xtype: 'button',
                    ref: 'mapBtn',
                    config: {
                        text: '',
                        icon: '&#59176;',
                        ui: 'light-gray',
                        cssClass: 'btn-notext simple-btn-mini',
                        style: 'float: right; margin: -2px -10px 0px 0px; z-index: 10;',
                        size: 'medium'
                    },
                    scope: self,
                    handler: function() {
                        _selector.scrollToEl(document.getElementById('news-detail-map'), 0, -51);
                    }
                }, {
                    xtype: 'button',
                    ref: 'dateBtn',
                    config: {
                        text: '',
                        icon: '&#128340;',
                        ui: 'light-gray',
                        cssClass: 'btn-notext simple-btn-mini',
                        style: 'float: right; margin: -2px -10px 0px 0px; z-index: 10;',
                        size: 'medium'
                    },
                    scope: self,
                    handler: function() {
                        _selector.scrollToEl(document.getElementById('news-detail-date'), 0, -51);
                    }
                }, {
                    xtype: 'collapsible',
                    ref: 'contentDetail',
                    config: {
                        id: 'news-detail-content',
                        size: 'small',
                        header: {
                            title: t('Loading'),
                            ui: 'light-gray'
                        },
                        iconCollapsed: 'calendar',
                        iconExpanded: 'calendar',
                        canCollapse: false,
                        content: {
                            ui: 'c'
                        }
                    },
                    items: _detailTpl
                }, {
                    xtype: 'collapsible',
                    ref: 'dateDetail',
                    config: {
                        id: 'news-detail-date',
                        size: 'small',
                        header: {
                            title: t('Event date'),
                            ui: 'light-gray'
                        },
                        iconCollapsed: 'clock',
                        iconExpanded: 'clock',
                        canCollapse: false,
                        content: {
                            ui: 'c'
                        }
                    }
                }, {
                    xtype: 'collapsible',
                    ref: 'mapDetail',
                    config: {
                        id: 'news-detail-map',
                        size: 'small',
                        header: {
                            title: t('Map'),
                            ui: 'light-gray'
                        },
                        iconCollapsed: 'map',
                        iconExpanded: 'map',
                        canCollapse: false,
                        content: {
                            ui: 'c'
                        }
                    }
                }, {
                    xtype: 'button',
                    ref: 'registerButton',
                    config: {
                        inline: false,
                        ui: 'g',
                        text: t('Subscribe to event'),
                        icon: '&#59136;'
                    },
                    scope: self,
                    handler: _onRegisterClick
                }, {
                    xtype: 'button',
                    ref: 'unregisterButton',
                    config: {
                        inline: false,
                        ui: 'd',
                        text: t('Unsubscribe to event'),
                        icon: '&#59201;'
                    },
                    scope: self,
                    handler: function() {
                        App.confirm.show(t('Unsubscribe'),
                            t('Are you sure you want to unsubscribe from this event ?'),
                            function(answer) {
                                if (answer === true) {
                                    _onUnregisterClick.call(self);
                                }
                            });
                    }
                }]
            }, '</div>'];
            _parent.prototype.constructor.call(self, {
                config: _fs.utils.applyIfAuto({
                    style: 'padding-top: 43px;'
                }, opts.config),
                listeners: {
                    scope: self,
                    afterrender: _onAfterRender
                },
                items: items
            });
            self.showEID = self.on('show', _onShow, self);
            self.hideEID = self.on('hide', _onHide, self);
        }
    });
}());
App.views.Newssearch = (function() {
    var _handlebars = Handlebars,
        _serverUrl = App.Settings.get('serverUrl'),
        _contentUrl = App.Settings.gets('serverUrl', 'contentSearch').join(''),
        _categoryUrl = App.Settings.gets('serverUrl', 'categoryList').join('');
    _handlebars.registerHelper('hasThumbnail', function(context, options) {
        if (context && context.length) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    _handlebars.registerHelper('getThumbnail', function(medias) {
        return '"' + _serverUrl + medias[0].thumb.replace('"', '\\"') + '"';
    });
    var _needReload,
        _newsList,
        _header,
        _categoryMenu,
        _categoryList,
        _fs = Fs,
        _fsviews = _fs.views,
        _debug = _fs.debug,
        _parent = _fsviews.Container,
        _settings = App.Settings,
        _emptyTpl = [
            '<div class="news-listing-empty">',
                '<span class="icon">&#128240;</span>',
                '<span class="message">',
                    t('No news.'),
                '</span>',
            '</div>'
        ].join('');
    var _onShow = function() {
        if (_needReload === true) {
            _newsList.reload();
            _needReload = false;
        }
        this.showCategoriesEvent.attach();
    };
    var _onHide = function() {
        this.showCategoriesEvent.detach();
    };
    var _onAfterRender = function() {
        function forceReload() {
            _needReload = true;
        }
        _settings.on('currentUserchanged', forceReload);
        _settings.on('newsupdatedchanged', forceReload);
        this.headerEl = document.getElementById(_header.config.id);
        this.showCategoriesEvent = new _fs.events.Click({
            autoAttach: true,
            el: this.headerEl,
            handler: function() {
                _categoryMenu.show();
            }
        });
    };
    return _parent.subclass({
        constructor: function(opts) {
            this.categoriesStore = new Fs.data.Store({
                enableHashTable: true,
                storageCache: true,
                reader: {
                    rootProperty: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                },
                proxy: {
                    type: 'jsonp',
                    cache: false,
                    url: _categoryUrl + '?callback={callback}'
                }
            });
            var self = this;
            this.categoriesStore.on('afterload', function(success, store, newRecords) {
                var record = {
                    id: 0,
                    name: t('Show all'),
                    color: 'white'
                };
                self.categoriesStore.addRecord(record);
                _categoryList.loadItems([record]);
            }, this);
            this.categoriesStore.on('loaderror', App.errors.onStoreError);
            this.newsStore = new Fs.data.PagingStore({
                enableHashTable: true,
                recordPerPage: 10,
                storageCache: true,
                reader: {
                    rootProperty: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                },
                proxy: {
                    type: 'jsonp',
                    cache: false,
                    url: _contentUrl + '?callback={callback}'
                }
            });
            this.newsStore.on('loaderror', App.errors.onStoreError);
            _newsList = new _fsviews.ListBuffered({
                config: {
                    cssClass: 'newslist scroll',
                    style: 'margin-top: 0px; margin-bottom: 0px;',
                    ui: 'w'
                },
                loading: false,
                emptyBeforeLoad: false,
                store: this.newsStore,
                itemTpl: _fs.Templates.get('newssearchlist'),
                emptyTpl: _emptyTpl,
                listeners: {
                    scope: this,
                    itemselect: function(e) {
                        var recordId,
                            item = _newsList.getItemFromEvent(e);
                        if (item !== false) {
                            recordId = item.getAttribute('data-id');
                            _settings.set('anim', {
                                type: 'slide'
                            });
                             
                            Fs.History.navigate('/news/detail/' + recordId);
                        }
                    }
                }
            });
            _header = new _fsviews.Header({
                config: {
                    id: 'news-header',
                    ui: 'f',
                    position: 'fixed'
                },
                items: ['<h1 class="ui-title" id="news-category-title">' + G_APP_NAME + '<div class="icon news-header-arrow">&#9662;</div></h1>']
            });
            _categoryList = new _fsviews.List({
                config: {
                    cssClass: 'categorylist list-flat',
                    ui: 'flat',
                    style: 'height: 70%; min-height: 30px; max-height: 300px;'
                },
                store: this.categoriesStore,
                itemTpl: _fs.Templates.get('newssearchcategorylist'),
                listeners: {
                    scope: this,
                    itemselect: function(e) {
                        var item = _categoryList.getItemFromEvent(e),
                            category_id = item.getAttribute('data-id'),
                            record = this.categoriesStore.getRecord(category_id);
                        var catname = record.name;
                        if (!record.id) {
                            catname = G_APP_NAME;
                        }
                        document.getElementById('news-category-title').innerHTML = catname +
                            '<div class="icon news-header-arrow">&#9662;</div>';
                        _debug.log('news', 'category item click', category_id);
                        _categoryMenu.hide();
                        if (!record.id) {
                            _newsList.store.setProxyOpts({
                                jsonParams: {}
                            });
                        } else {
                            _newsList.store.setProxyOpts({
                                jsonParams: {
                                    category: category_id
                                }
                            });
                        }
                        var eid = _newsList.store.on('afterload', function() {
                            App.helpers.hideLoading();
                            _newsList.store.off(eid);
                        }, this);
                        App.helpers.showLoading();
                        _newsList.reload();
                        _debug.log('news', 'store proxy opts', _newsList.store.getProxyOpts());
                    }
                }
            });
            
            _categoryMenu = new _fsviews.FloatingPanel({
                config: {
                    id: 'category-menu',
                    hidden: true,
                    overlay: 'darklight',
                    arrow: 'top',
                    style: [
                        'top: 55px;',
                        'width: 80%; min-width: 175px; max-width: 350px;',
                        'min-height: 40px;',
                        'box-shadow: 0px 0px 21px 0px rgba(0, 0, 0, 0.6);'
                    ].join('')
                },
                listeners: {
                    scope: this,
                    hide: function() {
                        _fs.Selector.removeClass(document.getElementById('mainpage'), 'blur');
                    },
                    show: function() {
                        _fs.Selector.addClass(document.getElementById('mainpage'), 'blur');
                    }
                },
                items: [
                    _categoryList
                ]
            });
            opts = {
                config: opts.config,
                items: [
                    _header, {
                        xtype: 'abstract',
                        ref: 'content',
                        xtpl: 'content',
                        config: {
                            style: 'padding-top: 0px;'
                        },
                        items: [{
                            xtype: 'pull',
                            ref: 'pull',
                            listeners: {
                                scope: this,
                                loading: function() {
                                    var eid = _newsList.store.on('afterload', function() {
                                        this.pull.setLoading(false);
                                        _newsList.store.off(eid);
                                    }, this);
                                    _newsList.reload();
                                }
                            }
                        }, _newsList]
                    },
                    _categoryMenu
                ],
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            };
            _parent.prototype.constructor.call(this, opts);
            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
        }
    });
}());
App.views.Userfavorites = (function() {
    
    var _needReload,
        _fs = Fs,
        _settings = App.Settings,
        _parent = _fs.views.Container,
        _contentUrl = App.Settings.gets('serverUrl', 'userFavorites').join(''),
        _emptyTpl = [
            '<div class="news-listing-empty">',
                '<span class="icon">&#9733;</span>',
                '<span class="message">',
                    t('No favorites.'),
                '</span>',
            '</div>'
        ].join('');
    var _onShow = function() {
        if (_needReload === true) {
            this.newsList.reload();
            _needReload = false;
        }
    };
    var _onAfterRender = function() {
        function forceReload() {
            _needReload = true;
        }
        _settings.on('currentUserchanged', forceReload);
        _settings.on('newsupdatedchanged', forceReload);
    };
    return _parent.subclass({
        constructor: function(opts) {
            this.newsStore = new _fs.data.PagingStore({
                enableHashTable: true,
                recordPerPage: 10,
                reader: {
                    rootProperty: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                },
                proxy: {
                    type: 'jsonp',
                    cache: false,
                    url: _contentUrl + '?callback={callback}'
                }
            });
            this.newsStore.on('loaderror', App.errors.onStoreError);
            opts.config.style = 'padding-bottom: 52px;';
            opts = {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Favorites'),
                        position: 'fixed'
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            ui: 'f',
                            size: 'large',
                            cssClass: 'ui-btn-right simple-btn-medium',
                            icon: '&#10227;'
                        },
                        scope: this,
                        handler: function() {
                            this.newsList.reload();
                        }
                    }]
                }, {
                    xtype: 'listbuffered',
                    config: {
                        cssClass: 'newslist scroll',
                        ui: 'w'
                    },
                    ref: 'newsList',
                    store: this.newsStore,
                    itemTpl: _fs.Templates.get('userfavoriteslist'),
                    emptyTpl: _emptyTpl,
                    listeners: {
                        scope: this,
                        itemselect: function(e) {
                            var recordId,
                                item = this.newsList.getItemFromEvent(e);
                            recordId = item.getAttribute('data-id');
                            _settings.set('anim', {
                                type: 'slide'
                            });
                            _fs.History.navigate('/news/detail/' + recordId +
                                '/?back=' + encodeURIComponent('/user/favorites'));
                        }
                    }
                }]
            };
            _parent.prototype.constructor.call(this, opts);
            this.on('show', _onShow, this);
            this.on('afterrender', _onAfterRender, this);
        }
    });
}());
App.views.Userlogin = (function() {
    var _backUrl,
        _fs = Fs,
        _app = App,
        _selector = _fs.Selector,
        _settings = _app.Settings,
        _url = _settings.gets('serverUrl', 'userLogin').join(''),
        _helpers = _app.helpers,
        _errors = _app.errors,
        _request = _fs.Request,
        _parent = _fs.views.Container;
    var _onAfterRender = function() {
        this.keypressEvent = new _fs.events.Abstract({
            eventName: 'keyup',
            el: this.el,
            autoAttach: true,
            scope: this,
            handler: _keypressed
        });
        this.flashMsg = this.flash.el.querySelector('.message');
        _settings.on('flashloginchanged', _showflash, this);
    };
    var _showflash = function(set, name, value) {
        var msg = value || _settings.get('flashlogin');
        if (typeof msg === 'string') {
            this.flashMsg.innerHTML = msg;
            this.flash.show();
            if (this.hidden === false) {
                _settings.empty('flashlogin');
            }
        }
    };
    var _onShow = function(routeArgs) {
        this.hidden = false;
        _backUrl = (routeArgs[0] ? decodeURIComponent(routeArgs[0]) : false);
        if (_backUrl && _backUrl.indexOf('favorites') !== -1) {
            _settings.set('flashlogin',
                t('You need to login to use favorites.'));
        }
        this.keypressEvent.attach();
        _showflash.call(this);
    };
    var _onHide = function() {
        var self = this;
        self.hidden = true;
        self.keypressEvent.detach();
        setTimeout(function() {
            self.flash.hide();
        }, 600);
    };
    var _keypressed = function(e) {
        var target = e.target,
            parent = target.parentNode;
        if (_selector.hasClass(parent, 'fielderror') === true) {
            if (target.validity.valid === true) {
                _selector.removeClass(parent, 'fielderror');
            }
        }
        if (e.keyCode === 13) {
            e.target.blur();
            _login.call(this);
        }
    };
    var _login = function() {
        if (_helpers.validateFields('input[name]', this.el) === false) {
            return false;
        }
        var fields = _helpers.fieldsToJson('input[name]', this.el),
            url = _url + '&callback={callback}';
        if (fields !== false) {
            url = _helpers.replaceByValues(url, fields, encodeURIComponent);
            _helpers.showLoading();
            _request.jsonp(url, {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        App.Settings.set('currentUser', data.user);
                        _fs.Storage.empty();
                        _fs.History.navigate('');
                    } else {
                        _errors.onJsonpError(success, data);
                    }
                }
            });
        }
    };
    return _parent.subclass({
        xtpl: 'container',
        fx: {
            hidefooter: true,
            animation: 'upslide'
        },
        constructor: function(opts) {
            var fields = [{
                xtype: 'textfield',
                config: {
                    icon: 'mail',
                    type: 'email',
                    name: 'email',
                    required: true,
                    placeHolder: t('Email') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    icon: 'key',
                    type: 'password',
                    name: 'password',
                    required: true,
                    placeHolder: t('Password') + '...'
                }
            }, {
                xtype: 'button',
                ref: 'loginBtn',
                config: {
                    inline: false,
                    ui: 'g',
                    text: t('Sign in'),
                    xicon: '&#128274;'
                },
                scope: this,
                handler: _login
            }, ('<hr class="separator"><i class="separator-text">' + t('or') + '</i>'), {
                xtype: 'button',
                config: {
                    inline: false,
                    ui: 'c',
                    text: t('Register'),
                    xicon: '&#59136;'
                },
                scope: this,
                handler: function() {
                    _fs.History.navigate('/user/register' +
                        (_backUrl ? ('/?back=' + encodeURIComponent(_backUrl)) : ''));
                }
            }];
            var flashmsg = [{
                xtype: 'flash',
                config: {
                    type: 'warning',
                    hidden: true
                },
                ref: 'flash',
                items: ['<p><div class="icon">&#9888;</div><span class="message"></span></p>']
            }];
            var items = [{
                xtype: 'header',
                config: {
                    ui: 'f',
                    title: t('Login'),
                    position: 'fixed'
                },
                items: [{
                    xtype: 'button',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-left simple-btn back-btn',
                        icon: '&#59233;'
                    },
                    scope: this,
                    handler: function() {
                        _settings.set('anim', {
                            type: 'slide',
                            reverse: true
                        });
                        _fs.History.navigate('/more/menu');
                    }
                }]
            }, {
                xtype: 'abstract',
                xtpl: 'content',
                items: flashmsg.concat(fields)
            }];
            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: items,
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });
            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
        }
    });
}());
App.views.Userprofile = (function() {
    var _fs = Fs,
        _settings = App.Settings,
        _url = _settings.gets('serverUrl', 'userProfileUpdate').join(''),
        _helpers = App.helpers,
        _errors = App.errors,
        _request = _fs.Request,
        _selector = _fs.Selector,
        _parent = _fs.views.Container;
    var _onAfterRender = function() {
        this.keypressEvent = new _fs.events.Abstract({
            eventName: 'keypress',
            el: this.el,
            autoAttach: true,
            scope: this,
            handler: _keypressed
        });
        var user = _settings.get('currentUser'),
            fields = _fs.Selector.gets('input[name]', this.el);
        _settings.on('currentUserchanged', function(settings, name, value) {
            _helpers.jsonToFields(value, fields);
        }, this);
        if (user) {
            _helpers.jsonToFields(user, fields);
        }
    };
    var _keypressed = function(e) {
        var target = e.target,
            parent = target.parentNode;
        if (_selector.hasClass(parent, 'fielderror') === true) {
            if (target.validity.valid === true) {
                _selector.removeClass(parent, 'fielderror');
            }
        }
        if (e.keyCode === 13) {
            e.target.blur();
            _updateProfile.call(this);
        }
    };
    var _updateProfile = function() {
        if (_helpers.validateFields('input[name]', this.el) === false) {
            return false;
        }
        var fields = _helpers.fieldsToJson('input[name]', this.el),
            url = _url + '&callback={callback}';
        if (fields !== false) {
            url = _helpers.replaceByValues(url, fields, encodeURIComponent);
            _helpers.showLoading();
            _request.jsonp(url, {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        _settings.set('currentUser', data.user);
                    } else {
                        _errors.onJsonpError(success, data);
                    }
                }
            });
        } else {
            var self = this;
            setTimeout(function() {
                _fs.Selector.get('input[name="current_password"]', self.el).focus();
            }, 500);
        }
    };
    return _parent.subclass({
        fx: {
            hidefooter: true,
            animation: 'paraslide'
        },
        constructor: function(opts) {
            var self = this;
            var profile = [{
                xtype: 'textfield',
                config: {
                    label: t('Email'),
                    labelInline: true,
                    icon: 'mail',
                    type: 'tel',
                    name: 'email',
                    readonly: true,
                    required: true,
                    disabled: true,
                    placeHolder: t('Email') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Name'),
                    labelInline: true,
                    icon: 'user',
                    name: 'name',
                    placeHolder: t('Name') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Company'),
                    labelInline: true,
                    icon: 'company',
                    name: 'company',
                    placeHolder: t('Company') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Role'),
                    labelInline: true,
                    icon: 'role',
                    name: 'role',
                    placeHolder: t('Role') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Phone'),
                    labelInline: true,
                    icon: 'phone',
                    type: 'tel',
                    name: 'phone',
                    placeHolder: t('Phone') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Current password'),
                    labelInline: true,
                    icon: 'key',
                    type: 'password',
                    name: 'current_password',
                    required: true,
                    placeHolder: t('Password') + '...'
                }
            }, {
                xtype: 'button',
                config: {
                    text: t('Update'),
                    ui: 'g',
                    inline: false
                },
                scope: this,
                handler: _updateProfile
            }];
            var items = [{
                xtype: 'header',
                config: {
                    ui: 'f',
                    title: t('My profile'),
                    position: 'fixed'
                },
                items: [{
                    xtype: 'button',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-left simple-btn',
                        icon: '&#59233;'
                    },
                    scope: this,
                    handler: function() {
                        _settings.set('anim', {
                            type: 'slide',
                            reverse: true
                        });
                        _fs.History.navigate('/more/menu');
                    }
                }]
            }, {
                xtype: 'abstract',
                config: {
                    style: 'padding-bottom: 51px;'
                },
                xtpl: 'content',
                items: profile
            }];
            _parent.prototype.constructor.call(self, {
                config: opts.config,
                items: items,
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });
        }
    });
}());
App.views.Userregister = (function() {
    var _fs = Fs,
        _settings = App.Settings,
        _url = _settings.gets('serverUrl', 'userRegister').join(''),
        _helpers = App.helpers,
        _errors = App.errors,
        _request = _fs.Request,
        _selector = _fs.Selector,
        _parent = _fs.views.Container;
    var _onAfterRender = function() {
        this.keypressEvent = new _fs.events.Abstract({
            eventName: 'keypress',
            el: this.el,
            autoAttach: true,
            scope: this,
            handler: _keypressed
        });
    };
    var _onShow = function(routeArgs) {
        _backUrl = decodeURIComponent(routeArgs[0]) || undefined;
        this.keypressEvent.attach();
    };
    var _onHide = function() {
        this.keypressEvent.detach();
    };
    var _keypressed = function(e) {
        var target = e.target,
            parent = target.parentNode;
        if (_selector.hasClass(parent, 'fielderror') === true) {
            if (target.validity.valid === true) {
                _selector.removeClass(parent, 'fielderror');
            }
        }
        if (e.keyCode === 13) {
            e.target.blur();
            _register.call(this);
        }
    };
    var _register = function() {
        if (_helpers.validateFields('input[name]', this.el) === false) {
            return false;
        }
        var fields = _helpers.fieldsToJson('input[name]', this.el),
            url = _url + '&callback={callback}';
        if (fields !== false) {
            url = _helpers.replaceByValues(url, fields, encodeURIComponent);
            _helpers.showLoading();
            _request.jsonp(url, {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        App.success.onJsonpSuccess(success, data);
                        App.Settings.set('currentUser', data.user);
                        _fs.History.navigate('');
                    } else {
                        _errors.onJsonpError(success, data);
                    }
                }
            });
        }
    };
    return _parent.subclass({
        xtpl: 'container',
        fx: {
            hidefooter: true,
            animation: 'upslide'
        },
        constructor: function(opts) {
            var items = [{
                xtype: 'textfield',
                ref: 'emailField',
                config: {
                    name: 'name',
                    icon: 'user',
                    type: 'text',
                    required: false,
                    placeHolder: t('Name') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'emailField',
                config: {
                    name: 'email',
                    icon: 'mail',
                    type: 'email',
                    required: true,
                    placeHolder: t('Email') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'passField',
                config: {
                    name: 'password',
                    icon: 'key',
                    type: 'password',
                    required: true,
                    placeHolder: t('Password') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'passconfirmField',
                config: {
                    name: 'password_confirmation',
                    icon: 'key',
                    type: 'password',
                    required: true,
                    placeHolder: t('Confirm password') + '...'
                }
            }, {
                xtype: 'button',
                config: {
                    inline: false,
                    ui: 'g',
                    text: t('Register'),
                    xicon: '&#128274;'
                },
                scope: this,
                handler: _register
            }];
            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Register'),
                        position: 'fixed'
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            ui: 'f',
                            size: 'large',
                            cssClass: 'ui-btn-left simple-btn back-btn',
                            icon: '&#59233;'
                        },
                        scope: this,
                        handler: function() {
                            _settings.set('anim', {
                                type: 'slide',
                                reverse: true
                            });
                            _fs.History.navigate('/more/menu');
                        }
                    }]
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    items: items
                }],
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });
            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
        }
    });
}());
App.views.Moremenu = (function() {
    var _fs = Fs,
        _settings = App.Settings,
        _helpers = App.helpers,
        _errors = App.errors,
        _request = _fs.Request,
        _selector = _fs.Selector,
        _parent = _fs.views.Container;
    var _logout = function() {
        var url = _settings.gets('serverUrl',
            'userLogout').join('') + '&callback={callback}';
        _helpers.showLoading();
        _request.jsonp(url, {
            scope: this,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _fs.Storage.empty();
                    _settings.set('currentUser', null);
                } else {
                    _errors.onJsonpError(success, data);
                }
            }
        });
    };
    var _refreshUI = function() {
        if (this.hidden) {
            this.needRefresh = true;
            return false;
        }
        var user = arguments[2] || _settings.get('currentUser');
        if (typeof user !== 'object') {
            _selector.addClass(this.userList.el, 'hidden');
            _selector.removeClass(this.anonymousList.el, 'hidden');
        } else {
            this.userList.bubbleEl.innerHTML = user.email;
            _selector.removeClass(this.userList.el, 'hidden');
            _selector.addClass(this.anonymousList.el, 'hidden');
        }
    };
    var _onShow = function() {
        this.hidden = false;
        if (this.needRefresh) {
            this.needRefresh = false;
            _refreshUI.call(this);
        }
    };
    var _onHide = function() {
        this.hidden = true;
    };
    var _onAfterRender = function() {
        if (App.conf.enabled('user') === true) {
            _settings.on('currentUserchanged', _refreshUI, this);
            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
            this.userList.bubbleEl = this.userList.el.querySelector('.ui-li-count');
            _refreshUI.call(this);
        }
    };
    var _onItemSelect = function(event) {
        var href = event.target.getAttribute('href');
        if (typeof href !== 'string') {
            href = event.target.parentNode.getAttribute('href');
        }
        if (href === '#/user/logout') {
            _logout();
        } else {
            _settings.set('anim', {
                type: 'slide'
            });
            _fs.History.navigate(href.replace('#', ''));
        }
    };
    return _parent.subclass({
        constructor: function(opts) {
            var items = [];
            if (App.conf.enabled('user') === true) {
                items.push({xtype: 'list',
                    ref: 'userList',
                    config: {
                        ui: 'cleanlist',
                        icon: 'arrow-r',
                        inset: true,
                        corner: 'all'
                    },
                    itemTpl: _fs.Templates.get('moremenu'),
                    items: [{
                        type: 'divider',
                        ui: 'g',
                        label: t('Logged as'),
                        bubble: '-'
                    }, {
                        label: t('My profile'),
                        labelIcon: '&#59170;',
                        route: '/user/profile'
                    }, {
                        label: t('Logout'),
                        labelIcon: '&#10150;',
                        route: '/user/logout'
                    }],
                    listeners: {
                        scope: this,
                        itemselect: _onItemSelect
                    }
                });
                items.push({
                    xtype: 'list',
                    ref: 'anonymousList',
                    config: {
                        ui: 'cleanlist',
                        icon: 'arrow-r',
                        inset: true,
                        corner: 'all',
                        shadow: true
                    },
                    itemTpl: _fs.Templates.get('moremenu'),
                    items: [{
                        type: 'divider',
                        ui: 'c',
                        label: t('Account')
                    }, {
                        label: t('Login'),
                        labelIcon: '&#128100;',
                        route: '/user/login/?back=' + _fs.History.here(true)
                    }, {
                        label: t('Register'),
                        labelIcon: '&#59136;',
                        route: '/user/register/?back=' + _fs.History.here(true)
                    }],
                    listeners: {
                        scope: this,
                        itemselect: _onItemSelect
                    }
                });
            }
            if (App.conf.enabled('about') === true ||
                App.conf.enabled('contact') === true) {
                var btns = [];
                if (App.conf.enabled('contact')) {
                    btns.push({
                        label: t('Contact'),
                        labelIcon: '&#59176;',
                        route: '/more/contact'
                    });
                }
                if (App.conf.enabled('about')) {
                    btns.push({
                        label: t('About us'),
                        labelIcon: '&#59141;',
                        route: '/more/about'
                    });
                }
                items.push({
                    xtype: 'list',
                    config: {
                        ui: 'cleanlist',
                        icon: 'arrow-r',
                        inset: true,
                        corner: 'all',
                        shadow: true
                    },
                    itemTpl: _fs.Templates.get('moremenu'),
                    items: btns,
                    listeners: {
                        scope: this,
                        itemselect: _onItemSelect
                    }
                });
            }
            opts.config.style = 'padding-bottom: 52px;';
            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('More'),
                        position: 'fixed'
                    }
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    items: items
                }],
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });
        }
    });
}());
App.views.Moreabout = (function() {
    var _fs = Fs,
        _settings = App.Settings,
        _parent = _fs.views.Container;
    return _parent.subclass({
        fx: {
            hidefooter: true,
            animation: 'upslide'
        },
        constructor: function(opts) {
            var blabla = '北京意得捷优科技有限公司是一家集自主研发及国外技术为一体的专业APP开发公司。\n以独创的技术快速为客户打造出精致、实用、兼容性强的移动应用。\n旨在打造出适用于国内中小企业需求的新媒体工具。\n详情及合作方式请关注：<a href="http://www.itsium.cn" target="_blank">www.itsium.cn</a>';
            if (blabla.indexOf('<br>') === -1) {
                var breakTag = '<br />';
                blabla = (blabla + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
            }
            var date = (new Date()).getFullYear(),
                version = G_APP_VERSION,
                about = [
                '<div class="ui-grid-solo about">',
                    '<div class="ui-block-a icon-app">',
                        
                    '</div>',
                '</div><div class="ui-grid-solo">',
                    '<div class="ui-block-a">',
                        '<div class="desc-app">',
                            '<span class="title-app">', G_COMPANY_NAME, '</span><br>',
                            '<span class="date-app">&nbsp;version ', version, '</span>',
                        '</div>',
                    '</div>',
                '</div>',
                
                '<p>', blabla, '</p><br>',
                '<hr class="separator"><i class="separator-text">', date, '</i>',
                '<div class="about-devby">Developed by ',
                    '<a href="http://www.itsium.cn" target="_blank">itsium.cn</a>',
                '</div>'
            ].join('');
            var items = [{
                xtype: 'header',
                config: {
                    ui: 'a',
                    position: 'fixed',
                    title: t('About')
                },
                items: [{
                    xtype: 'button',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-left simple-btn back-btn',
                        icon: '&#59233;'
                    },
                    scope: this,
                    handler: function() {
                        _settings.set('anim', {
                            reverse: true
                        });
                        _fs.History.navigate('/more/menu');
                    }
                }]
            }, {
                xtype: 'abstract',
                xtpl: 'content',
                config: {
                    
                },
                items: about
            }];
            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: items
            });
        }
    });
}());
App.controllers.Main = (function() {
    
    var _layoutPage,
        _mainMenu,
        _activeTab,
        _lastPage,
        _defaultScrollY = (navigator.userAgent.match(/Android/i) ? 1 : 0),
        _pages = {},
        _fs = Fs,
        _history = _fs.History,
        _parent = _fs.Router,
        _app = App,
        _appViews = _app.views,
        _settings = _app.Settings,
        _selector = _fs.Selector,
        _scrollPos = {},
        _views = {
            previous: null,
            current: null
        },
        _activeBtnClass = 'ui-btn-active';
    var _changeActiveTab = function(newTab) {
        var ntab = document.getElementById(newTab);
        if (!ntab) {
            return;
        }
        var activeTabs = _selector.gets('.' + _activeBtnClass,
            document.getElementById('main-menu')),
            length = activeTabs.length;
        while (length--) {
            _selector.removeClass(activeTabs[length], _activeBtnClass);
        }
        _selector.addClass(ntab, _activeBtnClass);
        _activeTab = newTab;
    };
    var _switchPage = function(newPage, routeArgs) {
        var anim,
            w = window,
            fromPage = _pages[_lastPage],
            toPage = _pages[newPage];
        function revert_scroll() {
            if (
                w.navigator.userAgent.match(/Android/i)) {
                var hideAddressBar = function() {
                    
                    w.scrollTo(0,0);
                    var nPageH = document.height;
                    var nViewH = w.outerHeight;
                    if (nViewH > nPageH) {
                        nViewH -= 250;
                        document.body.style.height = nViewH + 'px';
                    }
                    w.scrollTo(0,1);
                };
                hideAddressBar();
            }
            
        }
        function check_footer() {
            if (_views.current.fx && _views.current.fx.hidefooter === true) {
                _selector.addClass(document.getElementById('page-footer'), 'hide');
            } else {
                _selector.removeClass(document.getElementById('page-footer'), 'hide');
            }
        }
        if (_lastPage) {
            _scrollPos[_lastPage] = (w.scrollY < 1 ? _defaultScrollY : w.scrollY);
            
            anim = (_fs.fx.isAnimating() ? false : _settings.get('anim'));
            if (!anim && _views.current.fx) {
                anim = _views.current.fx;
            }
            if (!anim) {
                
                _selector.addClass(document.getElementById(_lastPage),
                    'hidden');
                check_footer();
            } else {
                _animateTransition(anim, document.getElementById(_lastPage), document.getElementById(newPage), {
                    reverse: (anim.reverse === true ? true : false),
                    callback: revert_scroll
                });
            }
            if (fromPage) {
                fromPage.fireScope('hide', fromPage, 0, _scrollPos[_lastPage]);
            }
        } else {
            check_footer();
        }
        if (!anim) {
            _selector.removeClass(document.getElementById(newPage),
                'hidden');
            revert_scroll();
        }
        _lastPage = newPage;
        if (toPage) {
            toPage.fireScope('show', toPage, routeArgs);
        }
    };
    var _animateTransition = function(anim, from, to, opts) {
        _settings.empty('anim');
        var footer = document.getElementById('page-footer');
        if (_views.current.fx && _views.current.fx.hidefooter === true) {
            opts.afterstart = function() {
                _selector.addClass(footer, 'hide');
            };
        } else if (_selector.hasClass(footer, 'hide')) {
            opts.beforestart = function() {
                _selector.removeClass(footer, 'hide');
            };
        }
        var view = _views.current;
        if (opts.reverse === true) {
            view = _views.previous;
        }
        if (view.fx && view.fx.animation) {
            anim.type = view.fx.animation;
        } else {
            anim.type = 'paraslide';
        }
        _fs.fx[anim.type](from, to, opts);
    };
    var _routeGenerate = function(name, parent) {
        return function() {
            var pageId = 'page-' + name,
                p = _pages[pageId],
                tabname = 'menu-' + (parent ? parent : name) + '-btn';
            if (!p) {
                p = _pages[pageId] = new _appViews[_fs.utils.capitalize(name)]({
                    config: {
                        id: pageId,
                        hidden: true
                    }
                });
                p.compile();
                p.render('#page-footer', 'beforebegin');
            }
            if (_views.current) {
                _views.previous = _views.current;
            }
            _views.current = p;
            _changeActiveTab(tabname);
            _switchPage(pageId, arguments);
        };
    };
    var _parseConfiguration = function() {
        var i, item, conf = App.conf;
        for (i in conf) {
            item = conf[i];
            if (item.enabled === true && item.router) {
                if (item.router.routes) {
                    var route, rconf;
                    for (route in item.router.routes) {
                        rconf = item.router.routes[route];
                        this.routes[route] = rconf[0];
                        this[rconf[0]] = _routeGenerate.apply(this, rconf);
                    }
                }
                if (item.router.before) {
                    var regexp;
                    for (regexp in item.router.before) {
                        this.before[regexp] = item.router.before[regexp];
                    }
                }
            }
        }
    };
    
    return _parent.subclass({
        before: {},
        routes: {
            '': 'routeNewsSearch',
            '/news/search': 'routeNewsSearch',
            '/news/detail/:id(/?back=:back)': 'routeNewsDetail',
            
            '/more/menu': 'routeMoreMenu'
        },
        constructor: function() {
            if (!_layoutPage) {
                _layoutPage = new _appViews.Layout({
                    config: {
                        id: 'mainpage'
                    }
                });
                _layoutPage.compile();
                _layoutPage.render('body');
            }
            _mainMenu = document.getElementById('main-menu');
            
            _parseConfiguration.call(this);
            _parent.prototype.constructor.apply(this, arguments);
            _fs.History.start();
        },
        isAuthenticated: function(callback) {
            var user = _settings.get('currentUser');
            if (!user) {
                _history.navigate('/user/login/?back=' + _history.here(true));
            } else {
                callback();
            }
        },
        isAlreadyAuthenticated: function(callback) {
            var user = _settings.get('currentUser');
            if (!user) {
                callback();
            } else {
                _history.navigate('/user/profile');
            }
        },
        routeNewsSearch: _routeGenerate('newssearch', 'news'),
        routeNewsDetail: _routeGenerate('newsdetail', 'news'),
        routeMoreMenu: _routeGenerate('moremenu', 'more')
    });
}());
(function() {
    Fs.Templates.setPath('/assets/mobile/templates/');
    var _getTemplates = function() {
        var i, item, templates = [], conf = App.conf;
        for (i in conf) {
            item = conf[i];
            if (item.enabled === true &&
                item.templates) {
                templates = templates.concat(item.templates);
            }
        }
        return templates;
    };
    var app = new Fs.App({
        require: {
            templates: [
                'moremenu',
                'newsdetail',
                'newsdetaildate',
                'newsdetailmap',
                'newssearchcategorylist',
                'newssearchlist'
            ].concat(_getTemplates())
        },
        defaultRoute: '',
        engine: 'JqueryMobile',
        ui: 'c',
        debug: false,
        onready: function() {
            var main = new App.controllers.Main();
            if (window.canAddToHome()) {
                var addtohome = new Fs.views.AddToHome({
                    renderTo: 'body',
                    timeout: (60 * 60 * 24 * 7), 
                    config: {
                        image: '/assets/icons/Icon-72@2x.png'
                    },
                    items: '先点击<div class="icon addtohome-content-icon">&#59157;</div><br>再"添加到主屏幕"'
                });
            }
        }
    });
}());