/**
 * @class Fs.views.ImageZoom
 * @extends Fs.views.Template
 */
Fs.views.ImageZoom = (function() {

    var _fs = Fs,
        _parent = _fs.views.Template;

    var panning = false,
        zooming = false,
        startX0,
        startY0,
        startX1,
        startY1,
        endX0,
        endY0,
        endX1,
        endY1,
        startDistanceBetweenFingers,
        endDistanceBetweenFingers,
        pinchRatio,
        imgWidth, // defined onload
        imgHeight, // defined onload
        currentContinuousZoom = 1.0,
        currentOffsetX = -100,
        currentOffsetY = -100,
        currentWidth = imgWidth,
        currentHeight = imgHeight,
        newContinuousZoom,
        newHeight,
        newWidth,
        newOffsetX,
        newOffsetY,
        centerPointStartX,
        centerPointStartY,
        centerPointEndX,
        centerPointEndY,
        translateFromZoomingX,
        translateFromZoomingY,
        translateFromTranslatingX,
        translateFromTranslatingY,
        translateTotalX,
        translateTotalY,
        percentageOfImageAtPinchPointX,
        percentageOfImageAtPinchPointY,
        theImage;

    document.addEventListener('DOMContentLoaded', function() {
        var style = document.createElement('style');

        style.innerHTML = [
            'body.hasoverlay { background: #000 !important; overflow: hidden !important; }',

            '.imagezoom-overlay {',
                'z-index: 20000;',
                'width: 100%;',
                'height: 100%;',
                'position: fixed;',
                'top: 0px;',
                'left: 0px;',
                'background: #000;',
                'overflow: hidden;',
            '}',

            '.imagezoom-overlay .imagezoom-image {',
                'z-index: 20001;',
                'position: absolute;',
                //'top: 50%;',
                //'left: 50%;',
            '}'
        ].join('');

        style.id = 'imagezoom-style';
        document.head.appendChild(style);

        document.removeEventListener('DOMContentLoaded', arguments.callee, false);
    }, false);

    var _tpl = function(data) {

        var str = [
            '<div id="{id}" class="imagezoom-overlay{hidden}">',
                '<img class="imagezoom-image">',
            '</div>'
        ].join('');

        if (data.hidden === true) {
            str = str.replace('{hidden}', ' hidden');
        } else {
            str = str.replace('{hidden}', '');
        }

        return str.replace('{id}', data.id);

    };

    var _touchstart = function(e) {

        panning = false;
        zooming = false;

        if (e.touches.length == 1) {
            panning = true;
            startX0 = e.touches[0].pageX;
            startY0 = e.touches[0].pageY;
        }

        if (e.touches.length == 2) {
            zooming = true;
            startX0 = e.touches[0].pageX;
            startY0 = e.touches[0].pageY;
            startX1 = e.touches[1].pageX;
            startY1 = e.touches[1].pageY;
            centerPointStartX = ((startX0 + startX1) / 2.0);
            centerPointStartY = ((startY0 + startY1) / 2.0);
            percentageOfImageAtPinchPointX = (centerPointStartX - currentOffsetX) / currentWidth;
            percentageOfImageAtPinchPointY = (centerPointStartY - currentOffsetY) / currentHeight;
            startDistanceBetweenFingers = Math.sqrt(Math.pow((startX1 - startX0), 2) + Math.pow((startY1 - startY0), 2));
        }

    };

    var _touchmove = function(e) {

        var self = this;

        // This keeps touch events from moving the entire window.

        e.preventDefault();

        if (panning) {

            endX0 = e.touches[0].pageX;
            endY0 = e.touches[0].pageY;
            translateFromTranslatingX = endX0 - startX0;
            translateFromTranslatingY = endY0 - startY0;
            newOffsetX = currentOffsetX + translateFromTranslatingX;
            newOffsetY = currentOffsetY + translateFromTranslatingY;
            this.imgEl.style.left = newOffsetX + "px";
            this.imgEl.style.top = newOffsetY + "px";

        } else if (zooming) {

            // Get the new touches
            endX0 = e.touches[0].pageX;
            endY0 = e.touches[0].pageY;
            endX1 = e.touches[1].pageX;
            endY1 = e.touches[1].pageY;

            // Calculate current distance between points to get new-to-old pinch ratio and calc width and height
            endDistanceBetweenFingers = Math.sqrt( Math.pow((endX1-endX0),2) + Math.pow((endY1-endY0),2) );
            pinchRatio = endDistanceBetweenFingers / startDistanceBetweenFingers;
            newContinuousZoom = pinchRatio * currentContinuousZoom;
            newWidth = imgWidth * newContinuousZoom;
            newHeight  = imgHeight * newContinuousZoom;

            // Get the point between the two touches, relative to upper-left corner of image
            centerPointEndX = ((endX0 + endX1) / 2.0);
            centerPointEndY = ((endY0 + endY1) / 2.0);

            // This is the translation due to pinch-zooming
            translateFromZoomingX = (currentWidth - newWidth) * percentageOfImageAtPinchPointX;
            translateFromZoomingY = (currentHeight - newHeight) * percentageOfImageAtPinchPointY;

            // And this is the translation due to translation of the centerpoint between the two fingers
            translateFromTranslatingX = centerPointEndX - centerPointStartX;
            translateFromTranslatingY = centerPointEndY - centerPointStartY;

            // Total translation is from two components: (1) changing height and width from zooming and (2) from the two fingers translating in unity
            translateTotalX = translateFromZoomingX + translateFromTranslatingX;
            translateTotalY = translateFromZoomingY + translateFromTranslatingY;

            // the new offset is the old/current one plus the total translation component
            newOffsetX = currentOffsetX + translateTotalX;
            newOffsetY = currentOffsetY + translateTotalY;

            // Set the image attributes on the page
            this.imgEl.style.left = newOffsetX + "px";
            this.imgEl.style.top = newOffsetY + "px";
            this.imgEl.width = newWidth;
            this.imgEl.height = newHeight;

        }

        //_transform.call(self);

    };

    var _touchend = function(e) {

        if (panning) {

            panning = false;
            currentOffsetX = newOffsetX;
            currentOffsetY = newOffsetY;

        } else if (zooming) {

            zooming = false;
            currentOffsetX = newOffsetX;
            currentOffsetY = newOffsetY;
            currentWidth = newWidth;
            currentHeight = newHeight;
            currentContinuousZoom = newContinuousZoom;

        }

    };

    var _touchcancel = function(e) {
        if (this.panning) {
            this.panning = false;
        } else if (this.zooming) {
            this.zooming = false;
        }
    };

    // @TODO replace style.top/style.left by transform for iphone4+
    var _transform = function() {

        var transform;

        if (window.navigator.userAgent.match(/Android 2.*/i) !== null) {
            transform = [
                'scale(', this.scale, ', ', this.scale, ') ',
                'translate(', this.posX, 'px,', this.posY, 'px) '
            ].join('');
        } else {
            transform = [
                'scale3d(', this.scale, ', ', this.scale, ', 1) ',
                'translate3d(', this.posX, 'px,', this.posY, 'px, 0) '
            ].join('');
        }

        this.imgEl.style.transform = transform;
        this.imgEl.style.oTransform = transform;
        this.imgEl.style.msTransform = transform;
        this.imgEl.style.mozTransform = transform;
        this.imgEl.style.webkitTransform = transform;
    };

    var _onimgload = function() {

        var self = this;

        var screenWidth = window.innerWidth,
            screenHeight = window.innerHeight;

        imgWidth = self.imgEl.width; // clientWidth
        imgHeight = self.imgEl.height; // clientHeight
        currentContinuousZoom = 1.0;

        if (imgWidth >= screenWidth) {
            currentOffsetX = 0;
            currentWidth = screenWidth;
        } else {
            currentOffsetX = (screenWidth - imgWidth) / 2.0;
            currentWidth = imgWidth;
        }

        currentContinuousZoom = (currentWidth / imgWidth);

        currentHeight = imgHeight * currentContinuousZoom;

        if (currentHeight >= screenHeight) {
            currentOffsetY = 0;
        } else {
            currentOffsetY = (screenHeight - currentHeight) / 2.0;
        }

        // currentOffsetX = 0;//-imgWidth / 2.0;
        // currentOffsetY = 0;//-imgHeight / 2.0;
        // currentWidth = imgWidth;
        // currentHeight = imgHeight;

        self.imgEl.height = currentHeight;
        self.imgEl.width = currentWidth;
        self.imgEl.style.left = currentOffsetX + "px";
        self.imgEl.style.top = currentOffsetY + "px";

    };

    var _onafterrender = function() {

        var self = this;

        this.clickEvent = new _fs.events.Click({
            autoAttach: true,
            disableScrolling: true,
            el: this.el,
            scope: this,
            handler: function() {
                setTimeout(function() { self.fire('hide'); }, 1);
            }
        });

        this.imgEl = this.el.querySelector('.imagezoom-image');

        this._onimgload = _onimgload.bind(this);
        this.imgEl.addEventListener('load', this._onimgload, false);

        if (this.config.image) {
            this.imgEl.src = this.config.image;
        }

        this._touchstart = _touchstart.bind(this);
        this._touchmove = _touchmove.bind(this);
        this._touchend = _touchend.bind(this);
        this._touchcancel = _touchcancel.bind(this);
        this._onhistoryback = _onhistoryback.bind(this);
    };

    var _onhistoryback = function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        window.removeEventListener('hashchange', arguments.callee, false);
        this.hide();
        _fs.History.navigate(this.currentUrl);
        return false;
    };

    var _onshow = function() {
        if (this.lastSrc) {
            this.imgEl.setAttribute('src', this.lastSrc);
        }
        _fs.Selector.removeClass(this.el, 'hidden');
        this.clickEvent.attach();
        _fs.Selector.addClass(document.querySelector('body'), 'hasoverlay');

        //_transform.call(this);

        this.currentUrl = _fs.History.here();

        this.imgEl.addEventListener('touchstart', this._touchstart, false);
        this.imgEl.addEventListener('touchmove', this._touchmove, false);
        this.imgEl.addEventListener('touchend', this._touchend, false);
        this.imgEl.addEventListener('touchcancel', this._touchcancel, false);
        window.addEventListener('hashchange', this._onhistoryback, false);
    };

    var _onhide = function() {
        this.lastSrc = this.imgEl.getAttribute('src');
        this.imgEl.removeAttribute('src');
        _fs.Selector.addClass(this.el, 'hidden');
        this.clickEvent.detach();
        _fs.Selector.removeClass(document.querySelector('body'), 'hasoverlay');

        this.imgEl.removeEventListener('touchstart', this._touchstart, false);
        this.imgEl.removeEventListener('touchmove', this._touchmove, false);
        this.imgEl.removeEventListener('touchend', this._touchend, false);
        this.imgEl.removeEventListener('touchcancel', this._touchcancel, false);
        window.removeEventListener('hashchange', this._onhistoryback, false);
    };

    var _onaftercompile = function() {

        this.off(this.eid);
        this.renderer.on('afterrender', _onafterrender,
            this, this.priority.VIEWS);

        if (!this.seid) {
            this.seid = this.renderer.on('show', _onshow,
                this, this.priority.VIEWS);
            this.heid = this.renderer.on('hide', _onhide,
                this, this.priority.VIEWS);
        }

    };

    return _parent.subclass({

        constructor: function(opts) {

            opts.template = _tpl;

            this.minScale = this.defaultMinScale = opts.minScale || 1.0;
            this.maxScale = this.defaultMaxScale = opts.maxScale || 3.0;

            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onaftercompile,
                this, this.priority.VIEWS);

        },

        updateImage: function(image) {
            delete this.lastSrc;
            this.imgEl.removeAttribute('width');
            this.imgEl.removeAttribute('height');
            this.imgEl.setAttribute('src', image);
            if (this.imgEl.complete && this.imgEl.naturalWidth !== 0) {
                this._onimgload();
            }
            return this;
        },

        /**
         * Show component.
         * @method show
         */
        show: function() {
            this.fire('show', this);
            return this;
        },

        /**
         * Hide component.
         * @method show
         */
        hide: function() {
            this.fire('hide', this);
            return this;
        }

    });

})();
