class HomeController < ApplicationController
  def index
  end

  def dl
    if request.env['HTTP_USER_AGENT'].downcase =~ /iphone/
      if Rails.configuration.app_store_link == ""
        render :text => "<script>alert('#{t("not_pushed_on_app_store")}');window.location = '#{root_url}';</script>"
        return 
      end
      redirect_to Rails.configuration.app_store_link
      return 
    elsif request.env['HTTP_USER_AGENT'].downcase =~ /android/
      if Rails.configuration.android_store_link == ""
        render :text => "<script>alert('#{t("not_pushed_on_android_store")}');window.location = '#{root_url}';</script>"   
        return
      end
      redirect_to Rails.configuration.android_store_link
      return
    end
    render :text => "<script>alert('#{t("please_visite_with_mobile")}');window.location = '#{root_url}';</script>"
    return
  end
end
