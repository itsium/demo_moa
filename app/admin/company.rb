#encoding: utf-8
ActiveAdmin.register Company do
  #config.clear_action_items!
  actions :all, :except => [:destroy, :new]

  menu :priority => 2, url: "/admin/companies/" + eval("Company.last.id.to_s")
  config.filters = false

   form :html => { :multipart => true } do |f|
   f.inputs "Content information" do
     f.input :name
     f.input :description, :as => :text
     f.input :address
     f.input :phone
   end

   f.inputs "Content images" do
     f.has_many :content_medias do |p|
       p.input :image, :as => :file, :label => "Image",:hint => p.object.image.nil? ? p.template.content_tag(:span, "No Image Yet") : p.template.image_tag(p.object.image.url(:thumb))
       p.input :_destroy, :as=>:boolean, :required => false, :label => 'Remove image'
     end
   end
   f.actions
 end

  show do |content|
    attributes_table do
     row :name
     row :description
     row :address
     row :phone

      row :content_medias do |p|
        ul do
          content.content_medias.each do |img|
            li do
              image_tag(img.image.url(:thumb))
            end
          end
        end
      end
    end
  end
end
