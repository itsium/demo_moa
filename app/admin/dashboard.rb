#encoding: utf-8
ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do
  #   div :class => "blank_slate_container", :id => "dashboard_default_message" do
  #     span :class => "blank_slate" do
  #       span I18n.t("active_admin.dashboard_welcome.welcome")
  #       small I18n.t("active_admin.dashboard_welcome.call_to_action")
  #     end
  #   end
  # content do

  # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
        column do
            panel "最新注册用户" do
                table_for User.order("created_at desc").limit(5) do  
                    column t("activerecord.attributes.user.name"), :name do |u|
                        link_to u.name, admin_user_path(u)
                    end
                    column t("activerecord.attributes.user.created_at"), :created_at  
                end  
                strong { link_to "查看所有用户", admin_users_path }  
            end
        end

        column do
             panel "最新新闻活动" do
                table_for Content.order("created_at desc").limit(5) do  
                    column t("activerecord.attributes.content.title"), :title do |c|
                        link_to c.title, admin_content_path(c)
                    end
                    column t("activerecord.attributes.content.created_at"), :created_at  
                end  
                strong { link_to "查看所有新闻活动", admin_contents_path }  
            end
        end
    end
  end # content
end
