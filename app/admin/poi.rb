#encoding: utf-8
ActiveAdmin.register Poi do
  filter :name
  filter :description
  filter :address

  form :html => { :enctype => "multipart/form-data" } do |f|
   f.inputs "内容" do
    f.input :name
    f.input :address
    f.input :description, :as => :text
    f.input :image, :as => :file, :hint => f.template.image_tag(f.object.image.url(:medium))
    f.input :longitude
    f.input :latitude
  end
  f.inputs "外部链接" do
    link_to "百度坐标提取", "http://api.map.baidu.com/lbsapi/getpoint/"
  end
  f.buttons
 end

  show do |content|
    attributes_table do
      row :name
      row :address
      row :description
      row :image do |p|
        image_tag(p.image.url(:thumb))
      end
      row :longitude
      row :latitude
    end
  end

  index do
    selectable_column
    column :id do |e|
      link_to e.id, admin_poi_path(e)
    end
    column :name do |e|
      link_to e.name, admin_poi_path(e)
    end
    column :address
    column :description
    default_actions
  end
end
