(function() {

  document.addEventListener('deviceready', onDeviceReady, false);

  window.onerror = function() {

    alert(JSON.stringify(arguments));

  };

  function onDeviceReady() {

    document.removeEventListener('deviceready', onDeviceReady, false);

    device.getInfo(function(infos) {
      if (infos.platform === 'iOS' &&
          parseInt(infos.version.substr(0, 1)) >= 7 &&
          (infos.model.substr(0, 4) === 'iPod' ||
           infos.model.substr(0, 6) === 'iPhone')) {
        document.getElementById('ios7css').removeAttribute('disabled');
      }
    });

  }

}());