class CreateEventInfos < ActiveRecord::Migration
  def change
    create_table :event_infos do |t|
      t.integer :content_id
      t.string :adress
      t.integer :total_seat
      t.boolean :open_subscription
      t.datetime :start_time
      t.datetime :end_time
      
      t.timestamps
    end
    add_index :event_infos, :content_id
  end
end
