class RemoveNewsIdFromContent < ActiveRecord::Migration
  def up
    remove_index :contents, :news_id
    remove_column :contents, :news_id
  end

  def down
    add_column :contents, :news_id, :integer
    add_index :contents, :news_id, :unique => true
  end
end
