#encoding: utf-8
class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :description
      t.string :phone
      t.string :address

      t.timestamps
    end
    Company.create! :name => "公司名称", :description => "企业描述"
  end
end
