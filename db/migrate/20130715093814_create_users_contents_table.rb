class CreateUsersContentsTable < ActiveRecord::Migration
  def up
  	create_table :users_contents, :id => false do |t|
        t.references :content
        t.references :user
    end
    add_index :users_contents, [:user_id, :content_id]
    add_index :users_contents, :content_id
  end

  def down
    drop_table :users_contents
  end
end
