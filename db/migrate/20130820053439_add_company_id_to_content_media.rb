class AddCompanyIdToContentMedia < ActiveRecord::Migration
  def change
    add_column :content_media, :company_id, :integer
  end
end
