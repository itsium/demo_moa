class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.integer :category_id
      t.string :title
      t.text :data

      t.timestamps
    end

    add_index :contents, :title
  end
end
