class AddAttachmentMapImageToEventInfos < ActiveRecord::Migration
  def self.up
    change_table :event_infos do |t|
      t.attachment :map_image
    end
  end

  def self.down
    drop_attached_file :event_infos, :map_image
  end
end
