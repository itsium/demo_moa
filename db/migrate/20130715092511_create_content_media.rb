class CreateContentMedia < ActiveRecord::Migration
  def change
    create_table :content_media do |t|
      t.integer :content_id
      t.string :caption

      t.timestamps
    end
  end
end
