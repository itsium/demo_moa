class ChangeCompanyDescType < ActiveRecord::Migration
  def up
    change_column :companies, :description, :text
    change_column :pois, :description, :text
  end

  def down
    change_column :companies, :description, :string
    change_column :pois, :description, :string
  end
end
